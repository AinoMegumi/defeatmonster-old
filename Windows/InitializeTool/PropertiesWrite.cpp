﻿#include "Win32API.hpp"
#include "PropertiesWrite.hpp"
#include "PropertiesRead.hpp"
#include <algorithm>
#include <fstream>

PropertiesWrite::PropertiesWrite(const std::string FilePath) : FilePath(FilePath) {
	if (FileExists(FilePath)) {
		PropertiesRead Prop(FilePath);
		this->Arr = Prop.GetData();
	}
}

bool PropertiesWrite::TagExists(const std::string Tag) {
	if (this->Arr.empty()) return false;
	return std::any_of(this->Arr.begin(), this->Arr.end(), [Tag](const PropertiesData& p) { return p.Tag == Tag; });
}

int PropertiesWrite::GetTagIndex(const std::string Tag) {
	for (int i = 0; i < this->Arr.size(); i++) if (this->Arr.at(i).Tag == Tag) return i;
	return -1;
}

void PropertiesWrite::AddData(const std::string Tag, const std::string Data) {
	if (TagExists(Tag)) this->Arr[this->GetTagIndex(Tag)].Data = Data;
	else this->Arr.emplace_back(Tag, Data);
}

void PropertiesWrite::Delete(const std::string Tag) {
	if (this->TagExists(Tag)) this->Arr.erase(this->Arr.begin() + this->GetTagIndex(Tag));
}

void PropertiesWrite::Write() {
	std::ofstream ofs(this->FilePath);
	for (const PropertiesData p : this->Arr) ofs << p.Tag << "=" << p.Data << std::endl;
	ofs.close();
}
