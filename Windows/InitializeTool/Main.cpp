#include "ApplicationDirectory.hpp"
#include "KgWinException.hpp"
#include "MSXMLRead.hpp"
#include "EnvironmentVariable.hpp"
#include "PropertiesRead.hpp"
#include "PropertiesWrite.hpp"
#include "RegistryRead.hpp"
#include "RegistryWrite.hpp"
#include "Win32API.hpp"
#include "IniRead.hpp"
#include "IniWrite.hpp"
#include <direct.h>
#include <iostream>

void CreateBaseFile(const ApplicationDirectoryA& AppPath) {
	const std::string FileCreateDir = EnvironmentVariableA("PROGRAMDATA").Get() + "\\Kamioda Games\\Kamioda Warrior\\" + GetSIDStringA();
	_mkdir(FileCreateDir.c_str());
	MSXMLRead xml(AppPath.ChangeFullPath(".\\Status\\partner.xml"), "characterdatabook/characterdata/");
	PropertiesWrite PropWrite(FileCreateDir + "\\dmsvdata.dat");
	xml.Load("tag");
	if (!PropWrite.TagExists("player")) PropWrite.AddData("player", 0);
	for (const auto& i : xml[0]) if (!PropWrite.TagExists(i)) PropWrite.AddData(i, 0);
	PropWrite.Write();
}

void CreateConfigFile(const ApplicationDirectoryA& AppPath) {
	const RegistryRead RegRead(HKEY_CURRENT_USER, "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Shell Folders");
	const IniWrite write(RegRead.ReadString("{4C5C32FF-BB9D-43B0-B5B4-2D72E54EAAA4}") + "\\Kamioda Games\\Kamioda Warrior\\dmuser.ini");
	const IniRead read(AppPath.ChangeFullPath(".\\System\\default.ini"));
	auto WriteOutTextConfig = [&write, &read](const std::string Section, const std::string Tag, const std::string DefaultData) {
		write.WriteString(Section, Tag, read.GetString(Section, Tag, DefaultData));
	};
	auto WriteOutNumberConfig = [&write, &read](const std::string Section, const std::string Tag, const int DefaultData) {
		write.WriteNum(Section, Tag, read.GetNum(Section, Tag, DefaultData));
	};
	WriteOutTextConfig("System", "Font", "C:\\Windows\\Fonts\\msgothic.ttc");
	WriteOutNumberConfig("System", "StringSize", 16);
	if (const std::string UserName = ConvertUserNameToFullName(GetExecutionUserNameA()); UserName.size() > 5)
		WriteOutTextConfig("System", "DefaultName", "プレイヤー");
	else write.WriteString("System", "DefaultName", UserName);
	WriteOutTextConfig("System", "Mode", "User");
	WriteOutNumberConfig("System", "GraphAllDamage", 0);
	WriteOutTextConfig("SoundFiles", "BattleMusic", ".\\Sound\\Music\\Battle-Glorious.ogg");
	WriteOutTextConfig("SoundFiles", "DangerMusic", ".\\Sound\\Music\\Battle-masyou.ogg");
	WriteOutTextConfig("SoundFiles", "LastSpurt", ".\\Sound\\Music\\Battle-Everlasting.ogg");
	WriteOutTextConfig("SoundFiles", "PlayerSideAttack", "N/A");
	WriteOutTextConfig("SoundFiles", "EnemySideAttack", "N/A");
	WriteOutTextConfig("SoundFiles", "MagicCast", "N/A");
	WriteOutNumberConfig("SoundVolume", "Master", 100);
	WriteOutNumberConfig("SoundVolume", "BattleMusic", 100);
	WriteOutNumberConfig("SoundVolume", "DangerMusic", 100);
	WriteOutNumberConfig("SoundVolume", "LastSpurt", 100);
	WriteOutNumberConfig("SoundVolume", "PlayerSideAttack", 100);
	WriteOutNumberConfig("SoundVolume", "EnemySideAttack", 100);
	WriteOutNumberConfig("SoundVolume", "MagicCast", 100);
	WriteOutNumberConfig("KeyConfig", "UpKey1", 17);
	WriteOutNumberConfig("KeyConfig", "UpKey2", 200);
	WriteOutNumberConfig("KeyConfig", "DownKey1", 31);
	WriteOutNumberConfig("KeyConfig", "DownKey2", 208);
	WriteOutNumberConfig("KeyConfig", "LeftKey1", 30);
	WriteOutNumberConfig("KeyConfig", "LeftKey2", 203);
	WriteOutNumberConfig("KeyConfig", "RightKey1", 32);
	WriteOutNumberConfig("KeyConfig", "RightKey2", 205);
	WriteOutNumberConfig("KeyConfig", "EnterKey1", 57);
	WriteOutNumberConfig("KeyConfig", "EnterKey2", 28);
	WriteOutNumberConfig("KeyConfig", "CancelKey1", 48);
	WriteOutNumberConfig("KeyConfig", "CancelKey2", 1);
	WriteOutNumberConfig("KeyConfig", "EngageKey1", 42);
	WriteOutNumberConfig("KeyConfig", "EngageKey2", 54);
}

void EntryConfigToRegistry(const ApplicationDirectoryA& AppPath) {
	RegistryWrite system(HKEY_CURRENT_USER, "SOFTWARE\\Kamioda Games\\Kamioda Warrior\\DefeatMonster");
	RegistryWrite soundfile(HKEY_CURRENT_USER, "SOFTWARE\\Kamioda Games\\Kamioda Warrior\\DefeatMonster\\Sound\\Files");
	RegistryWrite soundvolume(HKEY_CURRENT_USER, "SOFTWARE\\Kamioda Games\\Kamioda Warrior\\DefeatMonster\\Sound\\Volume");
	RegistryWrite keyconfig(HKEY_CURRENT_USER, "SOFTWARE\\Kamioda Games\\Kamioda Warrior\\DefeatMonster\\KeyConfig");
	const IniRead read(AppPath.ChangeFullPath(".\\System\\default.ini"));
	system.Write("Font", read.GetString("System", "Font", "C:\\Windows\\Fonts\\msgothic.ttc"));
	system.Write("Font", read.GetNum("System", "StringSize", 16));
	if (const std::string UserName = ConvertUserNameToFullName(GetExecutionUserNameA()); UserName.size() > 5)
		system.Write("DefaultName", "プレイヤー");
	else system.Write("DefaultName", UserName);
	system.Write("DefaultName", read.GetString("System", "Mode", "User"));
	system.Write("GraphAllDamage", read.GetNum("System", "GraphAllDamage", 0));
	soundfile.Write("BattleMusic", read.GetString("SoundFiles", "BattleMusic", ".\\Sound\\Music\\Battle-Glorious.ogg"));
	soundfile.Write("DangerMusic", read.GetString("SoundFiles", "DangerMusic", ".\\Sound\\Music\\Battle-masyou.ogg"));
	soundfile.Write("LastSpurt", read.GetString("SoundFiles", "LastSpurt", ".\\Sound\\Music\\Battle-Everlasting.ogg"));
	soundfile.Write("PlayerSideAttack", read.GetString("SoundFiles", "PlayerSideAttack", "N/A"));
	soundfile.Write("EnemySideAttack", read.GetString("SoundFiles", "EnemySideAttack", "N/A"));
	soundfile.Write("MagicCast", read.GetString("SoundFiles", "MagicCast", "N/A"));
	soundvolume.Write("Master", read.GetNum("SoundVolume", "Master", 100));
	soundvolume.Write("BattleMusic", read.GetNum("SoundVolume", "BattleMusic", 100));
	soundvolume.Write("DangerMusic", read.GetNum("SoundVolume", "DangerMusic", 100));
	soundvolume.Write("LastSpurt", read.GetNum("SoundVolume", "LastSpurt", 100));
	soundvolume.Write("PlayerSideAttack", read.GetNum("SoundVolume", "PlayerSideAttack", 100));
	soundvolume.Write("EnemySideAttack", read.GetNum("SoundVolume", "EnemySideAttack", 100));
	soundvolume.Write("MagicCast", read.GetNum("SoundVolume", "MagicCast", 100));
	keyconfig.Write("UpKey1", read.GetNum("KeyConfig", "UpKey1", 17));
	keyconfig.Write("UpKey2", read.GetNum("KeyConfig", "UpKey2", 200));
	keyconfig.Write("DownKey1", read.GetNum("KeyConfig", "DownKey1", 31));
	keyconfig.Write("DownKey2", read.GetNum("KeyConfig", "DownKey2", 208));
	keyconfig.Write("LeftKey1", read.GetNum("KeyConfig", "LeftKey1", 30));
	keyconfig.Write("LeftKey2", read.GetNum("KeyConfig", "LeftKey2", 203));
	keyconfig.Write("RightKey1", read.GetNum("KeyConfig", "RightKey1", 32));
	keyconfig.Write("RightKey2", read.GetNum("KeyConfig", "RightKey2", 205));
	keyconfig.Write("EnterKey1", read.GetNum("KeyConfig", "EnterKey1", 57));
	keyconfig.Write("EnterKey2", read.GetNum("KeyConfig", "EnterKey2", 28));
	keyconfig.Write("CancelKey1", read.GetNum("KeyConfig", "CancelKey1", 48));
	keyconfig.Write("CancelKey2", read.GetNum("KeyConfig", "CancelKey2", 1));
	keyconfig.Write("EngageKey1", read.GetNum("KeyConfig", "EngageKey1", 42));
	keyconfig.Write("EngageKey2", read.GetNum("KeyConfig", "EngageKey2", 54));
}

int input(const int max, const int min) {
	int i;
	std::cin >> i;
	return (i > max || i < min) ? input(max, min) : i;
}

void GraphStartScreen() {
	std::cout
		<< "========================================" << std::endl
		<< "         初期設定を行います          " << std::endl
		<< " 設定が完了するまでしばらくお待ち下さい " << std::endl
		<< "   右上の[Ｘ]ボタンで閉じないで下さい   " << std::endl
		<< "設定中もコンピュータはご利用いただけます" << std::endl
		<< "========================================" << std::endl;
}

inline void StartMessage() {
	std::cout << "設定後にサインアウトされることがあります。。" << std::endl;
	std::cout << "開始前に全てのタスクを終了させることを推奨します。" << std::endl;
	system("pause");

}

inline int inputWriteTarget() {
	std::cout << "設定はどちらに書き込みますか？" << std::endl;
	std::cout << "1. ユーザーの「保存したゲーム」フォルダ内" << std::endl;
	std::cout << "2. ユーザーのレジストリ" << std::endl;
	return input(2, 1);
}

int main() {
	try {
		StartMessage();
		system("cls");
		const int i = inputWriteTarget();
		system("cls");
		GraphStartScreen();
		ApplicationDirectoryA AppPath = ApplicationDirectoryA::Initialize(true);
		CoInitialize(nullptr);
		CreateBaseFile(AppPath);
		CoUninitialize();
		std::cout << "基本ファイル作成完了" << std::endl;
		i == 1 ? CreateConfigFile(AppPath) : EntryConfigToRegistry(AppPath);
		std::cout << "設定登録完了" << std::endl;
		if (i == 2) {
			std::cout << "サインアウトします" << std::endl;
			std::cout << "未保存のデータがある場合は保存してからEnterキーを押してください。" << std::endl;
			system("pause");
			if (0 == ExitWindowsEx(EWX_LOGOFF, NULL)) throw KgWinException();
		}
		else {
			std::cout << "全ての処理がが完了しました" << std::endl;
			std::cout << "Enterキーを押すと終了します。" << std::endl;
			system("pause");
		}
		return 0;
	}
	catch (const KgWinException& kex) {
		kex.GraphErrorMessageOnConsole();
	}
	catch (const std::exception& er) {
		std::cout << er.what() << std::endl;
	}
	system("cls");
	std::cout << "作業に失敗しました" << std::endl;
	std::cout << "サインアウトに失敗した場合は手動でサインアウトして下さい。" << std::endl;
	std::cout << "その他の場所で失敗した場合はアクセス権限を確認して下さい。" << std::endl;
	std::cout << "同じ処理で何度も同じエラーが発生する場合は、制作者に連絡して下さい。" << std::endl;
	return 1;
}
