#ifndef __AIMANAGER_HPP__
#define __AIMANAGER_HPP__
#include "Status.hpp"
#include "DllFunctionCallManager.hpp"
#include "Random.hpp"
#include <memory>

template<class InternalAI, class ExternalAI> 
class AIManager {
private:
	InternalAI Internal;
	ExternalAI* External;
public:
	AIManager(InternalAI&& Inter, ExternalAI* Ext)
		: Internal(Inter), External(Ext) {}
	template<size_t n>
	SkillA command(const std::array<Parameter, n>& CurrentParameter, const size_t CharacterPos) {
		if (this->External == nullptr) return this->Internal.command();
		auto CreateAIParameterList = [](const std::array<AIParameter, n>& Param) {
			AIParameterList List{};
			List.Parameters = new AIParameter[n];
			for (size_t i = 0; i < n; i++) List.Parameters[i] = Param[i];
			List.CharacterNum = static_cast<unsigned int>(n);
			return List;
		};
		const std::string tag = this->External->command(
			CreateAIParameterList(Status::CreateParameterArr(CurrentParameter)),
			CreateAISkillList(CurrentParameter[CharacterPos].AbleToUseAttackMagic),
			CreateAISkillList(CurrentParameter[CharacterPos].AbleToUseSingleCureMagic),
			CreateAISkillList(CurrentParameter[CharacterPos].AbleToUseRangeCureMagic)
		);
		for (const auto& i : CurrentParameter[CharacterPos].AbleToUseAttackMagic)
			if (tag == i.Tag) return i;
		for (const auto& i : CurrentParameter[CharacterPos].AbleToUseSingleCureMagic)
			if (tag == i.Tag) return i;
		for (const auto& i : CurrentParameter[CharacterPos].AbleToUseRangeCureMagic)
			if (tag == i.Tag) return i;
		throw std::runtime_error("not exist tag is returned.");
		//if (this->External == nullptr) return this->Internal.command();
		//const SkillA skill = this->External->command(CurrentParameter);
		//return skill.Tag == "amnormal"
		//	? BaseBattleCreateImpl::NormalAttack
		//	: (skill.Tag == "amguard" ? BaseBattleCreateImpl::Guard : skill);
	}
	size_t GetTarget() {
		return External == nullptr ? this->Internal.GetTarget() : this->External->GetTarget();
	}
};
#endif
