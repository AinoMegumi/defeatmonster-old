﻿#ifndef __COMMANDLINEMANAGER_HPP__
#define __COMMANDLINEMANAGER_HPP__
#include "Split.hpp"

class CommandLineManager {
private:
	std::vector<std::string> CmdArgs;
public:
	CommandLineManager(const std::string Args) {
		const std::vector<std::string> temp = SplitString(Args, ' ');
		for (const std::string s : temp) {
			if (s.front() == '/') this->CmdArgs.emplace_back(s);
			else {
				if (this->CmdArgs.back().front() == '/') this->CmdArgs.emplace_back(s);
				else this->CmdArgs.back() += " " + s;
			}
		}
	}
	std::string GetCommandLineData(const std::string OrderIndex) const {
		for (size_t i = 0; i < this->CmdArgs.size(); i++) {
			if (OrderIndex == this->CmdArgs[i]) 
				return this->CmdArgs.size() == i + 1 ? "" : this->CmdArgs[i + 1];
		}
		return "";
	}
};
#endif
