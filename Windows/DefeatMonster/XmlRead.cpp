﻿#include "MSXMLRead.hpp"
#include "Program.hpp"
#include "StatusRawData.hpp"
#include "Win32API.hpp"
#include "Skill.hpp"
#include "StatusRawData.hpp"
#include "KgWinException.hpp"
#include "AddVector.hpp"
#include "RegistryRead.hpp"
#include <tuple>
#include <vector>

std::vector<StatusRawData> ReadPlayerData() {
	MSXMLRead XmlRead(Program::ChangeFullPath(".\\Status\\player.xml"), "characterdatabook/characterdata/");
	XmlRead.Load("tag", "hp", "mp", "attack", "defence", "magicattack", "magicdefence", "magiccure", "speed", "clever", "element1", "element2");
	std::vector<StatusRawData> Arr{};
	for (size_t i = 0; i < XmlRead[0].size() && Program::LoopCondition(); i++) {
		Arr.emplace_back(StatusRawData(
			"", XmlRead[0][i], "internal", -1, XmlRead[1].Get<int>(i), XmlRead[2].Get<int>(i), XmlRead[3].Get<int>(i), XmlRead[4].Get<int>(i),
			XmlRead[5].Get<int>(i), XmlRead[6].Get<int>(i), XmlRead[7].Get<int>(i), XmlRead[8].Get<int>(i),
			XmlRead[9].Get<int>(i), XmlRead[10][i], XmlRead[11][i]
		));
	}
	return Arr;
}

inline std::vector<StatusRawData> ReadPartnerData(const std::string& XmlFilePath) {
	MSXMLRead XmlRead(XmlFilePath, "characterdatabook/characterdata/");
	XmlRead.Load("name", "tag", "sex", "hp", "mp", "attack", "defence", "magicattack", "magicdefence", "magiccure", "speed", "clever", "element1", "element2");
	std::vector<StatusRawData> Arr{};
	for (size_t i = 0; i < XmlRead[0].size() && Program::LoopCondition(); i++) {
		const std::string tag = XmlRead["tag"][i];
		std::string AIDll = Program::ChangeFullPath(".\\AI\\Partner\\" + tag + ".dll");
		if (!FileExists(AIDll)) AIDll = Program::ChangeFullPath(".\\AI\\Partner\\ipcommon.dll");
		Arr.emplace_back(StatusRawData(
			XmlRead["name"][i], tag, AIDll, XmlRead["sex"].Get<int>(i), XmlRead["hp"].Get<int>(i), XmlRead["mp"].Get<int>(i), XmlRead["attack"].Get<int>(i),
			XmlRead["defence"].Get<int>(i),	XmlRead["magicattack"].Get<int>(i), XmlRead["magicdefence"].Get<int>(i), XmlRead["magiccure"].Get<int>(i),
			XmlRead["speed"].Get<int>(i), XmlRead["clever"].Get<int>(i), XmlRead["element1"][i], XmlRead["element2"][i]
		));
	}
	return Arr;
}

std::vector<StatusRawData> ReadPartnerData(const std::string& SavedGamesFolder, const bool ReadUserCharacter) {
	std::vector<StatusRawData> Arr = ReadPartnerData(Program::ChangeFullPath(".\\Status\\partner.xml"));
	return ReadUserCharacter && FileExists(SavedGamesFolder + "\\Kamioda Games\\Kamioda Warrior\\DefeatMonster\\partner.xml")
		? Arr + ReadPartnerData(SavedGamesFolder + "\\Kamioda Games\\Kamioda Warrior\\DefeatMonster\\partner.xml")
		: Arr;
}

inline std::vector<StatusRawData> ReadEnemyData(const std::string& XmlFilePath) {
	MSXMLRead XmlRead(XmlFilePath, "characterdatabook/characterdata/");
	XmlRead.Load("name", "tag", "hp", "mp", "attack", "defence", "magicattack", "magicdefence", "magiccure", "speed", "clever", "element1", "element2");
	std::vector<StatusRawData> Arr;
	for (size_t i = 0; i < XmlRead[0].size() && Program::LoopCondition(); i++) {
		const std::string tag = XmlRead["tag"][i];
		std::string AIDll = Program::ChangeFullPath(".\\AI\\Enemy\\" + tag + ".dll");
		if (!FileExists(AIDll)) AIDll = Program::ChangeFullPath(".\\AI\\Enemy\\iecommon.dll");
		Arr.emplace_back(StatusRawData(
			XmlRead["name"][i], tag, AIDll, 0, XmlRead["hp"].Get<int>(i), XmlRead["mp"].Get<int>(i), XmlRead["attack"].Get<int>(i),
			XmlRead["defence"].Get<int>(i), XmlRead["magicattack"].Get<int>(i), XmlRead["magicdefence"].Get<int>(i), XmlRead["magiccure"].Get<int>(i),
			XmlRead["speed"].Get<int>(i), XmlRead["clever"].Get<int>(i), XmlRead["element1"][i], XmlRead["element2"][i]
		));
	}
	return Arr;
}

std::vector<StatusRawData> ReadEnemyData(const std::string& SavedGamesFolder, const bool ReadUserCharacter) {
	std::vector<StatusRawData> Arr = ReadEnemyData(Program::ChangeFullPath(".\\Status\\enemy.xml"));
	return ReadUserCharacter && FileExists(SavedGamesFolder + "\\Kamioda Games\\Kamioda Warrior\\DefeatMonster\\enemy.xml")
		? Arr + ReadEnemyData(SavedGamesFolder + "\\Kamioda Games\\Kamioda Warrior\\DefeatMonster\\enemy.xml")
		: Arr;
}

std::vector<SkillA> LoadMagic(const std::string FilePath) {
	if (!FileExists(FilePath)) throw KgWinException(FilePath + " : File is not found");
	MSXMLRead xml(FilePath, "magicdatabook/magicdata/");
	xml.Load("name", "tag", "usemp", "basepower", "hiraganadescription", "kanjidescription", "element", "lessmp", "effect", "sound", "rangeflag", "resurrectionflag", "magictype", "addblend");
	if (xml[0].size() != xml[1].size()
		|| xml[0].size() != xml[2].size()
		|| xml[0].size() != xml[3].size()
		|| xml[0].size() != xml[4].size()
		|| xml[0].size() != xml[5].size()
		|| xml[0].size() != xml[6].size()
		|| xml[0].size() != xml[7].size()
		|| xml[0].size() != xml[8].size()
		|| xml[0].size() != xml[9].size()
		|| xml[0].size() != xml[10].size()
		|| xml[0].size() != xml[11].size()
		|| xml[0].size() != xml[12].size()
		|| xml[0].size() != xml[13].size()
		) throw KgWinException(FilePath + " : File is broken");
	std::vector<SkillA> Arr;
	for (size_t i = 0; i < xml[0].size(); i++) {
		SkillA SkillInfo{};
		SkillInfo.Name = xml[0].Get(i);
		SkillInfo.Tag = xml[1].Get(i);
		SkillInfo.UseMP = xml[2].Get<int>(i);
		SkillInfo.BasePower = xml[3].Get<int>(i);
		SkillInfo.Description = Program::PrintInKanji ? xml[5].Get(i) : xml[4].Get(i);
		SkillInfo.SkillElement = Element(xml[6].Get(i)).Elem;
		SkillInfo.LessMP = xml[7].Get<int>(i);
		SkillInfo.EffectGraphPath = xml[8].Get(i) == "N/A" ? "N/A" : Program::ChangeFullPath(".\\Graphic\\Effect\\" + xml[8].Get(i));
		SkillInfo.EffectSoundPath = xml[9].Get(i) == "N/A" ? "N/A" : Program::ChangeFullPath(".\\Sound\\Effect\\" + xml[9].Get(i));
		SkillInfo.Range = (xml[10].Get<int>(i) == 1);
		SkillInfo.ResurrectionEffect = (xml[11].Get<int>(i) == 1);
		SkillInfo.SkillType = xml[12].Get<int>(i);
		SkillInfo.AddBlendLevel = xml[13].Get<int>(i);
		Arr.emplace_back(SkillInfo);
	}
	return Arr;
}

std::tuple<std::vector<SkillA>, std::vector<SkillA>, std::vector<SkillA>> LoadAllMagic() {
	return std::make_tuple(
		LoadMagic(Program::ChangeFullPath(".\\Magic\\AttackMagic.xml")),
		LoadMagic(Program::ChangeFullPath(".\\Magic\\SingleCureMagic.xml")),
		LoadMagic(Program::ChangeFullPath(".\\Magic\\RangeCureMagic.xml"))
	);
}

std::vector<size_t> GetLevelUpBorderPointList(const std::string Tag) {
	MSXMLRead xml(Program::ChangeFullPath(".\\Level\\explist.xml"), "expdatabook/expdata/");
	xml.Load("tag");
	for (int i = 2; i <= 100; i++) xml.Load("lv" + std::to_string(i));
	std::vector<size_t> Arr;
	for (size_t i = 0; i < xml[0].size(); i++) {
		if (xml[0][i] == Tag) {
			Arr.emplace_back(0);
			for (size_t j = 1; j < xml.size(); j++) {
				const size_t Last = Arr.back();
				const size_t Data = xml[j].Get<size_t>(i);
				const size_t inputdata = Last + Data;
				Arr.emplace_back(inputdata);
			}
			Arr.erase(Arr.begin());
			break;
		}
	}
	return Arr;
}
