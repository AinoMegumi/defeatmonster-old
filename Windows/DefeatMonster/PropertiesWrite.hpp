﻿#ifndef __PROPERTIESWRITE_HPP__
#define __PROPERTIESWRITE_HPP__
#include "PropertiesData.hpp"
#include <vector>
#include <utility>

class PropertiesWrite {
private:
	std::string FilePath;
	std::vector<PropertiesData> Arr;
	size_t GetTagIndex(const std::string Tag);
public:
	// コンストラクタ
	PropertiesWrite(const std::string FilePath);
	// タグとデータを追加します。
	// 既にタグが存在する場合は、データを書き換えます。
	void AddData(const std::string Tag, const std::string Data);
	// 指定されたタグが存在するかを確認します。
	bool TagExists(const std::string Tag);
	// 指定されたタグとそれに対応するデータを削除します。
	void Delete(const std::string Tag);
	// データをファイルに書き出します。
	void Write();
};
#endif
