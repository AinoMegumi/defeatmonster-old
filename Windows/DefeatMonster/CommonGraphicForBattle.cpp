﻿#include "TitleInfo.hpp"
#include "Program.hpp"
#include "CommonGraphicForBattle.hpp"
#include "ParameterGraphicColor.hpp"
bool CommonGraphicForBattle::GraphNumMode = false;
int CommonGraphicForBattle::NameWidthWithGaugeMode;
int CommonGraphicForBattle::NameWidthWithNumMode;

CommonGraphicForBattle::CommonGraphicForBattle(KgDxGraphic::NoExcept::Picture& Background, KgDxGraphic::ThrowException::String& String)
	: StringHandle(String), PlayerSideGaugeBlackField(202, Program::StringSize / 2, Color::Black, Color::White),
	MessageWindow(WindowWidth - 20, Program::StringSize * 4 + 2, Color::Gray, Color::White), Background(Background) {}

void CommonGraphicForBattle::GraphPlayerSideCharacterParameterWithGaugeMode(const int X, const int Y, const Status& sta) const {
	this->PlayerSideGaugeBlackField.Print(X + this->NameWidthWithGaugeMode, Y);
	this->PlayerSideGaugeBlackField.Print(X + this->NameWidthWithGaugeMode, Y + this->PlayerSideGaugeBlackField.Height);
	this->StringHandle.get().Print(X, Y, sta.Name, GaugeMode::GetNameGraphColor(sta));
	KgDxGraphic::NoExcept::Box(CalcGaugeLength(this->PlayerSideGaugeBlackField.Width, sta.HP.GraphParameter), this->PlayerSideGaugeBlackField.Height, GaugeMode::GetHPGaugeColor(sta)).Print(X + this->NameWidthWithGaugeMode, Y, true);
	KgDxGraphic::NoExcept::Box(CalcGaugeLength(this->PlayerSideGaugeBlackField.Width, sta.MP.GraphParameter), this->PlayerSideGaugeBlackField.Height, Color::Pink).Print(X + this->NameWidthWithGaugeMode, Y + this->PlayerSideGaugeBlackField.Height, true);
}

void CommonGraphicForBattle::GraphPlayerSideCharacterParameterWithNumMode(const int X, const int Y, const Status& sta) const {
	this->StringHandle.get().Print(X, Y, sta.Name, NumMode::GetNameGraphColor(sta));
	this->StringHandle.get().FormatPrint(X, Y + Program::StringSize, NumMode::GetStatusGraphColor(sta), "HP: %4d", sta.HP.GraphParameter.Get());
	this->StringHandle.get().FormatPrint(X, Y + Program::StringSize * 2, NumMode::GetStatusGraphColor(sta), "MP: %4d", sta.MP.GraphParameter.Get());
	this->StringHandle.get().FormatPrint(X, Y + Program::StringSize * 3, NumMode::GetStatusGraphColor(sta), "Lv: %4d", static_cast<int>(sta.Level.GetCurrentLevel()));
}

void CommonGraphicForBattle::GraphBackground() const {
	this->Background.get().Print(0, 0, WindowWidth, WindowHeight, false);
}

void CommonGraphicForBattle::ReloadMemberInfo(const IniRead& ini) {
	this->StringHandle.get() = std::move(
		KgDxGraphic::ThrowException::String(
			Program::ChangeFullPath(ini.GetString("System", "Font", "C:\\Windows\\Fonts\\msgothic.ttc")),
			Program::StringSize
		)
	);
	this->MessageWindow = std::move(MenuWindow(WindowWidth - 20, Program::StringSize * 4 + 2, Color::Gray, Color::White));
	NameWidthWithGaugeMode = this->StringHandle.get().GetPrintStringWidth("あああああ");
	NameWidthWithNumMode = this->StringHandle.get().GetPrintStringWidth("ああああああ");
	this->PlayerSideGaugeBlackField = std::move(MenuWindow(202, Program::StringSize / 2, Color::Black, Color::White));
}
