﻿#ifdef _DEBUG
#include "BattleSimulator.hpp"

int BattleSimulator::ExecuteAttackCommandToEnemy(const size_t Attacker, bool &CriticalFlag) const {
	return this->Characters[3].Crash(this->Characters[Attacker], CriticalFlag);
}

int BattleSimulator::ExecuteAttackCommandToEnemy(const size_t Attacker, const SkillA& AttackInfo) const {
	this->Characters[Attacker].MP -= AttackInfo.UseMP;
	return this->Characters[3].Crash(AttackInfo, this->Characters[Attacker]);
}

int BattleSimulator::ExecuteCureCommand(const size_t Healer, const size_t Target, const SkillA& CureMagicInfo) const {
	const int CurePoint = this->Characters[Target].Dead()
		? (CureMagicInfo.ResurrectionEffect ? this->Characters[Target].Cure(this->Characters[Healer], CureMagicInfo) : 0)
		: this->Characters[Target].Cure(this->Characters[Healer], CureMagicInfo);
	if (CurePoint != 0) this->Characters[Healer].MP -= CureMagicInfo.UseMP;
	return CurePoint;
}

int BattleSimulator::ExecuteCureCommand(const size_t Healer, const SkillA& CureMagicInfo) const {
	int Total = 0;
	int CureCharacterNum = 0;
	for (size_t i = 0; i < 3; i++) {
		if (this->Characters[i].Dead() && !CureMagicInfo.ResurrectionEffect) continue;
		Total += this->Characters[i].Cure(this->Characters[Healer], CureMagicInfo);
		CureCharacterNum += 1;
	}
	if (Total != 0) this->Characters[Healer].MP -= CureMagicInfo.UseMP;
	return static_cast<int>(((static_cast<double>(Total) / static_cast<double>(CureCharacterNum)) + 0.5) * 10) / 10;
}

bool BattleSimulator::ExecuteResurrection(const size_t Healer, const size_t Target, const SkillA& CureMagicInfo) const {
	this->Characters[Healer].MP -= CureMagicInfo.UseMP;
	return this->Characters[Target].Resurrection(this->Characters[Healer]);
}

int BattleSimulator::ExecuteAttackCommandFromEnemy(const size_t Crasher, bool &CriticalFlag) const {
	return this->Characters[Crasher].Crash(this->Characters[3], CriticalFlag);
}

int BattleSimulator::ExecuteAttackCommandFromEnemy(const size_t Crasher, const SkillA& AttackMagicInfo) const {
	this->Characters[3].MP -= AttackMagicInfo.UseMP;
	return this->Characters[Crasher].Crash(AttackMagicInfo, this->Characters[3]);
}

int BattleSimulator::ExecuteAttackCommandFromEnemy(const SkillA& AttackMagicInfo) const {
	int Total = 0;
	int CrashCharacterNum = 0;
	for (size_t i = 0; i < 3; i++) {
		if (this->Characters[i].Dead()) continue;
		Total += this->Characters[i].Crash(AttackMagicInfo, this->Characters[3]);
		CrashCharacterNum++;
	}
	this->Characters[3].MP -= AttackMagicInfo.UseMP;
	return static_cast<int>((static_cast<double>(Total) / static_cast<double>(CrashCharacterNum) + 0.5) * 10) / 10;
}

std::array<int, 3> BattleSimulator::ExecuteCureCommand(const size_t Healer, const SkillA& CureMagicInfo, const int NoCureCharacterInputNum) const {
	std::array<int, 3> CurePointList;
	for (size_t i = 0; i < 3; i++)
		CurePointList[i] = (this->Characters[i].Dead() && !CureMagicInfo.ResurrectionEffect ? NoCureCharacterInputNum : CurePointList[i] = this->Characters[i].Cure(this->Characters[Healer], CureMagicInfo));
	this->Characters[Healer].MP -= CureMagicInfo.UseMP;
	return CurePointList;
}

std::array<int, 3> BattleSimulator::ExecuteAttackCommandFromEnemy(const SkillA& AttackMagicInfo, const int DeadCharacterInputNum) const {
	std::array<int, 3> DamageList;
	for (size_t i = 0; i < 3; i++)
		DamageList[i] = (this->Characters[i].Dead() ? DeadCharacterInputNum : DamageList[i] = this->Characters[i].Crash(AttackMagicInfo, this->Characters[3]));
	this->Characters[3].MP -= AttackMagicInfo.UseMP;
	return DamageList;
}
#endif
