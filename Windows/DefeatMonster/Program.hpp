﻿#ifndef __PROGRAM_HPP__
#define __PROGRAM_HPP__
#include "GameEnd.hpp"
#include "KgDxSystem.hpp"
#include "KgDxGraphic.hpp"
#include "CppIniRead.hpp"
#ifdef MessageBox
#undef MessageBox
#endif

namespace Program {
	extern KgDxSystem::FPSManager Fps;
	extern int StringSize;
	extern bool PrintInKanji;
	extern bool GameMasterMode;
	extern std::string Language;
	extern std::string LanguageDllName;
	extern std::string SaveDataFilePath;
	extern IniRead LangIniFile;
	extern HINSTANCE hInstance;
	void InitializeProgramInfo();
	std::string ChangeFullPath(const std::string Path);
	std::string GetApplicationCurrentDirectory();
	void RegWriteNumber(const std::string KeyName, const unsigned long Data);
	void RegWriteString(const std::string KeyName, const std::string Data);
	int MessageBox(LPCSTR lpMessage, LPCSTR lpCaption, const DWORD dwStyle);
	bool LoopCondition();
	namespace RegistryReader {
		extern std::string UserIniConfigFilePath;
		std::string RegReadRootString(const std::string KeyName, const std::string DefaultString);
		unsigned long RegReadRootNumber(const std::string KeyName, const unsigned long DefaultNumber);
		std::string RegReadSoundFile(const std::string KeyName, const std::string DefaultFile);
		int RegReadSoundVolume(const std::string KeyName, const unsigned long DefaultVolume);
		unsigned long RegReadKeyConfig(const std::string KeyName, const unsigned long DefaultKeyID);
	}
}
#endif
