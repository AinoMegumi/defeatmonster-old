﻿#ifndef __ADDVECTOR_HPP__
#define __ADDVECTOR_HPP__
#include <string>
#include <vector>
template<class C>
std::vector<C> operator + (const std::vector<C> a, const std::vector<C> b) {
	std::vector<C> c = a;
	for (const auto& i : b) c.emplace_back(i);
	return c;
}
#endif
