﻿#ifndef __COMMONGRAPHICFORBATTLE_HPP__
#define __COMMONGRAPHICFORBATTLE_HPP__
#include "Status.hpp"
#include "MenuWindow.hpp"
#include "KgDxGraphic.hpp"
#include "CppIniRead.hpp"

class CommonGraphicForBattle {
private:
	MenuWindow PlayerSideGaugeBlackField;
protected:
	std::reference_wrapper<KgDxGraphic::ThrowException::String> StringHandle;
public:
	static int NameWidthWithGaugeMode;
	static int NameWidthWithNumMode;
public:
	static bool GraphNumMode;
private:
	std::reference_wrapper<KgDxGraphic::NoExcept::Picture> Background;
protected:
	MenuWindow MessageWindow;
	void GraphPlayerSideCharacterParameterWithGaugeMode(const int X, const int Y, const Status& sta) const;
	void GraphPlayerSideCharacterParameterWithNumMode(const int X, const int Y, const Status& sta) const;
	void GraphBackground() const;
	void ReloadMemberInfo(const IniRead& ini);
public:
	CommonGraphicForBattle(KgDxGraphic::NoExcept::Picture& Background, KgDxGraphic::ThrowException::String& String);
};
#endif
