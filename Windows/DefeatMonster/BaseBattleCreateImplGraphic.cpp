﻿#include "InRangeJudgement.hpp"
#include "ParameterGraphicColor.hpp"
#include "Battle.hpp"
#include "Color.hpp"

void BaseBattleCreateImpl::GraphEnemyCharacterParameterWithGaugeMode(const int X, const int Y, const Status& sta) const {
	this->EnemySideGaugeBlackField.Print(X, Y);
	this->EnemySideGaugeBlackField.Print(X, Y + this->EnemySideGaugeBlackField.Height);
	KgDxGraphic::NoExcept::Box(CalcGaugeLength(this->EnemySideGaugeBlackField.Width - 2, sta.HP.GraphParameter), this->EnemySideGaugeBlackField.Height - 2, GaugeMode::GetHPGaugeColor(sta)).Print(X + 1, Y + 1, true);
	KgDxGraphic::NoExcept::Box(CalcGaugeLength(this->EnemySideGaugeBlackField.Width - 2, sta.MP.GraphParameter), this->EnemySideGaugeBlackField.Height - 2, Color::Pink).Print(X + 1, Y + 1 + EnemySideGaugeBlackField.Height, true);
}

void BaseBattleCreateImpl::GraphEnemyCharacterParameterWithNumMode(const int X, const int Y, const Status& sta) const {
	this->EnemySideNumParameterGraphField.Print(X, Y);
	this->StringHandle.get().FormatPrint(X + 1, Y + 1, NumMode::GetStatusGraphColor(sta), "HP:%5d　MP:%3d", sta.HP.GraphParameter.Get(), sta.MP.GraphParameter.Get());
}

void BaseBattleCreateImpl::GraphEnemy() const {
	this->EnemyGraphic.Print((WindowWidth - this->EnemyGraphic.Width) / 2, (WindowHeight - this->EnemyGraphic.Height) / 2, true);
}
