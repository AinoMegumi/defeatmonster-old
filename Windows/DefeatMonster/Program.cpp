﻿#include "Program.hpp"
#include "KgDxSystemBase.hpp"
#include "ApplicationDirectory.hpp"
#include <Windows.h>
#include "RegistryRead.hpp"
#include "RegistryWrite.hpp"
#include "Win32API.hpp"
#include "CppIniRead.hpp"

namespace Program {
	KgDxSystem::FPSManager Fps;
	int StringSize;
	bool PrintInKanji;
	bool GameMasterMode;
	std::string Language;
	std::string LanguageDllName;
	std::string SaveDataFilePath;
	IniRead LangIniFile;
	HINSTANCE hInstance;
	namespace {
		ApplicationDirectoryA AppPath;
	}
	std::string GetApplicationCurrentDirectory() { return AppPath.GetApplicationCurrentDirectory(); }
	std::string ChangeFullPath(const std::string Path) { return Path.empty() ? "" : AppPath.ChangeFullPath(Path, true); }
	int MessageBox(LPCSTR lpMessage, LPCSTR Caption, const DWORD dwStyle) {
		MSGBOXPARAMSA mbp{};
		mbp.cbSize = sizeof(mbp);
		mbp.hInstance = Program::hInstance;
		mbp.lpszCaption = Caption;
		mbp.lpszText = lpMessage;
		mbp.dwStyle = dwStyle;
		return MessageBoxIndirectA(&mbp);
	}
	namespace RegistryReader {
		std::string UserIniConfigFilePath;
		namespace {
			inline std::string LoadUserIniFile(const std::string Root, const std::string KeyName, const std::string DefaultString) {
				return  IniRead(UserIniConfigFilePath).GetString(Root, KeyName, DefaultString);
			}
			inline unsigned long LoadUserIniFile(const std::string Root, const std::string KeyName, const unsigned long DefaultNum) {
				return IniRead(UserIniConfigFilePath).GetNum(Root, KeyName, DefaultNum);
			}
			inline std::string LoadDefaultIniFile(const std::string Root, const std::string KeyName, const std::string DefaultString) {
				return  IniRead(ChangeFullPath(".\\System\\default.ini")).GetString(Root, KeyName, DefaultString);
			}
			inline unsigned long LoadDefaultIniFile(const std::string Root, const std::string KeyName, const unsigned long DefaultNum) {
				return IniRead(ChangeFullPath(".\\System\\default.ini")).GetNum(Root, KeyName, DefaultNum);
			}
			std::string RegReadString(const std::string Root, const std::string KeyName, const std::string DefaultString) {
				try {
					
					if (const std::string str = RegistryRead(HKEY_CURRENT_USER, "SOFTWARE\\Kamioda Games\\Kamioda Warrior\\DefeatMonster" + (Root.empty() ? "" : ("\\" + Root))).ReadString(KeyName); 
						!str.empty()) return str;
					throw std::exception();
				}
				catch (...) {
					const std::string IniTag = Root.find("\\") == std::string::npos ? Root : (Root.substr(0, Root.find_first_of("\\")) + Root.substr(Root.find_first_of("\\") + 1));
					if (FileExists(UserIniConfigFilePath)) {
						try {  
							if (const std::string str = LoadUserIniFile(Root.empty() ? "System" : Root, IniTag, DefaultString); !str.empty()) return str;
						}
						catch(...) {}
					}
					return LoadDefaultIniFile(IniTag.empty() ? "System" : IniTag, KeyName, DefaultString);
				}
			}
			unsigned long RegReadNumber(const std::string Root, const std::string KeyName, const unsigned long DefaultNum) {
				try {
					return RegistryRead(HKEY_CURRENT_USER, "SOFTWARE\\Kamioda Games\\Kamioda Warrior\\DefeatMonster" + (Root.empty() ? "" : ("\\" + Root))).ReadNumber(KeyName);
				}
				catch (...) {
					const std::string IniTag = KeyName.find("\\") == std::string::npos ? KeyName : (KeyName.substr(0, KeyName.find_first_of("\\")) + KeyName.substr(KeyName.find_first_of("\\") + 1));
					if (FileExists(UserIniConfigFilePath)) {
						try { return LoadUserIniFile(Root.empty() ? "System" : Root, IniTag, DefaultNum); }
						catch (...) {}
					}
					return LoadDefaultIniFile(Root.empty() ? "System" : Root, IniTag, DefaultNum);
				}
			}
		}
		std::string RegReadRootString(const std::string KeyName, const std::string DefaultString) { return RegReadString("", KeyName, DefaultString); }
		unsigned long RegReadRootNumber(const std::string KeyName, const unsigned long DefaultNumber) { return RegReadNumber("", KeyName, DefaultNumber); }
		std::string RegReadSoundFile(const std::string KeyName, const std::string DefaultFile) { return RegReadString("Sound\\Files", KeyName, ChangeFullPath(DefaultFile)); }
		int RegReadSoundVolume(const std::string KeyName, const unsigned long DefaultVolume) { return static_cast<unsigned int>(RegReadNumber("Sound\\Volume", KeyName, DefaultVolume)); }
		unsigned long RegReadKeyConfig(const std::string KeyName, const unsigned long DefaultKeyID) { return RegReadNumber("KeyConfig", KeyName, DefaultKeyID); }
	}
	void InitializeProgramInfo() {
		AppPath = ApplicationDirectoryA::Initialize(true);
		AppPath.ChangeCurrentDirectory();
		RegistryReader::UserIniConfigFilePath = ChangeFullPath(
			RegistryRead(
				HKEY_CURRENT_USER, "Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Shell Folders"
			).ReadString("{4C5C32FF-BB9D-43B0-B5B4-2D72E54EAAA4}") + "\\Kamioda Games\\Kamioda Warrior\\dmuser.ini");
	}
	void RegWriteNumber(const std::string KeyName, const unsigned long Data) {
		RegistryWrite(HKEY_CURRENT_USER, "SOFTWARE\\Kamioda Games\\Kamioda Warrior\\DefeatMonster").Write(KeyName, Data);
	}
	void RegWriteString(const std::string KeyName, const std::string Data) {
		RegistryWrite(HKEY_CURRENT_USER, "SOFTWARE\\Kamioda Games\\Kamioda Warrior\\DefeatMonster").Write(KeyName, Data);
	}
	bool LoopCondition() {
		return KgDxSystemBase::ProcessMessage();
	}
}
