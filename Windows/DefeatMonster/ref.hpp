﻿#ifndef __REF_HPP__
#define __REF_HPP__
#include <functional>

namespace standard {
	template<typename T>
	class ref {
	private:
		std::reference_wrapper<T> data;
	public:
		ref(T& t) : data(t) {}
		ref(T&&) = delete;
		ref(const ref& x) : data(x.data) {}
		ref& operator = (const ref& x) noexcept {
			this->data = x.data;
			return *this;
		}
		T& get() const { return this->data.get(); }
		std::reference_wrapper<T> operator * () noexcept { return this->data; }
		std::reference_wrapper<T> operator * () const noexcept { return this->data; }
	};

	template<typename T, size_t ArrSize>
	class ArrayRef : public ref<std::array<T, ArrSize>> {
	public:
		ArrayRef(std::array<T, ArrSize>& arr) : ref<std::array<T, ArrSize>>(arr) {}
		T& operator [] (const size_t pos) { return ref<std::array<T, ArrSize>>::get().at(pos); }
		T& operator [] (const size_t pos) const { return ref<std::array<T, ArrSize>>::get().at(pos); }
	};

	template<typename T>
	class VectorRef : public ref<std::vector<T>> {
	public:
		VectorRef(std::vector<T>& arr) : ref<std::vector<T>>(arr) {}
		T& operator [] (const size_t pos) { return ref<std::vector<T>>::get()[pos]; }
	};
}
#endif
