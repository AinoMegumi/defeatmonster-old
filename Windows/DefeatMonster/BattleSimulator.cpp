﻿#ifdef _DEBUG
#include "BattleSimulator.hpp"
#include "XmlRead.hpp"
#include "Color.hpp"
constexpr int StartFirstPartnerID = 0;

static Status SetPlayerStatusOnSimulator(const std::vector<StatusRawData>& PlayerData, const int BattleStyle, const int SexID) {
	StatusRawData r = PlayerData.at(BattleStyle);
	r.Name = "ＧＭ神御田";
	r.SexID = SexID;
	Status s = std::move(Status::CreatePlayerStatus(r));
	return s;
}

static inline std::string GetResultString(const int ResultID) {
	static const std::string Result[3] = { "Lose", "Draw", "Win" };
	return Result[ResultID];
}

#include "Win32API.hpp"

void WriteOutResultInfo(const int ResultID, const std::string& FirstPartnerName, const std::string& SecondPartnerName, const std::string& EnemyName, const bool EngageFlag, const std::string& CommandLogFileName) {
	if (!FileExists("D:\\Users\\AinoMegumi\\DebugLog\\Kamioda Games\\Kamioda Warrior\\DefeatMonster\\DefeatCheckSimulator.csv")) {
		std::ofstream ofs("D:\\Users\\AinoMegumi\\DebugLog\\Kamioda Games\\Kamioda Warrior\\DefeatMonster\\DefeatCheckSimulator.csv");
		ofs << "Result,FirstPartner,SecondPartner,Enemy,Engage,CommandLog" << std::endl;
	}
	std::ofstream ofs("D:\\Users\\AinoMegumi\\DebugLog\\Kamioda Games\\Kamioda Warrior\\DefeatMonster\\DefeatCheckSimulator.csv", std::ios::out | std::ios::app);
	ofs << GetResultString(ResultID) << "," << FirstPartnerName << "," << SecondPartnerName << "," << EnemyName << "," << EngageFlag << "," << CommandLogFileName << std::endl;
}

void BattleSimulator::OutputCommandLog(const std::string& Attacker, const std::string& AttackName, const std::string& Target, const int Damage) const {
	if (!FileExists(this->CommandLogFilePath)) {
		std::ofstream ofs(this->CommandLogFilePath);
		ofs << "Cmd,ATK,Target,Damage" << std::endl;
	}
	std::ofstream ofs(this->CommandLogFilePath, std::ios::out | std::ios::app);
	ofs << AttackName << "," << Attacker << "," << Target << "," << Damage << std::endl;
}

void BattleSimulator::OutputCommandLog(const std::string& Attacker, const std::string& AttackName, const int Average) const {
	if (!FileExists(this->CommandLogFilePath)) {
		std::ofstream ofs(this->CommandLogFilePath);
		ofs << "Cmd,ATK,Target,Damage" << std::endl;
	}
	std::ofstream ofs(this->CommandLogFilePath, std::ios::out | std::ios::app);
	ofs << AttackName << "," << Attacker << ",Range," << Average << std::endl;
}

static inline std::vector<KgDxGraphic::NoExcept::Picture> GetEnemyGraphicList(const std::vector<StatusRawData>& EnemyList) {
	std::vector<KgDxGraphic::NoExcept::Picture> EnemyGraphList;
	for (const auto& i : EnemyList) {
		auto p = KgDxGraphic::NoExcept::Picture(Program::ChangeFullPath(".\\Graphic\\Enemy\\" + i.Tag + ".png"));
		p.Load();
		EnemyGraphList.emplace_back(std::move(p));
	}
	return EnemyGraphList;
}

int BattleSimulator::main() {
	KgDxInput::Keyboard KeyInput(false);
	BattleCommonInformation bcm(KeyInput);
	const KgDxGraphic::Color CursorDefaultColor = Color::Green;
	bcm.init(CursorDefaultColor);
	bcm.Control = 0;
	int WinCount = 0;
	const std::vector<StatusRawData> PlayerArr = ReadPlayerData();
	const std::vector<StatusRawData> PartnerArr = ReadPartnerData("", false);
	const std::vector<StatusRawData> EnemyArr = ReadEnemyData("", false);
	const std::vector<KgDxGraphic::NoExcept::Picture> EnemyGraphicData = GetEnemyGraphicList(EnemyArr);
	this->StartBattleMusic();
	for (int BattleStyle = 0; BattleStyle < 4 && this->CommonLoopCondition(false); BattleStyle++) {
		for (int SexID = 0; SexID < 4 && this->CommonLoopCondition(false); SexID++) {
			this->Characters[0] = std::move(SetPlayerStatusOnSimulator(PlayerArr, BattleStyle, SexID));

			for (size_t FirstPartnerID = StartFirstPartnerID; FirstPartnerID < PartnerArr.size() && this->CommonLoopCondition(false); FirstPartnerID++) {
				this->Characters[1] = std::move(Status::CreatePartnerStatus(PartnerArr.at(FirstPartnerID)));


				for (size_t SecondPartnerID = 0; SecondPartnerID < PartnerArr.size() && this->CommonLoopCondition(false); SecondPartnerID++) {
					if (FirstPartnerID == SecondPartnerID) continue;
					for (int i = 0; i < 2 && this->CommonLoopCondition(false); i++) {
						this->Characters[1].IsEngageCharacter = (i == 1);

						// どちらもエンゲージしてない場合の検証はダブり検証になるのでスキップ
						if (FirstPartnerID > SecondPartnerID && !this->Characters[1].IsEngageCharacter) continue;
						// 同じパートナーになるのを防止
						if (SecondPartnerID == PartnerArr.size()) break;
						this->Characters[2] = std::move(Status::CreatePartnerStatus(PartnerArr.at(SecondPartnerID)));
						
						for (size_t EnemyID = 0; EnemyID < EnemyArr.size() && this->CommonLoopCondition(false); EnemyID++) {
							for (size_t j = 0; j < 3; j++) {
								this->Characters[j].HP += this->Characters[j].HP.RealParameter.GetMax();
								this->Characters[j].MP += this->Characters[j].MP.RealParameter.GetMax();
							}
							this->CommandLogFilePath = "D:\\Users\\AinoMegumi\\DebugLog\\Kamioda Games\\Kamioda Warrior\\DefeatMonster\\CmdLog\\" + std::to_string(std::chrono::system_clock::to_time_t(std::chrono::system_clock::now())) + ".csv";
							
							this->Characters[3] = std::move(Status::CreateEnemyStatus(EnemyArr.at(EnemyID)));
							PartnerCommandManager Player(this->Characters.get(), 2, 0, this->DllFuncMgr, bcm.CursorColor, this->StringHandle, bcm.CursorPoints[0], this->NameWidthWithGaugeMode);
							PartnerCommandManager FirstPartner(this->Characters.get(), 2, 1, this->DllFuncMgr, bcm.CursorColor, this->StringHandle, bcm.CursorPoints[1], this->NameWidthWithGaugeMode);
							PartnerCommandManager SecondPartner(this->Characters.get(), 2, 2, this->DllFuncMgr, bcm.CursorColor, this->StringHandle, bcm.CursorPoints[2], this->NameWidthWithGaugeMode);
							const int ResultID = this->BattleSection(bcm, CursorDefaultColor, Player, FirstPartner, SecondPartner, EnemyGraphicData.at(EnemyID));
							WriteOutResultInfo(ResultID, this->Characters[1].Name, this->Characters[2].Name, this->Characters[3].Name, i == 1, this->CommandLogFilePath.substr(this->CommandLogFilePath.find_last_of("\\") + 1));
							const __int64 start = GetCurrentClockOnMilliSeconds();
							if (ResultID == 2) WinCount++;
							COMMONWAIT(start, this->CommonLoopCondition()) {
								KgDxGraphic::NoExcept::Clear();
								KgDxGraphic::NoExcept::ChangeCleannessLevel(100);
								this->GraphBackground();
								if (!this->Win()) this->GraphEnemy(EnemyGraphicData.at(EnemyID));
								KgDxGraphic::NoExcept::ChangeCleannessLevel(70);
								this->GraphPlayerSideCharacterParameter();
								this->GraphEnemyCharacterParameter();
								this->MessageWindow.Print(MESSAGEWINDOWGRAPHPOINT);
								KgDxGraphic::NoExcept::Flip();
							}
						}
					}
				}
				system("empty.exe DefeatMonster.exe");
			}
		}
	}
	return WinCount;
}
#endif
