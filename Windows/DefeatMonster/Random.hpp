﻿#ifndef __RANDOM_HPP__
#define __RANDOM_HPP__
#include <random>

class Random {
private:
	static thread_local std::mt19937 mt;
public:
	static std::mt19937& GetEngine() { return mt; }
};

template<typename Container>
decltype(auto) SelectElementry(Container&& c) {
	std::uniform_int_distribution<std::size_t> rand(0, c.size() - 1);
	return c[rand(Random::GetEngine())];
}
#endif
