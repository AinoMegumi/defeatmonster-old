﻿#include "TitleInfo.hpp"
#include "Program.hpp"
#include "CppIniRead.hpp"
#include "InRangeJudgement.hpp"
#include "Battle.hpp"
#include "Color.hpp"
SkillA BaseBattleCreateImpl::NormalAttack;
SkillA BaseBattleCreateImpl::Guard;

void BaseBattleCreateImpl::LoadBasicInfo() {
	NormalAttack = SkillA("N/A", "amnormal", 0, 0, "N/A", ElementInfo::Normal, Program::ChangeFullPath(".\\Graphic\\Effect\\sword.png"), Program::ChangeFullPath(".\\Sound\\Effect\\normal_attack.mp3"), false, false, 0, 0, -1);
	Guard = SkillA("N/A", "amguard", 0, 0, "N/A", ElementInfo::Normal, "N/A", "N/A", false, false, 0, 0, -1);
}

BaseBattleCreateImpl::BaseBattleCreateImpl(KgDxGraphic::NoExcept::Picture& BackgroundGraph, const std::string& EnemyGraphicFilePath,
	KgDxGraphic::ThrowException::String& string, DllFunctionCallManager& DllFuncManager, int& MasterVolume)
	: CommonGraphicForBattle(BackgroundGraph, string),
	EnemyGraphic(EnemyGraphicFilePath.empty() ? KgDxGraphic::NoExcept::Picture() : KgDxGraphic::NoExcept::Picture(EnemyGraphicFilePath)),
	EnemySideGaugeBlackField(802, 20, Color::Black, Color::White),
	EnemySideNumParameterGraphField(string.GetPrintStringWidth("HP:00000　　MP:000") + 2, Program::StringSize + 2, Color::Gray, Color::White),
	GraphAllDamageWhenRangeAttack(1 == Program::RegistryReader::RegReadRootNumber("GraphAllDamage", 0)), DllFuncMgr(DllFuncManager),
	msg(DllFuncManager, string), MasterVolume(MasterVolume) {
	this->MainBattleMusic = std::move(KgDxSound::NoExcept::Sound(Program::RegistryReader::RegReadSoundFile("BattleMusic", "")));
	this->DangerBattleMusic = std::move(KgDxSound::NoExcept::Sound(Program::RegistryReader::RegReadSoundFile("DangerMusic", "")));
	this->LastSpurtBattleMusic = std::move(KgDxSound::NoExcept::Sound(Program::RegistryReader::RegReadSoundFile("LastSpurt", "")));
	this->PlayerSideAttackSound = std::move(KgDxSound::NoExcept::Sound(Program::RegistryReader::RegReadSoundFile("PlayerSideAttack", "")));
	this->EnemySideAttackSound = std::move(KgDxSound::NoExcept::Sound(Program::RegistryReader::RegReadSoundFile("EnemySideAttack", "")));
	this->MagicCastSound = std::move(KgDxSound::NoExcept::Sound(Program::RegistryReader::RegReadSoundFile("MagicCast", "")));

	this->MainBattleMusic.Load();
	this->DangerBattleMusic.Load();
	this->LastSpurtBattleMusic.Load();
	this->PlayerSideAttackSound.Load();
	this->EnemySideAttackSound.Load();
	this->MagicCastSound.Load();

	this->MainBattleMusic.ChangeVolume(MasterVolume * Program::RegistryReader::RegReadSoundVolume("BattleMusic", 0x00000064));
	this->DangerBattleMusic.ChangeVolume(MasterVolume * Program::RegistryReader::RegReadSoundVolume("DangerMusic", 0x00000064));
	this->LastSpurtBattleMusic.ChangeVolume(MasterVolume * Program::RegistryReader::RegReadSoundVolume("LastSpurt", 0x00000064));
	this->PlayerSideAttackSound.ChangeVolume(MasterVolume * Program::RegistryReader::RegReadSoundVolume("PlayerSideAttack", 0x00000064));
	this->EnemySideAttackSound.ChangeVolume(MasterVolume * Program::RegistryReader::RegReadSoundVolume("EnemySideAttack", 0x00000064));
	this->MagicCastSound.ChangeVolume(MasterVolume * Program::RegistryReader::RegReadSoundVolume("MagicCast", 0x00000064));
#ifdef _DEBUG
	if (!EnemyGraphicFilePath.empty()) this->EnemyGraphic.Load();
#else
	this->EnemyGraphic.Load();
#endif
}

void BaseBattleCreateImpl::ChangeEnemyGraphic(const std::string& EnemyGraphFilePath) {
	this->EnemyGraphic.Delete();
	this->EnemyGraphic = std::move(KgDxGraphic::NoExcept::Picture(EnemyGraphFilePath));
	this->EnemyGraphic.Load();
}

void BaseBattleCreateImpl::StartBattleMusic() const {
	this->MainBattleMusic.Play(KgDxSound::PlayType::Loop);
}

static inline bool MusicStopped(const KgDxSound::NoExcept::Sound& SoundInfo) { 
	return KgDxSound::SoundState::Stopping == SoundInfo.GetSoundState(); 
};


void BaseBattleCreateImpl::ChangeMusic(const std::initializer_list<Status>& PlayerSideCharacterList, const Status& EnemyMonster) {
	auto DangerCharacterExist = [](const std::initializer_list<Status>& PlayerSideCharacterList) {
		for (const auto& i : PlayerSideCharacterList) if (i.HP.RealParameter.GetRatioArrange() > 0 && i.HP.RealParameter.GetRatioArrange() <= 25) return true;
		return false;
	};
	auto MusicStopped = [](const KgDxSound::NoExcept::Sound& SoundInfo) { return KgDxSound::SoundState::Stopping == SoundInfo.GetSoundState(); };
	if (DangerCharacterExist(PlayerSideCharacterList)) {
		if (MusicStopped(this->DangerBattleMusic)) {
			this->MainBattleMusic.Pause();
			this->LastSpurtBattleMusic.Pause();
			this->DangerBattleMusic.Play(KgDxSound::PlayType::Loop);
		}
	}
	else if (DangerCharacterExist({ EnemyMonster })) {
		if (MusicStopped(this->LastSpurtBattleMusic)) {
			this->MainBattleMusic.Pause();
			this->LastSpurtBattleMusic.Play(KgDxSound::PlayType::Loop);
			this->DangerBattleMusic.Stop();
		}
	}
	else if (MusicStopped(this->MainBattleMusic)) {
		this->MainBattleMusic.Play(KgDxSound::PlayType::Loop);
		this->DangerBattleMusic.Stop();
		this->LastSpurtBattleMusic.Stop();
	}
}

bool BaseBattleCreateImpl::Lose(const std::initializer_list<Status>& PlayerSideCharactersHP) {
	for (const auto& i : PlayerSideCharactersHP) if (!i.Dead()) return false;
	return true;
}

bool BaseBattleCreateImpl::Win(const Status EnemySideCharacter) {
	return EnemySideCharacter.Dead();
}

#include "Win32API.hpp"

void BaseBattleCreateImpl::ReloadMemberInfo(const std::initializer_list<Status>& PlayerSideCharacterList, const Status& EnemyMonster) {
	IniRead ini(
		FileExists(Program::RegistryReader::UserIniConfigFilePath)
		? Program::RegistryReader::UserIniConfigFilePath
		: Program::ChangeFullPath(".\\System\\default.ini")
	);
	Program::StringSize = ini.GetNum("System", "StringSize", 16);
	CommonGraphicForBattle::ReloadMemberInfo(ini);
	this->EnemySideNumParameterGraphField = std::move(MenuWindow(this->StringHandle.get().GetPrintStringWidth("HP:00000　　MP:000") + 2, Program::StringSize + 2, Color::Gray, Color::White));
	if (!MusicStopped(this->MainBattleMusic)) this->MainBattleMusic.Pause();
	if (!MusicStopped(this->DangerBattleMusic)) this->DangerBattleMusic.Pause();
	if (!MusicStopped(this->LastSpurtBattleMusic)) this->LastSpurtBattleMusic.Pause();
	this->MasterVolume.get() = ini.GetNum("SoundVolume", "Master", 100);
	this->MainBattleMusic.ChangeVolume(this->MasterVolume.get() * ini.GetNum("SoundVolume","BattleMusic", 100));
	this->DangerBattleMusic.ChangeVolume(this->MasterVolume.get() * ini.GetNum("SoundVolume","DangerMusic", 100));
	this->LastSpurtBattleMusic.ChangeVolume(this->MasterVolume.get() * ini.GetNum("SoundVolume","LastSpurt", 100));
	this->PlayerSideAttackSound.ChangeVolume(this->MasterVolume.get() * ini.GetNum("SoundVolume","PlayerSideAttack", 100));
	this->EnemySideAttackSound.ChangeVolume(this->MasterVolume.get() * ini.GetNum("SoundVolume","EnemySideAttack", 100));
	this->MagicCastSound.ChangeVolume(this->MasterVolume.get() * ini.GetNum("SoundVolume","MagicCast", 100));
	this->ChangeMusic(PlayerSideCharacterList, EnemyMonster);
}
