﻿#include "Program.hpp"
#include "IsPlayerJudgement.hpp"
#include "StatusRawData.hpp"
#include "PropertiesRead.hpp"
#include "XmlRead.hpp"
#include "Win32API.hpp"
#include "IsPlayerJudgement.hpp"

static inline size_t to_size_t(const std::string& str) {
#ifdef WIN64
	return std::stoull(str);
#else
	return std::stoul(str);
#endif
}

static inline size_t GetExp(const std::string& Tag) {
	try {
		return FileExists(Program::SaveDataFilePath) ? to_size_t(PropertiesRead(Program::SaveDataFilePath).GetData(Tag)) : 0;
	}
	catch (...) {
		return 0;
	}
}

StatusRawData::StatusRawData(const std::string Name, const std::string Tag, const std::string AIDllPath, const int SexID, const int HP, const int MP, const int Attack, 
	const int Defence, const int MagicAttack, const int MagicDefence, const int MagicCure, const int Speed, const int Cleverness, const std::string Element1, const std::string Element2)
	: Name(Name), Tag(Tag), ExternalAIDllPath(AIDllPath), SexID(SexID), HP(HP), MP(MP), Attack(Attack), Defence(Defence), MagicAttack(MagicAttack), MagicDefence(MagicDefence), MagicCure(MagicCure),
	Speed(Speed), Cleverness(Cleverness), LevelUpBorderPointList(GetLevelUpBorderPointList(IsPlayer(Tag) ? "player" : Tag)), CurrentExp(GetExp(IsPlayer(Tag) ? "player" : Tag)),
	Element1(Element1), Element2(Element2) {}

StatusRawData StatusRawData::operator + (const StatusRawData& r) const {
	if (r.Tag != this->Tag) throw std::runtime_error("Identified tag is wrong.");
	return StatusRawData(
		this->Name, this->Tag, this->ExternalAIDllPath, this->SexID, this->HP + r.HP, this->MP + r.MP,
		this->Attack + r.Attack, this->Defence + r.Defence, this->MagicAttack + r.MagicAttack,
		this->MagicDefence + r.MagicDefence, this->MagicCure + r.MagicCure, this->Speed + r.Speed,
		this->Cleverness + r.Cleverness, this->Element1, this->Element2
	);
}
