﻿#ifndef __COLOR_HPP__
#define __COLOR_HPP__
#include "KgDxColor.hpp"

class Color {
public:
	static KgDxGraphic::Color Gray;
	static KgDxGraphic::Color White;
	static KgDxGraphic::Color Green;
	static KgDxGraphic::Color Red;
	static KgDxGraphic::Color Blue;
	static KgDxGraphic::Color Orange;
	static KgDxGraphic::Color Yellow;
	static KgDxGraphic::Color Black;
	static KgDxGraphic::Color Cyan;
	static KgDxGraphic::Color Pink;
};
#endif
