﻿#ifndef __MESSAGE_HPP__
#define __MESSAGE_HPP__
#include "KgDxGraphic.hpp"
#include "ref.hpp"
#include "DllFunctionCallManager.hpp"
#include <functional>

class Message {
private:
	std::reference_wrapper<DllFunctionCallManager> DllFuncMgr;
	std::reference_wrapper<KgDxGraphic::ThrowException::String> StringHandle;
	template<typename T> T GetFunc(const std::string FunctionName) const {
		return this->DllFuncMgr.get().GetFunctionAddress<T>(FunctionName);
	}
public:
	Message(DllFunctionCallManager& DllFuncManager, KgDxGraphic::ThrowException::String& string)
		: DllFuncMgr(DllFuncManager), StringHandle(string) {}
	void IntroductEnemy(const int X, const int Y, const std::string Name) const;
	void Attack(const int X, const int Y, const std::string Name) const;
	void EngageAttack(const int X, const int Y, const std::string Name) const;
	void CastSpell(const int X, const int Y, const std::string Name, const std::string SkillName, const bool IgnorePlayerInstructionFlag) const;
	void EngageMagic(const int X, const int Y, const std::string Name, const std::string SkillName) const;
	void DamageToEnemy(const int X, const int Y, const std::string EnemyName, const int DamagePoint, const bool Critical) const;
	void Crash(const int X, const int Y, const std::string CrasherName, const int DamagePoint, const bool Critical) const;
	void RangeCrash(const int X, const int Y, const std::string CrasherName, const int DamagePoint) const;
	void Cure(const int X, const int Y, const std::string HealedCharacterName, const int CurePoint) const;
	void RangeCure(const int X, const int Y, const std::string HealerName, const int CurePoint) const;
	void Ressurection(const int X, const int Y, const std::string ResuscitationCharacter, const bool Succeeded) const;
	void LowMP(const int X, const int Y) const;
	void Dead(const int X, const int Y, const std::string DeadCharacterName) const;
	void Defeat(const int X, const int Y, const std::string EnemyName) const;
	void Defence(const int X, const int Y, const std::string DefenceCharacter) const;
};
#endif
