﻿#ifdef _DEBUG
#include "BattleSimulator.hpp"
#include "Color.hpp"

void BattleSimulator::GraphNumGraphField(const int X, const int Y) const {
	this->NumModeParameterGraphField.Print(X, Y);
}

void BattleSimulator::GraphEnemyCharacterParameter() const {
	Program::GameMasterMode && GraphNumMode
		? BaseBattleCreateImpl::GraphEnemyCharacterParameterWithNumMode(WindowWidth - 220, 0, this->Characters[3])
		: BaseBattleCreateImpl::GraphEnemyCharacterParameterWithGaugeMode(470, 1, this->Characters[3]);
}

void BattleSimulator::GraphParameterWithNumMode(const int X, const int Y) const {
	static const time_t Start = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
	this->GraphNumGraphField(X, Y);
	BaseBattleCreateImpl::GraphPlayerSideCharacterParameterWithNumMode(X + 1, Y + 1, this->Characters[0]);
	BaseBattleCreateImpl::GraphPlayerSideCharacterParameterWithNumMode(X + 2 + this->NameWidthWithNumMode, Y + 1, this->Characters[1]);
	BaseBattleCreateImpl::GraphPlayerSideCharacterParameterWithNumMode(X + 3 + this->NameWidthWithNumMode * 2, Y + 1, this->Characters[2]);
	if ((std::chrono::system_clock::to_time_t(std::chrono::system_clock::now()) - Start) % 2 == 0) {
		KgDxGraphic::NoExcept::ChangeCleannessLevel(100);
		this->StringHandle.get().Print(X, Program::StringSize * 5 + 3, "●REC(シミュレーションモード)", Color::Red);
		KgDxGraphic::NoExcept::ChangeCleannessLevel(70);
	}
}

void BattleSimulator::CommonGraphic(const KgDxGraphic::NoExcept::Picture& EnemyGraphicInfo) const {
	KgDxGraphic::NoExcept::ChangeCleannessLevel(100);
	this->GraphBackground();
	this->GraphEnemy(EnemyGraphicInfo);
	KgDxGraphic::NoExcept::ChangeCleannessLevel(70);
	this->GraphPlayerSideCharacterParameter();
	this->GraphEnemyCharacterParameter();
}
#endif
