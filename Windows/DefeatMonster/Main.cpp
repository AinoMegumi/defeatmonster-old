﻿#include "TitleInfo.hpp"
#include <Windows.h>
#include "Program.hpp"
#include "KgWinException.hpp"
#include "LanguageRuntimeFunctionType.hpp"
#include "DllFunctionCallManager.hpp"
#include "CommandLineManager.hpp"
#include "Launcher.hpp"
#include "Setup.hpp"
#include "BattleSimulator.hpp"
#include "XmlRead.hpp"
#include "Random.hpp"
#include "KgDxSound.hpp"
#include "RegistryRead.hpp"
enum class Menu { Launch, Title, Game, End };

Menu GameMain(const int Control, KgDxGraphic::ThrowException::String& StringHandle, int& MasterVolume, const std::string& SavedGamesFolder);
void InitializeComponent(LPSTR lpCmdLine);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE, LPSTR lpCmdLine, int) {
	Program::hInstance = hInstance;
	try {
		Menu MenuInfo = Menu::Launch;
		InitializeComponent(lpCmdLine);
		const std::string SavedGamesFolder = RegistryRead(HKEY_CURRENT_USER, "Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Shell Folders").ReadString("{4C5C32FF-BB9D-43B0-B5B4-2D72E54EAAA4}");
#ifdef _DEBUG
		Program::MessageBox("デバッグモード中は経験値は入りません", Title, MB_OK);
#else
#ifdef TPRELEASE
		if (Program::MessageBox("このクライアントはTechnical Previewのため、何かしらの不具合がありますのでご了承いただける方のみ遊んでください。\nよろしいですか？", Title, MB_ICONWARNING | MB_YESNO) == IDNO)
			throw GameEnd(1);
#endif
		if (Program::GameMasterMode) {
			if (Program::MessageBox("ゲームマスターモードで起動しています。\n続けますか？\n※ゲームマスターモードの場合、オフラインモンスターの強さが上がりますが、経験値は増えません。", Title, MB_ICONWARNING | MB_YESNO) == IDNO)
				throw GameEnd(1);
		}
#endif
		const KgDxSystem::System MainInfo(Title, WindowWidth, WindowHeight, true, KgDxGraphic::Color(TEXT("#99D9EA")), TEXT("#ffffff"));
		KgDxGraphic::ThrowException::String StringHandle(
			Program::ChangeFullPath(Program::RegistryReader::RegReadRootString("Font", "C:\\Windows\\Fonts\\msgothic.ttc")),
			Program::StringSize
		);
		int MasterVolume =
#ifdef _DEBUG
		100;
#else
		Program::RegistryReader::RegReadSoundVolume("Master", 100);
#endif
		int Control = 0;
		bool EndFlag = false;
		CommonGraphicForBattle::NameWidthWithGaugeMode = StringHandle.GetPrintStringWidth("あああああ");
		CommonGraphicForBattle::NameWidthWithNumMode = StringHandle.GetPrintStringWidth("ああああああ");
		while (Program::LoopCondition() && !EndFlag) {
			switch (MenuInfo) {
			case Menu::Launch:
				if (!Program::GameMasterMode) Launch();
				MenuInfo = Menu::Title;
				break;
			case Menu::Title: {
				Control = StartMenu(StringHandle);
				if (Control == 3 || Control == 4) {
					static const auto f = DllFunctionCallManager(Program::LanguageDllName).GetFunctionAddress<LanguageRuntimeFuncType::InPreparation>("InPreparation");
					f();
				}
				else if (Control == 5) EndFlag = true;
				else MenuInfo = Menu::Game;
				break;
			}
			case Menu::Game:
				MenuInfo = GameMain(Control, StringHandle, MasterVolume, SavedGamesFolder);
				break;
			default:
				throw KgWinException("Menu is wrong.");
			}
		}
		MainInfo.End();
		return 0;
	}
	catch (const std::exception& er) {
		Program::MessageBox(er.what(), Title, MB_ICONERROR | MB_OK);
	}
	catch (const KgWinException& kex) {
		Program::MessageBox(kex.what(), Title, MB_ICONERROR | MB_OK);
	}
	catch (const GameEnd& end) {
		return end.exit();
	}
	catch (...) {
		Program::MessageBox("不明なエラーが発生しました。", Title, MB_ICONERROR | MB_OK);
	}
	return 1;
}

inline std::array<Status, 4> SetOfflineModeAllCharacter(const std::string& SavedGamesFolder, const bool UserDataLoad) {
	CreateAllCharacterStatus(2);
	std::array<Status, 4> Characters;
	Characters[0] = GetPlayerStatus();
	const auto Partner = ReadPartnerData(SavedGamesFolder, UserDataLoad);
	Characters[1] = GetPartnerStatus("fpartner", Partner);
	Characters[2] = GetPartnerStatus("spartner", Partner);
	Characters[3] = ChooseEnemy(std::max({ Characters[0].Level.GetCurrentLevel(), Characters[1].Level.GetCurrentLevel(), Characters[2].Level.GetCurrentLevel() }), SavedGamesFolder, UserDataLoad);
	return Characters;
}

static inline void ChangeSettingScreen() {
	KgDxGraphic::NoExcept::Picture p(Program::ChangeFullPath(".\\Graphic\\Background\\settings.png"));
	p.Load();
	KgDxGraphic::NoExcept::Clear();
	p.Print(0, 0, WindowWidth, WindowHeight, false);
	KgDxGraphic::NoExcept::Flip();
}

inline std::string GetBattleMusicFilePathFromRegistry() {
	try { return Program::ChangeFullPath(Program::RegistryReader::RegReadSoundFile("BattleMusic", ".\\Sound\\Music\\battle.mp3")); }
	catch (...) { return Program::ChangeFullPath(".\\Sound\\Music\\battle.mp3"); }
}

template<typename RetType, class BattleClass>
RetType OfflineBattleMain(KgDxGraphic::ThrowException::String& StringHandle, int& MasterVolume, const std::string& SavedGamesFolder) {
	KgDxSound::NoExcept::Sound Setting(Program::ChangeFullPath(".\\Sound\\Music\\shutsujin.mp3"));
	KgDxSound::NoExcept::Sound BattleMusic(GetBattleMusicFilePathFromRegistry());
	Setting.Load();
	BattleMusic.Load();
	Setting.ChangeVolume(MasterVolume);
	BattleMusic.ChangeVolume(MasterVolume * 70 / 100);
	Setting.Play(KgDxSound::PlayType::Loop);
	ChangeSettingScreen();
	bool Flag = false;
	if (const bool Flag1 = FileExists(SavedGamesFolder + "\\Kamioda Games\\Kamioda Warrior\\DefeatMonster\\partner.xml"),
		Flag2 = FileExists(SavedGamesFolder + "\\Kamioda Games\\Kamioda Warrior\\DefeatMonster\\enemy.xml");
		Flag1 || Flag2) {
		Flag = (IDYES == Program::MessageBox("ユーザーのパートナーデータとモンスターデータを読み込みますか？", Title, MB_ICONINFORMATION | MB_YESNO));
	}
	std::array<Status, 4> Characters = SetOfflineModeAllCharacter(SavedGamesFolder, Flag);
	SetStrategy(Characters[1].Name, Characters[2].Name);
	DllFunctionCallManager DllFuncManager(Program::LanguageDllName);
	int FirstPartnerBattleStyle;
	int SecondPartnerBattleStyle;
	LoadStrategy(FirstPartnerBattleStyle, SecondPartnerBattleStyle);
	Setting.Stop();
	KgDxGraphic::NoExcept::Picture Background(Program::ChangeFullPath(".\\Graphic\\Background\\background.png"));
	const std::string EnemyGraph = FileExists(Program::ChangeFullPath(".\\Graphic\\Enemy\\" + Characters[3].IdentifiedTag + ".png"))
		? Program::ChangeFullPath(".\\Graphic\\Enemy\\" + Characters[3].IdentifiedTag + ".png")
		: Program::ChangeFullPath(SavedGamesFolder + "\\Kamioda Games\\Kamioda Warrior\\DefeatMonster\\Graphic\\Enemy\\" + Characters[3].IdentifiedTag + ".png");
	Background.Load();
	BattleClass bat(Characters, Background, EnemyGraph, StringHandle, DllFuncManager, MasterVolume);
	const RetType result = static_cast<RetType>(bat.main(FirstPartnerBattleStyle, SecondPartnerBattleStyle, SavedGamesFolder, Flag));
	BattleMusic.Stop();
	return result;
}

void NormalBattleMain(KgDxGraphic::ThrowException::String& StringHandle, int& MasterVolume, const std::string& SavedGamesFolder) {
	// 勝ち：2　負け：0　引き分け：1
	const int Result = OfflineBattleMain<int, Offline::NormalBattle>(StringHandle, MasterVolume, SavedGamesFolder);
	UNREFERENCED_PARAMETER(Result);
}

void ContinuousDefeatMain(KgDxGraphic::ThrowException::String& StringHandle, int& MasterVolume, const std::string& SavedGamesFolder) {
	const int Result = OfflineBattleMain<int, Offline::ContinuousDefeat>(StringHandle, MasterVolume, SavedGamesFolder);
	UNREFERENCED_PARAMETER(Result);
}

void TimeAttackMain(KgDxGraphic::ThrowException::String& StringHandle, int& MasterVolume, const std::string& SavedGamesFolder) {
	// 勝ち：所要時間　負け：0　引き分け：1
	const time_t Result = OfflineBattleMain<time_t, Offline::TimeAttack>(StringHandle, MasterVolume, SavedGamesFolder);
	UNREFERENCED_PARAMETER(Result);
}

void BattleTwoPlayerMain(KgDxGraphic::ThrowException::String& StringHandle, const int MasterVolume) {
	Program::MessageBox(Program::LangIniFile.GetString("System", "Ready", "This mode is being prepared").c_str(), Title, MB_OK);
}

void BattleBetweenPlayersMain(KgDxGraphic::ThrowException::String& StringHandle, const int MasterVolume) {
	Program::MessageBox(Program::LangIniFile.GetString("System", "Ready", "This mode is being prepared").c_str(), Title, MB_OK);
}

#ifdef _DEBUG
void SimulatorMain(KgDxGraphic::ThrowException::String& StringHandle, int& MasterVolume) {
	std::array<Status, 4> Characters{};
	DllFunctionCallManager DllFuncManager(Program::LanguageDllName);
	KgDxGraphic::NoExcept::Picture Background(Program::ChangeFullPath(".\\Graphic\\Background\\background.png"));
	Background.Load();
	BattleSimulator bat(Characters, Background, StringHandle, DllFuncManager, MasterVolume);
	const int WinCount = bat.main();
	Program::MessageBox(("全てのケースの検証が終了しました。\n勝利したパターン:" + std::to_string(WinCount) + "パターン").c_str(), "シミュレーションモード", (WinCount == 0 ? MB_ICONWARNING : MB_ICONINFORMATION) | MB_OK);
}
#endif

Menu GameMain(const int Control, KgDxGraphic::ThrowException::String& StringHandle, int& MasterVolume, const std::string& SavedGamesFolder) {
	switch (Control) {
		case 0: // 通常対戦
			NormalBattleMain(StringHandle, MasterVolume, SavedGamesFolder);
			break;
		case 1: // 連続討伐
			ContinuousDefeatMain(StringHandle, MasterVolume, SavedGamesFolder);
			break;
		case 2: // タイムアタック
			TimeAttackMain(StringHandle, MasterVolume, SavedGamesFolder);
			break;
		case 3: // 協力対戦
			BattleTwoPlayerMain(StringHandle, MasterVolume);
			break;
		case 4: // プレイヤー対戦
			BattleBetweenPlayersMain(StringHandle, MasterVolume);
			break;
		case 5: // ゲーム終了
			return Menu::End;
#ifdef _DEBUG
		case 6:
			SimulatorMain(StringHandle, MasterVolume);
			break;
#endif
		default:
			break;
	}
	KgDxGraphic::NoExcept::ChangeCleannessLevel(100);
	return Menu::Title;
}

#include "Win32API.hpp"

bool BootWithGameMasterMode() {
	if (Program::RegistryReader::RegReadRootString("Mode", "User") != "GameMaster") return false;
	std::string Password = "e|)|cCe$dKGYIaA$KK|Y";
	for (auto& i : Password) i ^= 17;
	try {
		const std::string ReadPasswordData = Program::RegistryReader::RegReadRootString("GMPassword", "");
		if (Password != ReadPasswordData) throw std::exception();
		return true;
	}
	catch (...) {
		KgDxSystem::System PasswordForm("ゲームマスターモードのパスワード入力", 640, 36, true, KgDxGraphic::Color("#000000"), KgDxGraphic::Color("#ffffff"));
		do {
			KgDxGraphic::NoExcept::Clear();
			KgDxGraphic::NoExcept::String::NoFontPrint(1, 1, "ゲームマスターモードのパスワードを入力してください。", KgDxGraphic::Color("#ffffff"));
			KgDxGraphic::NoExcept::Flip();
			const std::string InputData = KgDxInput::BasicInput::InputString(1, 19, 30, false);
			if (InputData == Password) return true;
		} while (Program::MessageBox("パスワードが違います", Title, MB_ICONINFORMATION | MB_RETRYCANCEL) == IDRETRY);
		PasswordForm.End();
		return false;
	}
}

void InitializeComponent(LPSTR lpCmdLine) {
	Program::InitializeProgramInfo();
	Program::StringSize = static_cast<int>(Program::RegistryReader::RegReadRootNumber("StringSize", 16));
#ifdef TPRELEASE
	UNREFERENCED_PARAMETER(lpCmdLine);
	Program::Language = "ja-jp_kanji";
	Program::LanguageDllName = Program::ChangeFullPath(".\\Language\\" + Program::Language + "\\lang.dll");
	Program::LangIniFile = IniRead(Program::ChangeFullPath(".\\Language\\" + Program::Language + "\\lang.ini"));
	Program::SaveDataFilePath = Program::ChangeFullPath(".\\dmsvdata.dat");
	Program::Fps = 60;
#else
	try {
		CommandLineManager cmd(lpCmdLine);
		Program::Language = cmd.GetCommandLineData("/lang");
		Program::LanguageDllName = Program::ChangeFullPath(".\\Language\\" + Program::Language + "\\lang.dll");
		Program::LangIniFile = IniRead(Program::ChangeFullPath(".\\Language\\" + Program::Language + "\\lang.ini"));
		Program::SaveDataFilePath = cmd.GetCommandLineData("/savepath") + "\\dmsvdata.dat";
		Program::Fps = KgDxSystem::FPSManager(std::stoi(cmd.GetCommandLineData("/fps")));
	}
	catch (...) {
		throw std::runtime_error(
			"This software needs to be started with the dedicated boot manager to this game.\n"
			"The boot manager can be started by game launcher included in "
			"\"Kamioda Games Software Total Management Service Package\"."
		);
	}
#endif
	Program::PrintInKanji = Program::LanguageDllName.find("hiragana") == std::string::npos;
#ifdef _DEBUG
	Program::GameMasterMode = true;
#else
	Program::GameMasterMode = BootWithGameMasterMode();
#endif
	BaseBattleCreateImpl::LoadBasicInfo();
}
