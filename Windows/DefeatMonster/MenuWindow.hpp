﻿#ifndef __MENUWINDOW_HPP__
#define __MENUWINDOW_HPP__
#include "KgDxGraphic.hpp"

class MenuWindow {
private:
	KgDxGraphic::NoExcept::Box Main;
	KgDxGraphic::NoExcept::Box Frame;
public:
	MenuWindow() = default;
	MenuWindow(const int Width, const int Height, const KgDxGraphic::Color WindowColor, const KgDxGraphic::Color FrameColor);
	void Print(const int X, const int Y) const;
	int Width, Height;
};

#endif
