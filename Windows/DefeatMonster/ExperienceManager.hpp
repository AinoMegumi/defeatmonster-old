﻿#ifndef __EXPERIENCEMANAGER_HPP__
#define __EXPERIENCEMANAGER_HPP__
#include "BodyParameter.hpp"


class ExperienceManager {
private:
	standard::number<size_t> DeadCount;
	standard::number<size_t> ElementalAdvantageAttackCount;
	standard::number<size_t> ElementalInferiorityAttackCount;
public:
	ExperienceManager();
	void AddDeadCount();
	void AddElementalAdvantageAttackCount();
	void AddElementalInferiorityAttackCount();
	size_t CalcExperience(const size_t BaseExp, const BodyParameter<int>& HPRef) const noexcept;
	void Reset();
};
#endif
