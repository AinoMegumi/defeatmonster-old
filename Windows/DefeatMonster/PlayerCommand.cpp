﻿#include "TitleInfo.hpp"
#include "InRangeJudgement.hpp"
#include "Program.hpp"
#include "Color.hpp"
#include "AddVector.hpp"
#include "PlayerCommand.hpp"
#include "LanguageRuntimeFunctionType.hpp"
#include "Battle.hpp"

static KgDxGraphic::Color GetNameGraphColor(const Status& sta) {
	return sta.Dead() ?  Color::Red : Color::White;
}

namespace Offline {
	PlayerCommand::PlayerCommand(const size_t CharacterID, std::array<Status, 4> &Characters, DllFunctionCallManager& DllFunction, KgDxGraphic::Color& CursorColor,
		KgDxGraphic::ThrowException::String& StringHandle, int& NameWidth)
		: CharacterID(CharacterID), CharacterInfo(Characters), DllFuncMgr(DllFunction), CursorColor(CursorColor), StringHandle(StringHandle), NameWidth(NameWidth),
		MainCommandWindow(StringHandle.GetPrintStringWidth("あああああはどうする？") + 2, Program::StringSize * 4 + 2, Color::Gray, Color::White), 
		MagicCommandWindow(500, Program::StringSize * 6 + 2, Color::Gray, Color::White),
		TargetCommandWindow(NameWidth + 2, Program::StringSize * 3 + 2, Color::Gray, Color::White) {}

	void PlayerCommand::GraphMagicList(const int X, const int Y, const std::array<SkillA, 3> GraphMagic) const {
		for (int i = 0; i < 3; i++)
			this->StringHandle.get().Print(X, Y + Program::StringSize * i, GraphMagic[i].Name,  i == 1 ? Color::Black : Color::White);
	}

	void PlayerCommand::GraphMagicList(const int X, const int Y, const int CursorPoint, const std::vector<SkillA> MagicList) const {
		const auto UseMPGraphicFunc = this->DllFuncMgr.get().GetFunctionAddress<LanguageRuntimeFuncType::GraphUseMP>("GraphUseMP");
		this->MagicCommandWindow.Print(X, Y);
		// this->MagicCommandCursorControl.ChangeChooseNum(static_cast<int>(MagicList.size()));
		KgDxGraphic::NoExcept::Box(this->MagicCommandWindow.Width - 2, Program::StringSize, this->CursorColor.get()).Print(X + 1, Y + 1 + Program::StringSize, true);
		this->GraphMagicList(X + 1, Y + 1, [MagicList, CursorPoint]() {
			std::array<SkillA, 3> Arr;
			if (MagicList.size() > 1) Arr[0] = MagicList[CursorPoint - 1 < 0 ? static_cast<int>(MagicList.size()) - 1 : CursorPoint - 1];
			Arr[1] = MagicList[CursorPoint];
			if (MagicList.size() > 2) Arr[2] = MagicList[CursorPoint + 1 > static_cast<int>(MagicList.size()) - 1 ? 0 : CursorPoint + 1];
			return Arr;
		}());
		this->StringHandle.get().Print(X + 1, Y + 1 + Program::StringSize * 4, MagicList[CursorPoint].Description, Color::White);
		this->StringHandle.get().Print(X + 1, Y + 1 + Program::StringSize * 5, GetStringFromDllFunc([MagicList, CursorPoint, UseMPGraphicFunc](char Buf[1024]) { 
			UseMPGraphicFunc(Buf, MagicList[CursorPoint].UseMP); 
		}), Color::White);
	}

	void PlayerCommand::GraphMainCommand(const int X, const int Y, const int CursorPoint, const size_t MagicNum) const {
		this->MainCommandWindow.Print(X, Y);
		static const std::array<std::string, 3> Arr = []() {
			static const std::string TagList[3] = { "AttackCommand", "MagicCommand", "DefenceCommand" };
			std::array<std::string, 3> data;
			for (int i = 0; i < 3; i++) data[i] = Program::LangIniFile.GetString("BattleMenu", TagList[i], "");
			return data;
		}();
		auto ChooseStringColor = [CursorPoint](const int MenuID) { return CursorPoint == MenuID ? Color::Black : Color::White; };
		KgDxGraphic::NoExcept::Box(this->MainCommandWindow.Width - 2, Program::StringSize, this->CursorColor.get()).Print(X + 1, Y + 1 + Program::StringSize * (CursorPoint + 1), true);
		this->StringHandle.get().Print(X + 1, Y + 1, (this->CharacterInfo[this->CharacterID].Name + "はどうする？").c_str(), Color::White);
		this->StringHandle.get().Print(X + 1, Y + 1 + Program::StringSize, Arr.at(0), ChooseStringColor(0));
		this->StringHandle.get().Print(X + 1, Y + 1 + Program::StringSize * 2, Arr.at(1), MagicNum == 0 ? Color::Red : ChooseStringColor(1));
		this->StringHandle.get().Print(X + 1, Y + 1 + Program::StringSize * 3, Arr.at(2), ChooseStringColor(2));
	}

	void PlayerCommand::GraphTargetList(const int X, const int Y, const int CursorPoint) const {
		this->TargetCommandWindow.Print(X, Y);
		KgDxGraphic::NoExcept::Box(this->TargetCommandWindow.Width - 2, Program::StringSize, this->CursorColor.get()).Print(X + 1, Y + 1 + Program::StringSize * CursorPoint, true);
		this->StringHandle.get().Print(X + 1, Y + 1, this->CharacterInfo[0].Name, CursorPoint == 0 ? Color::Black : GetNameGraphColor(this->CharacterInfo[0]));
		this->StringHandle.get().Print(X + 1, Y + 1 + Program::StringSize, this->CharacterInfo[1].Name, CursorPoint == 1 ? Color::Black : GetNameGraphColor(this->CharacterInfo[1]));
		this->StringHandle.get().Print(X + 1, Y + 1 + Program::StringSize * 2, this->CharacterInfo[2].Name, CursorPoint == 2 ? Color::Black : GetNameGraphColor(this->CharacterInfo[2]));
	}

	void PlayerCommand::UpdateMemberInfo() {
		this->MainCommandWindow = std::move(MenuWindow(Program::StringSize * 3 + 2, Program::StringSize * 3 + 2, Color::Gray, Color::White));
		this->MagicCommandWindow = std::move(MenuWindow(500, Program::StringSize * 6 + 2, Color::Gray, Color::White));
		this->TargetCommandWindow = std::move(MenuWindow(this->NameWidth.get() + 2, Program::StringSize * 3 + 2, Color::Gray, Color::White));
	}
}
