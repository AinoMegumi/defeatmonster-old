﻿#ifndef __PLAYERCOMMAND_HPP__
#define __PLAYERCOMMAND_HPP__
#include "ref.hpp"
#include "DllFunctionCallManager.hpp"
#include "Skill.hpp"
#include "MenuWindow.hpp"
#include "Cursor.hpp"
#include "Status.hpp"
#include <functional>

namespace Offline {
	class PlayerCommand {
	public:
		size_t CharacterID;
	private:
		standard::ArrayRef<Status, 4> CharacterInfo;
		std::reference_wrapper<DllFunctionCallManager> DllFuncMgr;
		std::reference_wrapper<KgDxGraphic::Color> CursorColor;
		std::reference_wrapper<KgDxGraphic::ThrowException::String> StringHandle;
		std::reference_wrapper<int> NameWidth;
	private:
		MenuWindow MainCommandWindow;
		MenuWindow MagicCommandWindow;
		MenuWindow TargetCommandWindow;
		void GraphMagicList(const int X, const int Y, const std::array<SkillA, 3> GraphMagic) const;
	public:
		PlayerCommand(const size_t CharacterID, std::array<Status, 4> &Characters, DllFunctionCallManager& DllFunction, KgDxGraphic::Color& CursorColor, KgDxGraphic::ThrowException::String& StringHandle, int& NameWidth);
		void GraphMagicList(const int X, const int Y, const int CursorPoint, const std::vector<SkillA> MagicList) const;
		void GraphMainCommand(const int X, const int Y, const int CursorPoint, const size_t MagicNum) const;
		void GraphTargetList(const int X, const int Y, const int CursorPoint) const;
		void UpdateMemberInfo();
	};
}
#endif
