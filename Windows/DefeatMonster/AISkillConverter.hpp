﻿#ifndef __SKILLCONVERTER_HPP__
#define __SKILLCONVERTER_HPP__
#include "AISkill.h"
#include "Skill.hpp"

inline AISkill ToAISkill(const SkillA& skill) {
	return {
		skill.Tag.c_str(),
		skill.UseMP,
		skill.BasePower,
		static_cast<unsigned int>(static_cast<size_t>(skill.SkillElement)),
		skill.Range,
		skill.ResurrectionEffect,
		skill.SkillType
	};
}

inline AISkillList CreateAISkillList(const std::vector<SkillA>& MagicList){
	AISkillList list{};
	list.SkillList = new AISkill[MagicList.size()];
	for (size_t i = 0; i < MagicList.size(); i++) list.SkillList[i] = ToAISkill(MagicList[i]);
	list.SkillNum = static_cast<unsigned int>(MagicList.size());
	return list;
}
#endif
