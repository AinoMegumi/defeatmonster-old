﻿#ifndef __LANGUAGERUNTIMEFUNCTIONTYPE_HPP__
#define __LANGUAGERUNTIMEFUNCTIONTYPE_HPP__
#include "KgDxGraphic.hpp"
#include <Windows.h>

namespace LanguageRuntimeFuncType {
	typedef void (WINAPI *IntroductEnemy)(char[1024], const char*);
	typedef IntroductEnemy Attack;
	typedef IntroductEnemy EngageAttack;
	typedef void (WINAPI *CastSpell)(char[1024], const char*, const char*, const bool);
	typedef void (WINAPI *EngageMagic)(char[1024], const char*, const char*);
	typedef void (WINAPI *DamageToEnemy)(char[1024], const char*, const int, const bool);
	typedef DamageToEnemy Crash;
	typedef void (WINAPI *RangeCrash)(char[1024], const char*, const int);
	typedef RangeCrash Cure;
	typedef RangeCrash RangeCure;
	typedef void (WINAPI *Ressurection)(char[1024], const char*, const bool);
	typedef void (WINAPI *LowMP)(char[1024]);
	typedef void (WINAPI *GraphUseMP)(char[1024], const int);
	typedef IntroductEnemy Dead;
	typedef IntroductEnemy Defeat;
	typedef IntroductEnemy Defence;
	typedef void (WINAPI *Win)();
	typedef Win Lose;
	typedef Win Draw;
	typedef Win InPreparation;
}
#endif
