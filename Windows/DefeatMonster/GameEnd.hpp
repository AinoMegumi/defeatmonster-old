﻿#ifndef __GAMEEND_HPP__
#define __GAMEEND_HPP__
class GameEnd {
private:
	int ExitCode;
public:
	GameEnd() : GameEnd(0) {}
	GameEnd(const int ExitCode) : ExitCode(ExitCode) {}
	int exit() const noexcept { return this->ExitCode; }
};
#endif
