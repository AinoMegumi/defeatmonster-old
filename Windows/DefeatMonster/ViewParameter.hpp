﻿#ifndef __VIEWPARAMETER_HPP__
#define __VIEWPARAMETER_HPP__
#include "PossibleChangeStatusArrange.hpp"
#include <functional>

template<typename T, std::enable_if_t<std::is_integral<T>::value&& std::is_signed<T>::value, std::nullptr_t> = nullptr>
class ViewParameter {
public:
	PossibleChangeStatusArrange<T> GraphParameter;
private:
	std::reference_wrapper<PossibleChangeStatusArrange<T>> RealParameter;
	T NotApplyDamageToGraph;
	T ApplySpeed;
public:
	ViewParameter(PossibleChangeStatus<T>& Real) : ViewParameter(Real, Real) {}
	ViewParameter(PossibleChangeStatus<T>& Real, const PossibleChangeStatus<T>& Graph)
		: RealParameter(Real), GraphParameter(Graph), NotApplyDamageToGraph(*Graph - *Real), ApplySpeed(0) {}
	void ApplyDamage() {
		this->NotApplyDamageToGraph = this->GraphParameter.Get() - this->RealParameter.get().Get();
		if (this->NotApplyDamageToGraph == 0) return;
		else if (this->NotApplyDamageToGraph > 0) this->ApplySpeed = std::max(std::max(1, this->NotApplyDamageToGraph / 20), this->ApplySpeed);
		else this->ApplySpeed = std::min(std::min(-1, this->NotApplyDamageToGraph / 20), this->ApplySpeed);
		this->GraphParameter -= this->ApplySpeed;
		this->NotApplyDamageToGraph -= this->ApplySpeed;
		if (this->NotApplyDamageToGraph == 0) this->ApplySpeed = 0;
	}
	void SpeedApply() {
		this->GraphParameter = this->RealParameter.get();
		this->NotApplyDamageToGraph = 0;
	}
};
#endif
