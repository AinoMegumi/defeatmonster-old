﻿#ifndef __INPUT_HPP__
#define __INPUT_HPP__
#include "KgDxInput.hpp"
#include <utility>

enum class KeyID { UpKey = 0, DownKey = 1, LeftKey = 2, RightKey = 3, EnterKey = 4, CancelKey = 5, EngageKey = 6};

class Cursor {
private:
	KgDxInput::Keyboard Key;
	std::array<std::pair<unsigned long, unsigned long>, 7> KeyConfigList;
	bool Pressed(const KeyID KeyPairID) const;
	std::reference_wrapper<int> CursorPoint;
	int Max;
public:
	Cursor(int& CursorVariable, const int ChooseTotal = 2, const bool UseLongPress = false);
	bool Fllush();
	void Update();
	bool Up() const;
	bool Down() const;
	bool Left() const;
	bool Right() const;
	bool Enter() const;
	bool Cancel() const;
	bool UseEngage() const;
	bool GraphParameterWithNum() const;
	void MoveCursor() const;
	void ReloadKeyConfig();
};

class AbleToChangeMaxCursor {
private:
	std::reference_wrapper<KgDxInput::Keyboard> Key;
	std::array<std::pair<unsigned long, unsigned long>, 7> KeyConfigList;
	bool Pressed(const KeyID KeyPairID) const;
	std::reference_wrapper<int> CursorPoint;
	int ChooseTotal;
public:
	AbleToChangeMaxCursor(KgDxInput::Keyboard& KeyInput, int& CursorVariable);
	template<typename T, std::enable_if_t<std::is_integral<T>::value, std::nullptr_t> = nullptr>
	void ChangeChooseTotal(const T NewChooseTotal) { 
		this->ChooseTotal = static_cast<int>(NewChooseTotal);
	}
	void Reset();
	bool Fllush();
	void Update();
	bool Up() const;
	bool Down() const;
	bool Left() const;
	bool Right() const;
	bool Enter() const;
	bool Cancel() const;
	bool UseEngage() const;
	bool ReloadKey() const;
	bool ChangeParameterGraphMode() const;
	bool SpeedApplyDamageInfo() const;
	void MoveCursor() const;
	void ReloadKeyInfo();
#ifdef _DEBUG
	bool ChangeHP(const int SubKey) const;
#endif
};
#endif
