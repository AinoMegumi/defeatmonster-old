﻿#include "Program.hpp"
#include "Message.hpp"
#include "LanguageRuntimeFunctionType.hpp"
#include "Color.hpp"
namespace LRFT = LanguageRuntimeFuncType;

void Message::IntroductEnemy(const int X, const int Y, const std::string Name) const {
	const auto f = this->GetFunc<LRFT::IntroductEnemy>("IntroductEnemy");
	const std::string str = [f, Name]() {
		std::string str;
		str.resize(1024);
		f(&str[0], Name.c_str());
		return str;
	}();
	this->StringHandle.get().Print(X, Y, str, Color::White);
}

void Message::Attack(const int X, const int Y, const std::string Name) const {
	const auto f = this->GetFunc<LRFT::Attack>("Attack");
	const std::string str = [f, Name]() {
		std::string str;
		str.resize(1024);
		f(&str[0], Name.c_str());
		return str;
	}();
	this->StringHandle.get().Print(X, Y, str, Color::White);
}

void Message::EngageAttack(const int X, const int Y, const std::string Name) const {
	const auto f = this->GetFunc<LRFT::EngageAttack>("EngageAttack");
	const std::string str = [f, Name]() {
		std::string str;
		str.resize(1024);
		f(&str[0], Name.c_str());
		return str;
	}();
	this->StringHandle.get().Print(X, Y, str, Color::White);
}

void Message::CastSpell(const int X, const int Y, const std::string Name, const std::string SkillName, const bool IgnorePlayerInstructionFlag) const {
	const auto f = this->GetFunc<LRFT::CastSpell>("CastSpell");
	const std::string str = GetStringFromDllFunc([f, Name, SkillName, IgnorePlayerInstructionFlag](char* s) {
		f(s, Name.c_str(), SkillName.c_str(), IgnorePlayerInstructionFlag);
	});
	this->StringHandle.get().Print(X, Y, str, Color::White);
}

void Message::EngageMagic(const int X, const int Y, const std::string Name, const std::string SkillName) const {
	const auto f = this->GetFunc<LRFT::EngageMagic>("EngageMagic");
	const std::string str = GetStringFromDllFunc([f, Name, SkillName](char* s) {
		f(s, Name.c_str(), SkillName.c_str());
	});
	this->StringHandle.get().Print(X, Y, str, Color::White);
}

void Message::DamageToEnemy(const int X, const int Y, const std::string CrasherName, const int DamagePoint, const bool Critical) const {
	const auto f = this->GetFunc<LRFT::DamageToEnemy>("DamageToEnemy");
	const std::string str = GetStringFromDllFunc([f, CrasherName, DamagePoint, Critical](char* s) {
		f(s, CrasherName.c_str(), DamagePoint, Critical);
	});
	this->StringHandle.get().Print(X, Y, str, Color::White);
}

void Message::Crash(const int X, const int Y, const std::string CrasherName, const int DamagePoint, const bool Critical) const {
	const auto f = this->GetFunc<LRFT::Crash>("Crash");
	const std::string str = GetStringFromDllFunc([f, CrasherName, DamagePoint, Critical](char* s) {
		f(s, CrasherName.c_str(), DamagePoint, Critical);
	});
	this->StringHandle.get().Print(X, Y, str, Color::White);
}

void Message::RangeCrash(const int X, const int Y, const std::string CrasherName, const int DamagePoint) const {
	const auto f = this->GetFunc<LRFT::RangeCrash>("RangeCrash");
	const std::string str = GetStringFromDllFunc([f, CrasherName, DamagePoint](char* s) {
		f(s, CrasherName.c_str(), DamagePoint);
	});
	this->StringHandle.get().Print(X, Y, str, Color::White);
}

void Message::Cure(const int X, const int Y, const std::string HealedCharacterName, const int CurePoint) const {
	const auto f = this->GetFunc<LRFT::Cure>("Cure");
	const std::string str = GetStringFromDllFunc([f, HealedCharacterName, CurePoint](char* s) {
		f(s, HealedCharacterName.c_str(), CurePoint);
	});
	this->StringHandle.get().Print(X, Y, str, Color::White);
}

void Message::RangeCure(const int X, const int Y, const std::string HealerName, const int CurePoint) const {
	const auto f = this->GetFunc<LRFT::RangeCure>("RangeCure");
	const std::string str = GetStringFromDllFunc([f, HealerName, CurePoint](char* s) {
		f(s, HealerName.c_str(), CurePoint);
	});
	this->StringHandle.get().Print(X, Y, str, Color::White);
}

void Message::Ressurection(const int X, const int Y, const std::string ResuscitationCharacter, const bool Succeeded) const {
	const auto f = this->GetFunc<LRFT::Ressurection>("Ressurection");
	const std::string str = GetStringFromDllFunc([f, ResuscitationCharacter, Succeeded](char* s) {
		f(s, ResuscitationCharacter.c_str(), Succeeded);
	});
	this->StringHandle.get().Print(X, Y, str, Color::White);
}

void Message::LowMP(const int X, const int Y) const {
	const auto f = this->GetFunc<LRFT::LowMP>("LowMP");
	const std::string str = GetStringFromDllFunc([f](char* s) {
		f(s);
	});
	this->StringHandle.get().Print(X, Y, str, Color::White);
}

void Message::Dead(const int X, const int Y, const std::string DeadCharacterName) const {
	const auto f = this->GetFunc<LRFT::Dead>("Dead");
	const std::string str = GetStringFromDllFunc([f, DeadCharacterName](char* s) {
		f(s, DeadCharacterName.c_str());
	});
	this->StringHandle.get().Print(X, Y, str, Color::White);
}

void Message::Defeat(const int X, const int Y, const std::string EnemyName) const {
	const auto f = this->GetFunc<LRFT::Defeat>("Defeat");
	const std::string str = GetStringFromDllFunc([f, EnemyName](char* s) {
		f(s, EnemyName.c_str());
	});
	this->StringHandle.get().Print(X, Y, str, Color::White);
}

void Message::Defence(const int X, const int Y, const std::string DefenceCharacter) const {
	const auto f = this->GetFunc<LRFT::Defence>("Defence");
	const std::string str = GetStringFromDllFunc([f, DefenceCharacter](char* s) {
		f(s, DefenceCharacter.c_str());
	});
	this->StringHandle.get().Print(X, Y, str, Color::White);
}
