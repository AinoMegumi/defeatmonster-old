﻿#ifndef __CHARACTERAI_HPP__
#define __CHARACTERAI_HPP__
#define DEFEATMONSTER_CLIENT
#include "Status.hpp"
#include "ref.hpp"
#include "Random.hpp"
#include "ExternalAI.hpp"
#include <functional>
#include <memory>

namespace Offline {
	inline bool IsFoolCommandSelected(const int Cleverness) {
		if (Cleverness >= 750) return false;
		std::bernoulli_distribution rand(std::clamp(static_cast<float>(750 - Cleverness), 0.0f, 750.0f) / 750.0f);
		return rand(Random::GetEngine());
	}

	inline size_t GetMaxDamageIndex(const Status& Target, const Status& Attacker) {
		size_t Index = 0;
		int MaxNum = 0;
		auto& m = Attacker.AbleToUseAttackMagic;
		for (size_t i = 0; i < m.size(); i++) {
			int a = Target.GetMinDamageReceived(Attacker, m[i]);
			if (a <= MaxNum) continue;
			Index = i;
			MaxNum = a;
		}
		return Index;
	}

	class PartnerAI {
	private:
		standard::ArrayRef<Status, 4> Characters;
		int BattleStyle;
	public:
		size_t ID;
	private:
		std::array<size_t, 3> PlayerSide = { 0, 1, 2 };
		SkillA AttackCommand() const;
		SkillA CureCommand(const int CureTargetCharacterNum);
		SkillA Ressurection() const;
		SkillA balance();
		SkillA attackmain();
		SkillA curemain();
		void ChooseCureTarget();
		int CheckCureTargetCharacterNum(const int Border) const;
		const Status& Character() const noexcept;
	public:
		PartnerAI(std::array<Status, 4>& Characters, const int BattleStyle, const size_t PartnerID);
		SkillA command();
		size_t GetTarget() const noexcept;
	};

	class EnemyAI {
	private:
		standard::ArrayRef<Status, 4> Characters;
		std::array<size_t, 3> PlayerSide = { 0, 1, 2 };
		SkillA AttackCommand() const;
		SkillA MagicAttackCommand() const;
		SkillA CureCommand() const;
		void ChooseTarget();
	public:
		EnemyAI(std::array<Status, 4>& Characters);
		SkillA command();
		size_t GetTarget() const;
	};
}
#endif
