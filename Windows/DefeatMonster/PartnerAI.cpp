﻿#include "Program.hpp"
#include "InRangeJudgement.hpp"
#include "CharacterAI.hpp"
#include "Battle.hpp"
#include <numeric>
#include <fstream>

namespace Offline {
	PartnerAI::PartnerAI(std::array<Status, 4>& Characters, const int BattleStyle, const size_t PartnerID)
		: Characters(Characters), BattleStyle(BattleStyle), ID(PartnerID) {}

	SkillA PartnerAI::command() {
		if (this->Characters[this->ID].HP.RealParameter.GetRatioArrange() < 15) return BaseBattleCreateImpl::Guard;
		this->ChooseCureTarget();
		switch (this->BattleStyle) {
			case 0:
				return this->attackmain();
			case 1:
				return this->balance();
			case 2:
				return this->curemain();
			default:
				return BaseBattleCreateImpl::NormalAttack;
		}
	}

	static inline size_t SearchHPLowestPercentageCharacter(const std::array<Status, 4> Characters) {
		size_t Index = 0;
		int MinPercentage = 100;
		for (size_t i = 0; i < 3; i++) {
			if (Characters[i].HP.RealParameter.GetRatioArrange() >= MinPercentage) continue;
			Index = i;
			MinPercentage = Characters[i].HP.RealParameter.GetRatioArrange();
		}
		return Index;
	}

	static inline size_t SearchReservedMagic(const std::vector<SkillA>& MagicList, const std::string Name) {
		for (size_t i = 0; i < MagicList.size(); i++) if (MagicList.at(i).Name == Name) return i;
		return MagicList.size();
	}

	const Status& PartnerAI::Character() const noexcept {
		return this->Characters[this->ID];
	}
	SkillA PartnerAI::AttackCommand() const {
		const Status& s = this->Character();
		if (s.AbleToUseAttackMagic.empty())
			return BaseBattleCreateImpl::NormalAttack;
		if (IsFoolCommandSelected(s.Cleverness.Get())) {
			std::uniform_int_distribution<int> rand(0, 1);
			return rand(Random::GetEngine()) == 0
				? BaseBattleCreateImpl::NormalAttack
				: SelectElementry(s.AbleToUseAttackMagic);
		}
		else {
			if (this->Characters[3].GetMinDamageReceived(s) >= this->Characters[3].HP.RealParameter)
				return BaseBattleCreateImpl::NormalAttack;
			if (s.Physical.AttackParam >= s.Magic.AttackParam)
				return BaseBattleCreateImpl::NormalAttack;
			else return s.AbleToUseAttackMagic.at(GetMaxDamageIndex(this->Characters[3], s));
		}
	}

	static constexpr bool CureTargetCountPlus(const int Ratio, const int Border, const size_t CharacterID) {
		return CharacterID == 0
			? Ratio <= Border
			: Ratio <= Border && Ratio != 0;
	}

	int PartnerAI::CheckCureTargetCharacterNum(const int Border) const {
		int CureTargetCount = 0;
		for (int i = 0; i < 3; i++) {
			if (CureTargetCountPlus(this->Characters[i].HP.RealParameter.GetRatioArrange(), Border, this->ID)) CureTargetCount++;
		}
		return CureTargetCount;
	}

	SkillA PartnerAI::CureCommand(const int CureTargetCharacterNum) {
		if (CureTargetCharacterNum > 1 && !this->Character().AbleToUseRangeCureMagic.empty())  // 範囲回復
			return this->Character().AbleToUseRangeCureMagic.back();
		else { // 単体回復
			while (this->Characters[this->PlayerSide.front()].HP.RealParameter == 0 && Program::LoopCondition()) {
				size_t tmp = this->PlayerSide.at(0);
				this->PlayerSide[0] = this->PlayerSide[1];
				this->PlayerSide[1] = this->PlayerSide[2];
				this->PlayerSide[2] = tmp;
			}
			if (this->Character().AbleToUseSingleCureMagic.back().Name != "リザレクション")
				return this->Character().AbleToUseSingleCureMagic.back();
			else return this->Character().AbleToUseSingleCureMagic.at(this->Character().AbleToUseSingleCureMagic.size() - 2);
		}

	}

	SkillA PartnerAI::Ressurection() const {
		return this->Character().AbleToUseSingleCureMagic.at(SearchReservedMagic(this->Character().AbleToUseSingleCureMagic, "リザレクション"));
	}

	static inline bool UseCureCommand(const std::vector<SkillA>& SingleCureMagic, const std::vector<SkillA>& RangeCureMagic, const int CureTargetCharacterNum) {
		return (!SingleCureMagic.empty() || !RangeCureMagic.empty()) && CureTargetCharacterNum >= 1;
	}

	static inline bool UseResurrectionCommand(const std::vector<SkillA>& SingleCureMagic, const Status Player, const Status OtherPartner) {
		return std::any_of(SingleCureMagic.begin(), SingleCureMagic.end(), [](const SkillA s) { return s.Name == "リザレクション"; })
			&& (Player.HP.IsMin() || OtherPartner.HP.IsMin());
	}

	// HP残量25％から回復を行う
	SkillA PartnerAI::balance() {
		if (const int CureTargetCharacterNum = this->CheckCureTargetCharacterNum(25);
			UseCureCommand(this->Character().AbleToUseSingleCureMagic, this->Character().AbleToUseRangeCureMagic, CureTargetCharacterNum)
			) return this->CureCommand(CureTargetCharacterNum);
		else if (UseResurrectionCommand(this->Character().AbleToUseSingleCureMagic, this->Characters[this->ID == 0 ? 1 : 0], this->Characters[this->ID == 0 ? 2 : 3 - this->ID]))
			return this->Ressurection();
		else return AttackCommand();
	}

	// 回復しない
	SkillA PartnerAI::attackmain() { return AttackCommand(); }

	// HP残量40％から回復を行う
	SkillA PartnerAI::curemain() {
		if (const int CureTargetCharacterNum = this->CheckCureTargetCharacterNum(40);
			UseCureCommand(this->Character().AbleToUseSingleCureMagic, this->Character().AbleToUseRangeCureMagic, CureTargetCharacterNum)
			) return this->CureCommand(CureTargetCharacterNum);
		else if (UseResurrectionCommand(this->Character().AbleToUseSingleCureMagic, this->Characters[this->ID == 0 ? 1 : 0], this->Characters[this->ID == 0 ? 2 : 3 - this->ID])) return this->Ressurection();
		else return AttackCommand();
	}

	void PartnerAI::ChooseCureTarget() {
		std::sort(this->PlayerSide.begin(), this->PlayerSide.end(), [this](const size_t a, const size_t b) {
			return this->Characters[a].HP.RealParameter.GetRatioArrange() < this->Characters[b].HP.RealParameter.GetRatioArrange();
		});
	}

	size_t PartnerAI::GetTarget() const noexcept {
		return this->PlayerSide.front();
	}
}
