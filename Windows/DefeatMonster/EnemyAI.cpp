﻿#include "CharacterAI.hpp"
#include "Battle.hpp"
#include <numeric>

namespace Offline {
	EnemyAI::EnemyAI(std::array<Status, 4> & Characters)
		: Characters(Characters) {}

	SkillA EnemyAI::AttackCommand() const {
		return BaseBattleCreateImpl::NormalAttack;
	}

	SkillA EnemyAI::MagicAttackCommand() const {
		if (IsFoolCommandSelected(this->Characters[3].Cleverness.Get())) return SelectElementry(this->Characters[3].AbleToUseAttackMagic);
		else {
			return this->Characters[3].AbleToUseAttackMagic.at(GetMaxDamageIndex(this->Characters[this->PlayerSide.front()], this->Characters[3]));
		}
	}

	SkillA EnemyAI::CureCommand() const {
		return SelectElementry(this->Characters[3].AbleToUseSingleCureMagic);
	}

	void EnemyAI::ChooseTarget() {
		std::shuffle(this->PlayerSide.begin(), this->PlayerSide.end(), Random::GetEngine());
	}

	SkillA EnemyAI::command() {
		std::iota(this->PlayerSide.begin(), this->PlayerSide.end(), 0);
		for (auto& i : this->PlayerSide) if (this->Characters[i].Dead()) i = 3;
		this->ChooseTarget();
		static std::uniform_int_distribution<int> rand(0, 2);
		switch (rand(Random::GetEngine())) {
		case 0: return this->AttackCommand();
		case 1: return this->Characters[3].AbleToUseAttackMagic.empty() ? this->command() : this->MagicAttackCommand();
		case 2: return this->Characters[3].AbleToUseSingleCureMagic.empty() ? this->command() : this->CureCommand();
		default: return this->AttackCommand();
		}
	}

	size_t EnemyAI::GetTarget() const {
		for (const auto& i : this->PlayerSide) if (i < 3) return i;
		throw std::runtime_error("Player side characters are dead, but game is not finish.");
	}

}
