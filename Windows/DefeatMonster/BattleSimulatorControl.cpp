﻿#ifdef _DEBUG
#include "Program.hpp"
#include "Color.hpp"
#include "BattleSimulator.hpp"
#include <numeric>

BattleSimulator::BattleSimulator(std::array<Status, 4>& Characters, KgDxGraphic::NoExcept::Picture& BackgroundGraph, KgDxGraphic::ThrowException::String& StringHandle, DllFunctionCallManager& DllFuncManager, int& MasterVolume)
	: BaseBattleCreateImpl(BackgroundGraph, "", StringHandle, DllFuncManager, MasterVolume),
	Characters(Characters), GaugeModeParameterGraphField(this->NameWidthWithGaugeMode + 202 + 2, Program::StringSize * 3 + 2, Color::Gray, Color::White),
	NumModeParameterGraphField(this->NameWidthWithGaugeMode * 2 + this->NameWidthWithGaugeMode + 2 + this->StringHandle.get().GetPrintStringWidth("ああ"), Program::StringSize * 4 + 2, Color::Gray, Color::White), Enemy(Characters) {
	std::iota(this->TurnControlArray.begin(), this->TurnControlArray.end(), 0);
}

void BattleSimulator::CreateTurnControlArray() {
	const int DifferenceBetweenMaxSpeedAndMinSpeed =
		std::max({ this->Characters[0].Speed.Get(), this->Characters[1].Speed.Get(), this->Characters[2].Speed.Get(), this->Characters[3].Speed.Get() })
		- std::min({ this->Characters[0].Speed.Get(), this->Characters[1].Speed.Get(), this->Characters[2].Speed.Get(), this->Characters[3].Speed.Get() });
	std::array<int, 4> SpeedArr = {
		this->Characters[0].Speed.GetParameterToCreateAttackTurn(Random::GetEngine(), DifferenceBetweenMaxSpeedAndMinSpeed, -DifferenceBetweenMaxSpeedAndMinSpeed),
		this->Characters[1].Speed.GetParameterToCreateAttackTurn(Random::GetEngine(), DifferenceBetweenMaxSpeedAndMinSpeed, -DifferenceBetweenMaxSpeedAndMinSpeed),
		this->Characters[2].Speed.GetParameterToCreateAttackTurn(Random::GetEngine(), DifferenceBetweenMaxSpeedAndMinSpeed, -DifferenceBetweenMaxSpeedAndMinSpeed),
		this->Characters[3].Speed.GetParameterToCreateAttackTurn(Random::GetEngine(), DifferenceBetweenMaxSpeedAndMinSpeed, -DifferenceBetweenMaxSpeedAndMinSpeed)
	};
	std::sort(this->TurnControlArray.begin(), this->TurnControlArray.end(), [this, SpeedArr](const int a, const int b) {
		return SpeedArr[a] > SpeedArr[b];
	});
}

void BattleSimulator::ApplyDamageToGraph(AbleToChangeMaxCursor& c) const {
	if (c.SpeedApplyDamageInfo()) {
		for (size_t i = 0; i < 4; i++) {
			this->Characters[i].HP.SpeedApply();
			this->Characters[i].MP.SpeedApply();
		}
	}
	else this->ApplyDamageToGraph();
}

void BattleSimulator::ApplyDamageToGraph() const {
	for (size_t i = 0; i < 4; i++) {
		this->Characters[i].HP.SpeedApply();
		this->Characters[i].MP.SpeedApply();
	}
}

bool BattleSimulator::CommonLoopCondition(const bool ApplyDamageInfo) const {
	Program::Fps.Wait();
	const bool Flag = Program::LoopCondition();
	if (ApplyDamageInfo) this->ApplyDamageToGraph();
	Program::Fps.Update();
	return Flag;
}

bool BattleSimulator::Win() const { return BaseBattleCreateImpl::Win(this->Characters[3]); }

bool BattleSimulator::Lose() const { return BaseBattleCreateImpl::Lose({ this->Characters[0], this->Characters[1], this->Characters[2] }); }

size_t BattleSimulator::GetEngagePartnerID() const noexcept { return this->Characters[1].IsEngageCharacter ? 1 : (this->Characters[2].IsEngageCharacter ? 2 : 0); }

size_t BattleSimulator::GetAliveFrontCharacter() const noexcept {
	if (!this->Characters[0].Dead()) return 0;
	else if (!this->Characters[1].Dead()) return 1;
	else return 2;
}

void BattleSimulator::ReloadMemberInfo(const std::initializer_list<Status>& PlayerSideCharacterList, const Status& EnemyMonster) {
	BaseBattleCreateImpl::ReloadMemberInfo(PlayerSideCharacterList, EnemyMonster);
	this->GaugeModeParameterGraphField = std::move(MenuWindow(this->NameWidthWithGaugeMode + 202 + 2, Program::StringSize * 3 + 2, Color::Gray, Color::White));
	this->NumModeParameterGraphField = std::move(MenuWindow(this->NameWidthWithGaugeMode * 2 + this->NameWidthWithGaugeMode + 2 + this->StringHandle.get().GetPrintStringWidth("ああ"), Program::StringSize * 4 + 2, Color::Gray, Color::White));
}

void BattleSimulator::ChangeTurnControl(BattleCommonInformation& bcm, std::array<bool, 3> &AllowAttack, const KgDxGraphic::Color& CursorDefaultColor) {
	for (size_t i = 0; i < 3; i++)
		if (AllowAttack[i] && this->Characters[i].Dead()) AllowAttack[i] = false;
	bcm.CursorColor = CursorDefaultColor;
	if (bcm.CurrentControlTurnArrReferencePoint == 3) bcm.Control = 6;
	else {
		bcm.CurrentControlTurnArrReferencePoint = (bcm.CurrentControlTurnArrReferencePoint == 3 ? 0 : bcm.CurrentControlTurnArrReferencePoint + 1);
		if (this->TurnControlArray[bcm.CurrentControlTurnArrReferencePoint] >= 0) bcm.Control = this->TurnControlArray[bcm.CurrentControlTurnArrReferencePoint] + 2;
	}
}

void BattleSimulator::InitControl(std::array<bool, 3> &AllowAttack, bool &AlreadyDoubleAttack) {
	AlreadyDoubleAttack = false;
	static const std::bernoulli_distribution rand(0.1);
	const bool MagicCureFlag = rand(Random::GetEngine());
	for (size_t i = 0; i < 4; i++) {
		if (MagicCureFlag && i != 3) this->Characters[i].CureMP();
		this->Characters[i].Guard = false;
		this->Characters[i].CreateAbleToUseMagicList();
		if (i < 3) AllowAttack[i] = !this->Characters[i].Dead();
	}
	this->CreateTurnControlArray();
}

int BattleSimulator::BattleSection(BattleCommonInformation& bcm, const KgDxGraphic::Color CursorDefaultColor, PartnerCommandManager& Player, PartnerCommandManager& FirstPartner, PartnerCommandManager& SecondPartner, const KgDxGraphic::NoExcept::Picture& EnemyGraphicInfo) {
	std::array<bool, 3> AllowAttack;
	std::bernoulli_distribution DoubleAttack(0.8);
	bool AlreadyDoubleAttack = false;
	auto PartnerCommand = [this, &AllowAttack, &bcm, &EnemyGraphicInfo](const size_t PartnerID, PartnerCommandManager& CmdMgr) {
		if (AllowAttack[PartnerID] && !this->Characters[3].HP.IsMin()) {
			bool IgnorePlayerInstructionFlag = false;
			if (CmdMgr.AIControl) bcm.Commands[PartnerID] = CmdMgr.AICommand();
			else IgnorePlayerInstructionFlag = CmdMgr.IgnorePlayerInstruction(bcm.Commands[PartnerID]);
			this->ExecutePartnerCommand(bcm.Commands[PartnerID], PartnerID, static_cast<size_t>(bcm.CursorPoints[PartnerID]), EnemyGraphicInfo, IgnorePlayerInstructionFlag);
		}
		bcm.Control = 1;
	};
	while (this->CommonLoopCondition()) {
		switch (bcm.Control) {
			case 0: // 初期化
				this->InitControl(AllowAttack, AlreadyDoubleAttack);
				bcm.init(CursorDefaultColor);
				break;
			case 1: // ターンコントロール変更
				this->ChangeTurnControl(bcm, AllowAttack, CursorDefaultColor);
				break;
			case 2: // プレイヤー
				if (AllowAttack[0] && !this->Characters[3].HP.IsMin()) {
					bcm.Commands[0] = Player.AICommand();
					bcm.EngageAttack = true;
					this->ExecutePlayerCommand(bcm.CursorPoints[0], bcm.EngageAttack, bcm.Commands[0], EnemyGraphicInfo);
				}
				bcm.Control = 1;
				break;
			case 3: { // パートナー１
				PartnerCommand(1, FirstPartner);
				bcm.Control = 1;
				break;
			}
			case 4: { // パートナー２
				PartnerCommand(2, SecondPartner);
				break;
			}
			case 5: // 敵
				if (!this->Characters[3].HP.IsMin() && !this->Lose()) this->ExecuteEnemySideCommand(EnemyGraphicInfo);
				if (AlreadyDoubleAttack || !DoubleAttack(Random::GetEngine())) bcm.Control = 1;
				else AlreadyDoubleAttack = true;
				break;
			case 6: // 判定
				if (this->Lose()) return this->Win() ? 1 : 0;
				else if (this->Win()) return 2;
				else bcm.Control = 0;
				break;
			default:
				bcm.Control = 0;
				break;
		}
	}
	throw GameEnd(0);
}
#endif
