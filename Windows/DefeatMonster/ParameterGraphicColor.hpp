﻿#ifndef __PARAMETERGRAPHICCOLOR_HPP__
#define __PARAMETERGRAPHICCOLOR_HPP__
#include "InRangeJudgement.hpp"
#include "Status.hpp"
#include "Color.hpp"

static constexpr int CalcGaugeLength(const int MaxLength, const PossibleChangeStatusArrange<int> Parameter) {
	return static_cast<int>(static_cast<float>(MaxLength) * Parameter.GetRatio()) == MaxLength && !Parameter.IsMax()
		? MaxLength - 1
		: static_cast<int>(static_cast<float>(MaxLength) * Parameter.GetRatio()) == 0 && !Parameter.IsMin()
			? 1
			: static_cast<int>(static_cast<float>(MaxLength) * Parameter.GetRatio());
}

namespace GaugeMode {
	inline KgDxGraphic::Color GetHPGaugeColor(const Status& sta) {
		return sta.HP.GraphParameter.GetRatioArrange() == 100
			? Color::Cyan
			: InRangeIncludedBorders(sta.HP.GraphParameter.GetRatioArrange(), 99, 51)
				? Color::Green
				: InRangeIncludedBorders(sta.HP.GraphParameter.GetRatioArrange(), 50, 26)
					? Color::Yellow
					: InRangeIncludedBorders(sta.HP.GraphParameter.GetRatioArrange(), 25, 10)
						? Color::Orange : Color::Red;
	}

	inline KgDxGraphic::Color GetStatusGraphColor(const Status& sta) {
		return InRangeIncludedBorders(sta.HP.GraphParameter.GetRatioArrange(), 100, 51)
			? Color::Green // 100～51%
			: InRangeIncludedBorders(sta.HP.GraphParameter.GetRatioArrange(), 50, 26)
				? Color::Yellow // 50～26%
				: InRangeIncludedBorders(sta.HP.GraphParameter.GetRatioArrange(), 25, 1)
					? Color::Orange // 25～1%
					: Color::Red; // 0%
	}

	inline KgDxGraphic::Color GetNameGraphColor(const Status& sta) {
		return InRangeIncludedBorders(sta.HP.RealParameter.GetRatioArrange(), 100, 51)
			? Color::Green // 100～51%
			: InRangeIncludedBorders(sta.HP.RealParameter.GetRatioArrange(), 50, 26)
				? Color::Yellow // 50～26%
				: InRangeIncludedBorders(sta.HP.RealParameter.GetRatioArrange(), 25, 1)
					? Color::Orange // 25～1%
					: Color::Red; // 0%
	}
}

namespace NumMode {
	inline KgDxGraphic::Color GetStatusGraphColor(const Status& sta) {
		return InRangeIncludedBorders(sta.HP.GraphParameter.GetRatioArrange(), 100, 41)
			? Color::White // 100～41%
			: InRangeIncludedBorders(sta.HP.GraphParameter.GetRatioArrange(), 40, 26)
				? Color::Yellow // 40～26%
				: InRangeIncludedBorders(sta.HP.GraphParameter.GetRatioArrange(), 25, 1)
					? Color::Orange // 25～1%
					: Color::Red; // 0%
	}

	inline KgDxGraphic::Color GetNameGraphColor(const Status& sta) {
		return InRangeIncludedBorders(sta.HP.RealParameter.GetRatioArrange(), 100, 41)
			? Color::White // 100～41%
			: InRangeIncludedBorders(sta.HP.RealParameter.GetRatioArrange(), 40, 26)
				? Color::Yellow // 40～26%
				: InRangeIncludedBorders(sta.HP.RealParameter.GetRatioArrange(), 25, 1)
					? Color::Orange // 25～1%
					: Color::Red; // 0%
	}
}
#endif
