﻿#ifdef _DEBUG
#include "BattleSimulator.hpp"

void BattleSimulator::ExecutePlayerCommand(const int CursorPoint, bool& EngageAttack, const SkillA& AttackInfo, const KgDxGraphic::NoExcept::Picture& EnemyGraphicInfo) const {
	if (this->Characters[0].Guard) this->GraphMessageScreenExecuteGuardCommand(this->Characters[0].Name, EnemyGraphicInfo);
	else {
		const size_t EngagePartnerID = this->GetEngagePartnerID();
		if (EngageAttack && (EngagePartnerID == 0 || this->Characters[EngagePartnerID].Dead() || this->Characters[EngagePartnerID].MP.RealParameter.IsMin())) 
			EngageAttack = false;
		switch (AttackInfo.SkillType) {
			case 0: // 攻撃
				// エンゲージアタックをする場合、パラメーターを変更する
				if (EngageAttack) {
					AttackInfo.Name == "N/A"
						? this->Characters[0].ApplyEngageToAttack(this->Characters[EngagePartnerID].MP)
						: this->Characters[0].ApplyEngageToMagicAttack(this->Characters[EngagePartnerID].MP);
				}
				this->GraphMessageScreenAfterExecuteAttackCommandToEnemy(0, AttackInfo, EnemyGraphicInfo, EngageAttack);
				break;
			case 1: // 単体回復
				if (EngageAttack) this->Characters[0].ApplyEngageToMagicCure(this->Characters[EngagePartnerID].MP);
				this->GraphMessageScreenAfterExecuteSingleCureCommand(0, AttackInfo, static_cast<size_t>(CursorPoint), EnemyGraphicInfo, EngageAttack);
				break;
			case 2: // 範囲回復
				if (EngageAttack) this->Characters[0].ApplyEngageToMagicCure(this->Characters[EngagePartnerID].MP);
				this->GraphMessageScreenAfterExecuteRangeCureCommand(0, AttackInfo, EnemyGraphicInfo, EngageAttack);
				break;
			default:
				throw KgWinException("SkillType is wrong.");
		}
	}
	this->Characters[0].CleanEngage();
}

void BattleSimulator::ExecutePartnerCommand(const SkillA& AttackInfo, const size_t AttackerID, const size_t LastCursorPoint, const KgDxGraphic::NoExcept::Picture& EnemyGraphicInfo, const bool IgnorePlayerInstructionFlag) const {
	if (this->Characters[AttackerID].Guard) this->GraphMessageScreenExecuteGuardCommand(this->Characters[AttackerID].Name, EnemyGraphicInfo);
	else {
		switch (AttackInfo.SkillType) {
			case 0: // 攻撃
				this->GraphMessageScreenAfterExecuteAttackCommandToEnemy(AttackerID, AttackInfo, EnemyGraphicInfo);
				break;
			case 1: // 単体回復
				this->GraphMessageScreenAfterExecuteSingleCureCommand(AttackerID, AttackInfo, LastCursorPoint, EnemyGraphicInfo, false, IgnorePlayerInstructionFlag);
				break;
			case 2: // 範囲回復
				this->GraphMessageScreenAfterExecuteRangeCureCommand(AttackerID, AttackInfo, EnemyGraphicInfo, false, IgnorePlayerInstructionFlag);
				break;
			default:
				throw KgWinException("SkillType is wrong.");
		}
	}
}

void BattleSimulator::ExecuteEnemySideCommand(const KgDxGraphic::NoExcept::Picture& EnemyGraphicInfo) {
	const SkillA AttackInfo = this->Enemy.command();
	if (this->Characters[3].Guard) this->GraphMessageScreenExecuteGuardCommand(this->Characters[3].Name, EnemyGraphicInfo);
	else {
		switch (AttackInfo.SkillType) {
			case 0: // 攻撃
				AttackInfo.Range
					? this->GraphMessageScreenAfterExecuteRangeAttackCommandFromEnemy(this->GetAliveFrontCharacter(), AttackInfo, EnemyGraphicInfo)
					: this->GraphMessageScreenAfterExecuteAttackCommandFromEnemy(this->Enemy.GetTarget(), AttackInfo, EnemyGraphicInfo);
				break;
			case 1: // 単体回復
				this->GraphMessageScreenAfterExecuteSingleCureCommand(3, AttackInfo, 3, EnemyGraphicInfo);
				break;
			default:
				throw KgWinException("SkillType is wrong.");
		}
	}
}
#endif
