#ifndef __AISKILL_H__
#define __AISKILL_H__
typedef struct {
	const char* SkillTag;
	int UseMP;
	int BasePower;
	unsigned int ElementID;
	bool Range;
	bool RessurectionEffect;
	int SkillType;
} AISkill;

typedef struct {
	AISkill* SkillList;
	unsigned int SkillNum;
} AISkillList;
#endif
