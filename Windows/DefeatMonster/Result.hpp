﻿#ifndef __RESULT_HPP__
#define __RESULT_HPP__
#include "ref.hpp"
#include "DllFunctionCallManager.hpp"
#include "LanguageRuntimeFunctionType.hpp"
#include "Status.hpp"
#include "KgDxGraphic.hpp"
#include <functional>

namespace Result {
	template<size_t arrsize>
	class BattleResultImpl {
	protected:
		std::reference_wrapper<DllFunctionCallManager> DllFuncMgr;
		standard::ArrayRef<Status, arrsize> Characters;
		standard::ref<KgDxGraphic::ThrowException::String> StringHandle;
		using InternalBaseClassType = BattleResultImpl<arrsize>;
	public:
		BattleResultImpl(std::array<Status, arrsize>& Characters, KgDxGraphic::ThrowException::String& StringHandle, DllFunctionCallManager& Dll)
			: Characters(Characters), StringHandle(StringHandle), DllFuncMgr(Dll) {}
	};
	namespace Offline {
		class NormalBattle : public BattleResultImpl<4> {
		public:
			NormalBattle(std::array<Status, 4>& Characters, KgDxGraphic::ThrowException::String& StringHandle, DllFunctionCallManager& Dll)
				: InternalBaseClassType(Characters, StringHandle, Dll) {}
			void ShowResult(const int ResultID) const;
		};

		class ContinuousDefeat : public BattleResultImpl<4> {
			ContinuousDefeat(std::array<Status, 4>& Characters, KgDxGraphic::ThrowException::String& StringHandle, DllFunctionCallManager& Dll)
				: InternalBaseClassType(Characters, StringHandle, Dll) {}
			void ShowResult(const int DefeatCount) const;
		};

		class TimeAttack : public BattleResultImpl<4> {
			TimeAttack(std::array<Status, 4>& Characters, KgDxGraphic::ThrowException::String& StringHandle, DllFunctionCallManager& Dll)
				: InternalBaseClassType(Characters, StringHandle, Dll) {}
			void ShowResult(const time_t ClearTime) const;
		};
	}

	namespace Online {
		class BattleTwoPlayer : public BattleResultImpl<5> {

		};

		class BattleBetweenPlayer : public BattleResultImpl<2> {

		};
	}
}
#endif
