﻿#include "Color.hpp"

KgDxGraphic::Color Color::Gray = KgDxGraphic::Color("#C0C0C0");
KgDxGraphic::Color Color::White = KgDxGraphic::Color("#ffffff");
KgDxGraphic::Color Color::Green = KgDxGraphic::Color("#00ff00");
KgDxGraphic::Color Color::Red = KgDxGraphic::Color("#ff0000");
KgDxGraphic::Color Color::Blue = KgDxGraphic::Color("#0000ff");
KgDxGraphic::Color Color::Orange = KgDxGraphic::Color("#ffa500");
KgDxGraphic::Color Color::Yellow = KgDxGraphic::Color("#ffff00");
KgDxGraphic::Color Color::Black = KgDxGraphic::Color("#000000");
KgDxGraphic::Color Color::Cyan = KgDxGraphic::Color("#00ffff");
KgDxGraphic::Color Color::Pink = KgDxGraphic::Color("#ffc0cb");
