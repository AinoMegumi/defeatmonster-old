﻿#include "HandleManager.hpp"
#include "Program.hpp"
#include "Status.hpp"
#include "EnvironmentVariable.hpp"
#include "PropertiesRead.hpp"
#include "XmlRead.hpp"
#include "Random.hpp"
#include "GameEnd.hpp"
#include <Windows.h>
#include <AclAPI.h>
#include <direct.h>

static inline void CreateProcessAndWait(const std::string& ExeFilePath, char CommandLine[1024]) {
	STARTUPINFOA        si{ 0 };
	PROCESS_INFORMATION pi{ 0 };
	ClassMember::HandleManager ProcessHandle(pi.hProcess);
	ClassMember::HandleManager ThreadHandle(pi.hThread);
	if (0 == CreateProcessA(ExeFilePath.c_str(), CommandLine, NULL, NULL, FALSE, 0, NULL, Program::GetApplicationCurrentDirectory().c_str(), &si, &pi))
		throw KgWinException();
	DWORD dwExitCode = STILL_ACTIVE;
	if (pi.hProcess) {
		while (dwExitCode == STILL_ACTIVE && Program::LoopCondition()) {
			if (WAIT_FAILED == WaitForSingleObject(pi.hProcess, 1000)) throw KgWinException();
			if (FALSE == GetExitCodeProcess(pi.hProcess, &dwExitCode)) throw KgWinException();
		}
		if (dwExitCode != 0) throw std::runtime_error("Error in setting menu");
	}
	else throw std::runtime_error("Failed to show setup window.");
}

void CreateAllCharacterStatus(const int PartnerNum) {
	if (!Program::LoopCondition()) throw GameEnd(1);
	_mkdir(Program::ChangeFullPath(".\\Temp").c_str());
	char CommandLine[1024];
#ifdef TPRELEASE
	sprintf_s(CommandLine, " /pn %d /ph \"%s\"", PartnerNum, Program::ChangeFullPath(".\\Temp").c_str());
#else
	sprintf_s(CommandLine, " /pn %d /ph \"%s\"", PartnerNum, EnvironmentVariableA("DMTEMP").Get().c_str());
#endif
	CreateProcessAndWait(Program::ChangeFullPath(".\\Language\\" + Program::Language + "\\dmsetup.exe"), CommandLine);
	// CreateChildProcess(Program::ChangeFullPath(".\\Language\\" + Program::Language + "\\dmsetup.exe"), PartnerNum);
}

void SetStrategy(const std::string FirstPartnerName, const std::string SecondPartnerName) {
	if (!Program::LoopCondition()) throw GameEnd(1);
	char CommandLine[1024];
	sprintf_s(CommandLine, " %s %s", FirstPartnerName.c_str(), SecondPartnerName.c_str());
	CreateProcessAndWait(Program::ChangeFullPath(".\\Language\\" + Program::Language + "\\dmstrtgy.exe"), CommandLine);
}

inline auto GetCharacterInfo(const std::vector<StatusRawData> Arr, const std::string Tag) {
	for (auto& i : Arr) if (i.Tag == Tag) return i;
	throw std::runtime_error(Tag + " : no character have such identification tag");
}

static inline int GetSexID(const std::string SexString) {
	static const std::string SetupScreenSexString[4] = {
		"男性", "女性", "男の娘", "漢女"
	};
	for (int i = 0; i < 4; i++) if (SexString.find(SetupScreenSexString[i]) != std::string::npos) return i;
	return 0;
}

Status GetPlayerStatus() {
#ifdef TPRELEASE
	const PropertiesRead Prop(Program::ChangeFullPath(".\\Temp\\dmsetup.properties"));
#else
	const PropertiesRead Prop(EnvironmentVariableA("DMTEMP").Get() + "\\dmsetup.properties");
#endif
	const auto Player = ReadPlayerData();
	StatusRawData r = GetCharacterInfo(Player, Prop.GetData("player"));
	r.Name = Prop.GetData("playername");
	r.SexID = GetSexID("playersex");
	Status s = std::move(Status::CreatePlayerStatus(r));
	return s;
}

Status GetPartnerStatus(const std::string proptag, const std::vector<StatusRawData> Partner) {
#ifdef TPRELEASE
	const PropertiesRead Prop(Program::ChangeFullPath(".\\Temp\\dmsetup.properties"));
#else
	const PropertiesRead Prop(EnvironmentVariableA("DMTEMP").Get() + "\\dmsetup.properties");
#endif
	const std::string partnertag = Prop.GetData(proptag);
	Status s = std::move(Status::CreatePartnerStatus(GetCharacterInfo(Partner, partnertag)));
	s.IsEngageCharacter = (partnertag == Prop.GetData("engage"));
	if (proptag == "spartner") DeleteFileA(Program::ChangeFullPath(".\\Temp\\dmsetup.properties").c_str());
	return s;
}

Status ChooseEnemy(const size_t MaxLevel, const std::string& SavedGamesFolder, const bool ReadUserCharacter) {
	const auto Enemy = ReadEnemyData(SavedGamesFolder, ReadUserCharacter);
	Status s = Status::CreateEnemyStatus(SelectElementry(Enemy));
	if (MaxLevel > 10) {
		s.AddSpecialAttackMagic([]() { // メテオは敵限定攻撃魔法であるためここで追加
			SkillA s{};
			s.Name = "メテオ";
			s.Tag = "ammeteo";
			s.UseMP = 75;
			s.BasePower = 250;
			s.Range = true;
			s.SkillElement = ElementInfo::Fire;
			s.SkillType = 0;
			s.EffectGraphPath = Program::ChangeFullPath(".\\Graphic\\Effect\\meteo.png");
			s.EffectSoundPath = "N/A";
			s.AddBlendLevel = -1;
			return s;
		}());
	}
	return s;
}

void LoadStrategy(int &FirstPartner, int &SecondPartner) {
	{
#ifdef TPRELEASE
		const PropertiesRead Prop(Program::ChangeFullPath(".\\Temp\\strategy.properties"));
#else
		const PropertiesRead Prop(EnvironmentVariableA("DMTEMP").Get() + "\\strategy.properties");
#endif
		FirstPartner = std::stoi(Prop.GetData("fpartner"));
		SecondPartner = std::stoi(Prop.GetData("spartner"));
	}
	DeleteFileA(Program::ChangeFullPath(".\\strategy.properties").c_str());
}
