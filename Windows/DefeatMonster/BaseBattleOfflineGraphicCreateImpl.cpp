﻿#include "Battle.hpp"

namespace Offline {
	void BaseBattle::GraphGaugeGraphField(const int X, const int Y) const {
		this->GaugeModeParameterGraphField.Print(X, Y);
	}

	void BaseBattle::GraphNumGraphField(const int X, const int Y) const {
		this->NumModeParameterGraphField.Print(X, Y);
	}

	void BaseBattle::GraphEnemyCharacterParameter() const {
		Program::GameMasterMode && GraphNumMode
			? BaseBattleCreateImpl::GraphEnemyCharacterParameterWithNumMode(WindowWidth - 220, 10, this->Characters[3])
			: BaseBattleCreateImpl::GraphEnemyCharacterParameterWithGaugeMode(470, 10, this->Characters[3]);
	}

	void BaseBattle::GraphParameterWithGaugeMode(const int X, const int Y) const {
		this->GraphGaugeGraphField(X, Y);
		CommonGraphicForBattle::GraphPlayerSideCharacterParameterWithGaugeMode(X + 1, Y + 1, this->Characters[0]);
		CommonGraphicForBattle::GraphPlayerSideCharacterParameterWithGaugeMode(X + 1, Y + 1 + Program::StringSize, this->Characters[1]);
		CommonGraphicForBattle::GraphPlayerSideCharacterParameterWithGaugeMode(X + 1, Y + 1 + Program::StringSize * 2, this->Characters[2]);
	}

	void BaseBattle::GraphParameterWithNumMode(const int X, const int Y) const {
		this->GraphNumGraphField(X, Y);
		CommonGraphicForBattle::GraphPlayerSideCharacterParameterWithNumMode(X + 1, Y + 1, this->Characters[0]);
		CommonGraphicForBattle::GraphPlayerSideCharacterParameterWithNumMode(X + 2 + this->NameWidthWithNumMode, Y + 1, this->Characters[1]);
		CommonGraphicForBattle::GraphPlayerSideCharacterParameterWithNumMode(X + 3 + this->NameWidthWithNumMode * 2, Y + 1, this->Characters[2]);
	}

	void BaseBattle::CommonGraphic() const {
		KgDxGraphic::NoExcept::ChangeCleannessLevel(100);
		this->GraphBackground();
		this->GraphEnemy();
		KgDxGraphic::NoExcept::ChangeCleannessLevel(70);
		this->GraphPlayerSideCharacterParameter();
		this->GraphEnemyCharacterParameter();
	}

	
}
