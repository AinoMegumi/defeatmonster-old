#ifndef __ISPLAYERJUDGEMENT_HPP__
#define __ISPLAYERJUDGEMENT_HPP__
#include <string>

inline bool IsPlayer(const std::string& Tag) {
	return Tag == "balance" || Tag == "warrior" || Tag == "wizard" || Tag == "crerick";
}
#endif
