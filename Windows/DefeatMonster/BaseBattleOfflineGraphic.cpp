﻿#include "Battle.hpp"
constexpr int MinusNumFromWidth = WindowWidth % 3;
constexpr int BaseWidth = (WindowWidth - MinusNumFromWidth) / 3;
constexpr size_t ReDrawCount = 5;

namespace Offline {
	static inline std::vector<KgDxGraphic::NoExcept::Picture> LoadEffectGraphic(const std::string& EffectGraphPath) {
		return KgDxGraphic::NoExcept::Picture::DivLoad(EffectGraphPath, [EffectGraphPath]() {
			KgDxGraphic::DivLoadSettings div{};
			KgDxGraphic::NoExcept::Picture p(EffectGraphPath);
			div.YNum = 1;
			div.AllNum = div.XNum = p.Width / p.Height;
			div.XSize = p.Width / div.XNum;
			div.YSize = p.Height;
			return div;
		}());
	}

	void BaseBattle::GraphAttackEffect(const int X1, const int Y1, const int X2, const int Y2, const std::vector<KgDxGraphic::NoExcept::Picture>& EffectGraphic, const int AddBlend, const std::string& AttackerName,
		const std::string& AttackName, const std::string& EffectSoundPath, const bool EngageAttack, const bool IgnorePlayerInstruction) const {
		KgDxSound::NoExcept::Sound EffectSound = std::move((EffectSoundPath.empty() || EffectSoundPath == "N/A")
			? KgDxSound::NoExcept::Sound() : KgDxSound::NoExcept::Sound(EffectSoundPath));
		if (!(EffectSoundPath.empty() || EffectSoundPath == "N/A")) {
			EffectSound.Load();
			EffectSound.ChangeVolume(this->MasterVolume);
			EffectSound.Play(KgDxSound::PlayType::Back);
		}
		KgDxGraphic::NoExcept::ChangeCleannessLevel(100);
		for (size_t i = 0; i < EffectGraphic.size() * ReDrawCount && this->CommonLoopCondition(); i++) {
			KgDxGraphic::NoExcept::Clear();
			this->GraphBackground();
			this->GraphEnemy();
			if (AddBlend != -1) {
				KgDxGraphic::NoExcept::ChangeAddBlendLevel(AddBlend);
				EffectGraphic.at(i / ReDrawCount).Print(X1, Y1, X2, Y2, true);
				KgDxGraphic::NoExcept::ChangeAddBlendLevel(0);
			}
			else EffectGraphic.at(i / ReDrawCount).Print(X1, Y1, X2, Y2, true);
			KgDxGraphic::NoExcept::ChangeCleannessLevel(70);
			this->GraphPlayerSideCharacterParameter();
			this->GraphEnemyCharacterParameter();
			this->MessageWindow.Print(MESSAGEWINDOWGRAPHPOINT);
			KgDxGraphic::NoExcept::ChangeCleannessLevel(100);
			this->GraphAttackMessage(AttackerName, AttackName, EngageAttack, IgnorePlayerInstruction);
			KgDxGraphic::NoExcept::Flip();
		}
	}

	void BaseBattle::GraphAttackEffectOnEnemy(const std::string& AttackerName, const SkillA& AttackInfo, const bool EngageAttack, const bool IgnorePlayerInstruction) const {
		if (AttackInfo.EffectGraphPath == "N/A") return;
		const auto Effect = LoadEffectGraphic(AttackInfo.EffectGraphPath);
		const int WidthHeightControlNum = std::min(this->EnemyGraphic.Width, this->EnemyGraphic.Height);
		return this->GraphAttackEffect(
			(WindowWidth - WidthHeightControlNum) / 2, (WindowHeight - WidthHeightControlNum) / 2,
			(WindowWidth + WidthHeightControlNum) / 2, (WindowHeight + WidthHeightControlNum) / 2,
			Effect, AttackInfo.AddBlendLevel, AttackerName, AttackInfo.Name, AttackInfo.EffectSoundPath, EngageAttack, IgnorePlayerInstruction);
	}

	void BaseBattle::GraphAttackEffectOnPlayerSide(const std::string& AttackerName, const SkillA& AttackInfo, const bool EngageAttack, const bool IgnorePlayerInstruction) const {
		// プレイヤーサイドへの範囲攻撃
		if (AttackInfo.EffectGraphPath == "N/A") return;
		const auto Effect = std::move(LoadEffectGraphic(AttackInfo.EffectGraphPath));
		return this->GraphAttackEffect((WindowWidth - BaseWidth) / 2, WindowHeight - BaseWidth, (WindowWidth + BaseWidth) / 2, WindowHeight, Effect, AttackInfo.AddBlendLevel, AttackerName, AttackInfo.Name, AttackInfo.EffectSoundPath, EngageAttack, IgnorePlayerInstruction);
	}

	void BaseBattle::GraphAttackEffectOnPlayerSideAll(const std::string& AttackerName, const SkillA& AttackInfo, const bool EngageAttack, const bool IgnorePlayerInstruction) const {
		if (AttackInfo.EffectGraphPath == "N/A") return;
		KgDxSound::NoExcept::Sound EffectSound = std::move((AttackInfo.EffectSoundPath.empty() || AttackInfo.EffectSoundPath == "N/A")
			? KgDxSound::NoExcept::Sound() : KgDxSound::NoExcept::Sound(AttackInfo.EffectSoundPath));
		const auto Effect = std::move(LoadEffectGraphic(AttackInfo.EffectGraphPath));
		if (!(AttackInfo.EffectSoundPath.empty() || AttackInfo.EffectSoundPath == "N/A")) {
			EffectSound.Load();
			EffectSound.ChangeVolume(this->MasterVolume);
			EffectSound.Play(KgDxSound::PlayType::Back);
		}
		KgDxGraphic::NoExcept::ChangeCleannessLevel(100);
		for (size_t i = 0; i < Effect.size() * ReDrawCount && this->CommonLoopCondition(); i++) {
			KgDxGraphic::NoExcept::Clear();
			this->GraphBackground();
			this->GraphEnemy();
			if (AttackInfo.AddBlendLevel != -1) {
				KgDxGraphic::NoExcept::ChangeAddBlendLevel(AttackInfo.AddBlendLevel);
				Effect.at(i / ReDrawCount).Print(MinusNumFromWidth, WindowHeight - BaseWidth, MinusNumFromWidth + BaseWidth, WindowHeight, true);
				Effect.at(i / ReDrawCount).Print(MinusNumFromWidth + BaseWidth, WindowHeight - BaseWidth, MinusNumFromWidth + BaseWidth * 2, WindowHeight, true);
				Effect.at(i / ReDrawCount).Print(MinusNumFromWidth + BaseWidth * 2, WindowHeight - BaseWidth, MinusNumFromWidth + BaseWidth * 3, WindowHeight, true);
				KgDxGraphic::NoExcept::ChangeAddBlendLevel(0);
			}
			else {
				Effect.at(i / ReDrawCount).Print(MinusNumFromWidth, WindowHeight - BaseWidth, MinusNumFromWidth + BaseWidth, WindowHeight, true);
				Effect.at(i / ReDrawCount).Print(MinusNumFromWidth + BaseWidth, WindowHeight - BaseWidth, MinusNumFromWidth + BaseWidth * 2, WindowHeight, true);
				Effect.at(i / ReDrawCount).Print(MinusNumFromWidth + BaseWidth * 2, WindowHeight - BaseWidth, MinusNumFromWidth + BaseWidth * 3, WindowHeight, true);
			}
			KgDxGraphic::NoExcept::ChangeCleannessLevel(70);
			this->GraphPlayerSideCharacterParameter();
			this->GraphEnemyCharacterParameter();
			this->MessageWindow.Print(MESSAGEWINDOWGRAPHPOINT);
			KgDxGraphic::NoExcept::ChangeCleannessLevel(100);
			this->GraphAttackMessage(AttackerName, AttackInfo.Name, EngageAttack, IgnorePlayerInstruction);
			KgDxGraphic::NoExcept::Flip();
		}
	}

	void BaseBattle::GraphAttackEffectOnPlayerSide(const std::string& AttackerName, const size_t Target, const SkillA& AttackInfo, const bool EngageAttack, const bool IgnorePlayerInstruction) const {
		if (AttackInfo.EffectGraphPath == "N/A") return;
		const auto Effect = std::move(LoadEffectGraphic(AttackInfo.EffectGraphPath));
		return this->GraphAttackEffect(
			MinusNumFromWidth + BaseWidth * static_cast<int>(Target), WindowHeight - BaseWidth, MinusNumFromWidth + BaseWidth * (static_cast<int>(Target) + 1), WindowHeight,
			Effect, AttackInfo.AddBlendLevel, AttackerName, AttackInfo.Name, AttackInfo.EffectSoundPath, EngageAttack, IgnorePlayerInstruction);
	}
	void BaseBattle::IntroductEnemyMonster(AbleToChangeMaxCursor& c) const {
		c.Reset();
		for (; !c.Enter() && this->CommonLoopCondition(c, false); c.Update()) {
			KgDxGraphic::NoExcept::Clear();
			KgDxGraphic::NoExcept::ChangeCleannessLevel(100);
			this->GraphBackground();
			this->GraphEnemy();
			KgDxGraphic::NoExcept::ChangeCleannessLevel(70);
			this->MessageWindow.Print(MESSAGEWINDOWGRAPHPOINT);
			this->msg.IntroductEnemy(MESSAGEWINDOWSTRINGGRAPHPOINT(1), this->Characters[3].Name);
			KgDxGraphic::NoExcept::Flip();
		}
	}

	void BaseBattle::CommonGraphicDuringExecutingCommand(const std::string& AttackerName, const std::string& SkillName, const bool EngageAttack, const bool IgnorePlayerInstructionFlag) const {
		this->CommonGraphic();
		this->MessageWindow.Print(MESSAGEWINDOWGRAPHPOINT);
		KgDxGraphic::NoExcept::ChangeCleannessLevel(100);
		this->GraphAttackMessage(AttackerName, SkillName, EngageAttack, IgnorePlayerInstructionFlag);
	}

	void BaseBattle::GraphPlayerSideCharacterParameter() const {
		GraphNumMode
			? this->GraphParameterWithNumMode(10, 10)
			: this->GraphParameterWithGaugeMode(10, 10);
	}

	void BaseBattle::GraphMessageScreenBeforeExecuteAttackOrCureCommand(const std::string& Attacker, const std::string& AttackName, const KgDxSound::NoExcept::Sound& PlaySoundData, const bool EngageAttack, const bool IgnorePlayerInstructionFlag) const {
		for (PlaySoundData.Play(KgDxSound::PlayType::Back); this->CommonLoopCondition() && PlaySoundData.GetSoundState() == KgDxSound::SoundState::Playing;) {
			KgDxGraphic::NoExcept::Clear();
			this->CommonGraphicDuringExecutingCommand(Attacker, AttackName, EngageAttack, IgnorePlayerInstructionFlag);
			KgDxGraphic::NoExcept::Flip();
		}
	}

	void BaseBattle::GraphMessageScreenAfterExecuteAttackCommandToEnemy(const size_t Attacker, const SkillA& AttackInfo, const bool EngageAttack) const {
		bool CriticalFlag = false;
		const int DamagePoint = (AttackInfo.Name == "N/A" || AttackInfo.Name.empty()) ? this->ExecuteAttackCommandToEnemy(Attacker, CriticalFlag) : this->ExecuteAttackCommandToEnemy(Attacker, AttackInfo);
		const __int64 start = GetCurrentClockOnMilliSeconds();
		COMMONWAIT(start, this->CommonLoopCondition()) {
			KgDxGraphic::NoExcept::Clear();
			this->CommonGraphicDuringExecutingCommand(this->Characters[Attacker].Name, AttackInfo.Name, EngageAttack, false);
			this->msg.DamageToEnemy(MESSAGEWINDOWSTRINGGRAPHPOINT(2), this->Characters[3].Name, DamagePoint, CriticalFlag);
			if (this->Characters[3].HP.IsMin()) this->msg.Defeat(MESSAGEWINDOWSTRINGGRAPHPOINT(3), this->Characters[3].Name);
			KgDxGraphic::NoExcept::Flip();
		}
	}

	void BaseBattle::GraphMessageScreenAfterExecuteSingleCureCommand(const size_t Healer, const SkillA& CureInfo, const size_t Target, const bool EngageAttack, const bool IgnorePlayerInstructionFlag) const {
		// リザレクションの実行は生きているかによって変わる
		if (CureInfo.ResurrectionEffect && this->Characters[Target].Dead()) return this->GraphMessageScreenAfterExecuteResurrection(Healer, Target, CureInfo);
		// リザレクションを実行したが生きている場合は0として処理
		const int CurePoint = CureInfo.ResurrectionEffect ? 0 : this->ExecuteCureCommand(Healer, Target, CureInfo);
		const __int64 start = GetCurrentClockOnMilliSeconds();
		COMMONWAIT(start, this->CommonLoopCondition()) {
			KgDxGraphic::NoExcept::Clear();
			this->CommonGraphicDuringExecutingCommand(this->Characters[Healer].Name, CureInfo.Name, EngageAttack, IgnorePlayerInstructionFlag);
			this->msg.Cure(MESSAGEWINDOWSTRINGGRAPHPOINT(2), this->Characters[Target].Name, CurePoint);
			KgDxGraphic::NoExcept::Flip();
		}
	}

	void BaseBattle::GraphMessageScreenAfterExecuteResurrection(const size_t Healer, const size_t Target, const SkillA& CureInfo) const {
		const bool Result = this->ExecuteResurrection(Healer, Target, CureInfo);
		const __int64 start = GetCurrentClockOnMilliSeconds();
		COMMONWAIT(start, this->CommonLoopCondition()) {
			KgDxGraphic::NoExcept::Clear();
			this->CommonGraphicDuringExecutingCommand(this->Characters[Healer].Name, CureInfo.Name, false, false);
			this->msg.Ressurection(MESSAGEWINDOWSTRINGGRAPHPOINT(2), this->Characters[Target].Name, Result);
			KgDxGraphic::NoExcept::Flip();
		}
	}

	static inline int GetDamageGraphLine(const std::array<int, 3>& DamageList, const size_t CharacterID) {
		switch (CharacterID) {
		case 0:
			return 2;
		case 1:
			return DamageList.at(0) == -1 ? 2 : 3;
		case 2:
			if (DamageList.at(0) == -1) return DamageList.at(1) == -1 ? 2 : 3;
			else return DamageList[1] == -1 ? 3 : 4;
		default:
			throw std::runtime_error("CharacterID is wrong.");
		}
	}

	void BaseBattle::GraphMessageScreenAfterExecuteRangeCureCommand(const size_t Healer, const SkillA& CureInfo, const bool EngageAttack, const bool IgnorePlayerInstructionFlag) const {
		if (this->GraphAllDamageWhenRangeAttack) {
			const std::array<int, 3> CurePointList = ExecuteCureCommand(Healer, CureInfo, -1);
			const __int64 start = GetCurrentClockOnMilliSeconds();
			auto DamageGraphLine = [CurePointList](const size_t CharacterID) { return GetDamageGraphLine(CurePointList, CharacterID); };
			COMMONWAIT(start, this->CommonLoopCondition()) {
				KgDxGraphic::NoExcept::Clear();
				this->CommonGraphicDuringExecutingCommand(this->Characters[Healer].Name, CureInfo.Name, EngageAttack, IgnorePlayerInstructionFlag);
				if (CurePointList[0] != -1) this->msg.Cure(MESSAGEWINDOWSTRINGGRAPHPOINT(DamageGraphLine(0)), this->Characters[0].Name, CurePointList.at(0));
				if (CurePointList[1] != -1) this->msg.Cure(MESSAGEWINDOWSTRINGGRAPHPOINT(DamageGraphLine(1)), this->Characters[1].Name, CurePointList.at(1));
				if (CurePointList[2] != -1) this->msg.Cure(MESSAGEWINDOWSTRINGGRAPHPOINT(DamageGraphLine(2)), this->Characters[2].Name, CurePointList.at(2));
				KgDxGraphic::NoExcept::Flip();
			}
		}
		else {
			const __int64 start = GetCurrentClockOnMilliSeconds();
			const int CureAverage = this->ExecuteCureCommand(Healer, CureInfo);
			COMMONWAIT(start, this->CommonLoopCondition()) {
				KgDxGraphic::NoExcept::Clear();
				this->CommonGraphicDuringExecutingCommand(this->Characters[Healer].Name, CureInfo.Name, EngageAttack, IgnorePlayerInstructionFlag);
				this->msg.RangeCure(MESSAGEWINDOWSTRINGGRAPHPOINT(2), this->Characters[Healer].Name, CureAverage);
				KgDxGraphic::NoExcept::Flip();
			}
		}

	}

	void BaseBattle::GraphMessageScreenExecuteGuardCommand(const std::string& ExecuteCharacterName) const {
		const __int64 start = GetCurrentClockOnMilliSeconds();
		COMMONWAIT(start, this->CommonLoopCondition()) {
			KgDxGraphic::NoExcept::Clear();
			this->CommonGraphic();
			this->MessageWindow.Print(MESSAGEWINDOWGRAPHPOINT);
			KgDxGraphic::NoExcept::ChangeCleannessLevel(100);
			this->msg.Defence(MESSAGEWINDOWSTRINGGRAPHPOINT(1), ExecuteCharacterName);
			KgDxGraphic::NoExcept::Flip();
		}
	}

	void BaseBattle::GraphAttackMessage(const std::string& AttackerName, const std::string& SkillName, const bool EngageAttack, const bool IgnorePlayerInstructionFlag) const {
		if (SkillName == "N/A" || SkillName.empty()) EngageAttack ? this->msg.EngageAttack(MESSAGEWINDOWSTRINGGRAPHPOINT(1), AttackerName) : this->msg.Attack(MESSAGEWINDOWSTRINGGRAPHPOINT(1), AttackerName);
		else EngageAttack ? this->msg.EngageMagic(MESSAGEWINDOWSTRINGGRAPHPOINT(1), AttackerName, SkillName) : this->msg.CastSpell(MESSAGEWINDOWSTRINGGRAPHPOINT(1), AttackerName, SkillName, IgnorePlayerInstructionFlag);
	}

	void BaseBattle::GraphMessageScreenAfterExecuteAttackCommandFromEnemy(const size_t Crasher, const SkillA& AttackInfo) const {
		bool CriticalFlag = false;
		const int DamagePoint = (AttackInfo.Name == "N/A" || AttackInfo.Name.empty()) ? this->ExecuteAttackCommandFromEnemy(Crasher, CriticalFlag) : this->ExecuteAttackCommandFromEnemy(Crasher, AttackInfo);
		const __int64 start = GetCurrentClockOnMilliSeconds();
		COMMONWAIT(start, this->CommonLoopCondition()) {
			KgDxGraphic::NoExcept::Clear();
			this->CommonGraphicDuringExecutingCommand(this->Characters[3].Name, AttackInfo.Name, false, false);
			this->msg.Crash(MESSAGEWINDOWSTRINGGRAPHPOINT(2), this->Characters[Crasher].Name, DamagePoint, CriticalFlag);
			if (this->Characters[Crasher].HP.IsMin()) this->msg.Dead(MESSAGEWINDOWSTRINGGRAPHPOINT(3), this->Characters[Crasher].Name);
			KgDxGraphic::NoExcept::Flip();
		}
	}

	void BaseBattle::GraphMessageScreenAfterExecuteRangeAttackCommandFromEnemy(const size_t Crasher, const SkillA& AttackInfo) const {
		if (this->GraphAllDamageWhenRangeAttack) {
			const std::array<int, 3> DamageList = ExecuteAttackCommandFromEnemy(AttackInfo, -1);
			const __int64 start = GetCurrentClockOnMilliSeconds();
			auto DamageGraphLine = [DamageList](const size_t CharacterID) { return GetDamageGraphLine(DamageList, CharacterID); };
			COMMONWAIT(start, this->CommonLoopCondition()) {
				KgDxGraphic::NoExcept::Clear();
				this->CommonGraphicDuringExecutingCommand(this->Characters[3].Name, AttackInfo.Name, false, false);
				if (DamageList[0] != -1) this->msg.Crash(MESSAGEWINDOWSTRINGGRAPHPOINT(DamageGraphLine(0)), this->Characters[0].Name, DamageList.at(0), false);
				if (DamageList[1] != -1) this->msg.Crash(MESSAGEWINDOWSTRINGGRAPHPOINT(DamageGraphLine(1)), this->Characters[1].Name, DamageList.at(1), false);
				if (DamageList[2] != -1) this->msg.Crash(MESSAGEWINDOWSTRINGGRAPHPOINT(DamageGraphLine(2)), this->Characters[2].Name, DamageList.at(2), false);
				KgDxGraphic::NoExcept::Flip();
			}

		}
		else {
			const int DamagePoint = this->ExecuteAttackCommandFromEnemy(AttackInfo);
			const __int64 start = GetCurrentClockOnMilliSeconds();
			COMMONWAIT(start, this->CommonLoopCondition()) {
				KgDxGraphic::NoExcept::Clear();
				this->CommonGraphicDuringExecutingCommand(this->Characters[3].Name, AttackInfo.Name, false, false);
				this->msg.RangeCrash(MESSAGEWINDOWSTRINGGRAPHPOINT(2), this->Characters[Crasher].Name, DamagePoint);
				KgDxGraphic::NoExcept::Flip();
			}
		}
	}

}
