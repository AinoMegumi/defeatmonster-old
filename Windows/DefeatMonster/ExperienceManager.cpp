﻿#include "ExperienceManager.hpp"

ExperienceManager::ExperienceManager()
	: DeadCount(0, 9, 0), ElementalAdvantageAttackCount(0), ElementalInferiorityAttackCount(0) {}

void ExperienceManager::AddDeadCount() { this->DeadCount++; }

void ExperienceManager::AddElementalAdvantageAttackCount() { this->ElementalAdvantageAttackCount++; }

void ExperienceManager::AddElementalInferiorityAttackCount() { this->ElementalInferiorityAttackCount++; }

size_t ExperienceManager::CalcExperience(const size_t BaseExp, const BodyParameter<int>& HPRef) const noexcept {
	if (HPRef.RealParameter.IsMin()) return 0;
	constexpr size_t i = 1;
	return ((BaseExp + this->ElementalAdvantageAttackCount - this->ElementalInferiorityAttackCount) / (i + this->DeadCount)).Get();
}

void ExperienceManager::Reset() {
	this->DeadCount = 0;
	this->ElementalAdvantageAttackCount = 0;
	this->ElementalInferiorityAttackCount = 0;
}
