﻿#ifndef __PARTNERCOMMANDMANAGER_HPP__
#define __PARTNERCOMMANDMANAGER_HPP__
#include "PlayerCommand.hpp"
#include "CharacterAI.hpp"
#include "ExternalAI.hpp"
#include "AIManager.hpp"

namespace Offline {
	class PartnerCommandManager {
	private:
		standard::ArrayRef<Status, 4> Characters;
		std::reference_wrapper<int> CursorPoint;
		size_t PartnerID;
	public:
		bool AIControl;
	private:
		AIManager<PartnerAI, OfflineExternalPartnerAI> AI;
		PlayerCommand PCmd;
	public:
		PartnerCommandManager(std::array<Status, 4>& Characters, const int BattleStyle, const size_t PartnerID, DllFunctionCallManager& DllFunction, 
			KgDxGraphic::Color& CursorColor, KgDxGraphic::ThrowException::String& StringHandle, int& CursorPoint, int& NameWidth);
		SkillA PlayerControl(std::function<SkillA(PlayerCommand&)> ChoosePlayerCommandFunc);
		SkillA AICommand();
		bool IgnorePlayerInstruction(SkillA& PlayerInstruction);
	};
}
#endif
