﻿#ifndef __SETUP_HPP__
#define __SETUP_HPP__
#include "Status.hpp"

Status GetPlayerStatus();
Status GetPartnerStatus(const std::string proptag, const std::vector<StatusRawData> Partner);
Status ChooseEnemy(const size_t MaxLevel, const std::string& SavedGamesFolder, const bool ReadUserCharacter);
void CreateAllCharacterStatus(const int PartnerNum);
void SetStrategy(const std::string FirstPartnerName, const std::string SecondPartnerName);
void LoadStrategy(int &FirstPartner, int &SecondPartner);
#endif
