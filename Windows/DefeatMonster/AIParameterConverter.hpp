﻿#ifndef __AIPARAMETERCONVERTER_HPP__
#define __AIPARAMETERCONVERTER_HPP__
#include "AIParameter.h"
#include "Parameter.hpp"

inline AIParameter ToAIParameter(const Parameter& Param) {
	return {
		Param.HP.RealParameter.Get(),
		Param.HP.RealParameter.GetMax(),
		Param.MP.RealParameter.Get(),
		Param.MP.RealParameter.GetMax(),
		Param.Physical.AttackParam.Get(),
		Param.Physical.DefenceParam.Get(),
		Param.Magic.AttackParam.Get(),
		Param.Magic.DefenceParam.Get(),
		Param.MagicCure.Get(),
		Param.Speed.Get(),
		Param.Cleverness.Get()
	};
}
#endif
