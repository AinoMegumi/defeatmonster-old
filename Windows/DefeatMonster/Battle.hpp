﻿#ifndef __BATTLE_HPP__
#define __BATTLE_HPP__
#include "TitleInfo.hpp"
#include "Program.hpp"
#include "CommonGraphicForBattle.hpp"
#include "Coordinate.hpp"
#include "CharacterAI.hpp"
#include "ref.hpp"
#include "Message.hpp"
#include "PlayerCommand.hpp"
#include "Cursor.hpp"
#include "KgDxSound.hpp"
#include "PartnerCommandManager.hpp"
#include "AIManager.hpp"
#include <chrono>
#include <ctime>
constexpr __int64 CommonWaitTime = 2000;
inline __int64 GetCurrentClockOnMilliSeconds() {
	return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now().time_since_epoch()).count();
}
#define COMMONWAIT(start, LoopCondition) for (__int64 elapsed = 0; elapsed < CommonWaitTime && LoopCondition; elapsed = GetCurrentClockOnMilliSeconds() - start)
#define MESSAGEWINDOWGRAPHPOINT 10, WindowHeight - Program::StringSize * 5
#define MESSAGEWINDOWSTRINGGRAPHPOINT(line) 11, WindowHeight - Program::StringSize * (6 - line) + 1
typedef OfflineExternalPartnerAI* (*GetOfflineExternalPartnerAI)(const size_t, const AIBattleStyle&);
typedef OfflineExternalPartnerAI* (*GetOnlineExternalPartnerAI)(const size_t, const AIBattleStyle&);
typedef OfflineExternalEnemyAI* (*GetOfflineExternalEnemyAI)();
typedef OnlineExternalEnemyAI* (*GetOnlineExternalEnemyAI)();

class BaseBattleCreateImpl : public CommonGraphicForBattle {
public:
	static SkillA NormalAttack;
	static SkillA Guard;
	static void LoadBasicInfo();
private:
	MenuWindow EnemySideGaugeBlackField;
	MenuWindow EnemySideNumParameterGraphField;
protected:
	bool GraphAllDamageWhenRangeAttack;
	KgDxGraphic::NoExcept::Picture EnemyGraphic;
	std::reference_wrapper<DllFunctionCallManager> DllFuncMgr;
	Message msg;
	KgDxSound::NoExcept::Sound PlayerSideAttackSound;
	KgDxSound::NoExcept::Sound EnemySideAttackSound;
	KgDxSound::NoExcept::Sound MagicCastSound;
	KgDxSound::NoExcept::Sound MainBattleMusic;
	KgDxSound::NoExcept::Sound DangerBattleMusic;
	KgDxSound::NoExcept::Sound LastSpurtBattleMusic;
protected:
	std::reference_wrapper<int> MasterVolume;
	void GraphEnemyCharacterParameterWithGaugeMode(const int X, const int Y, const Status& sta) const;
	void GraphEnemyCharacterParameterWithNumMode(const int X, const int Y, const Status& sta) const;
	void GraphEnemy() const;
	void ChangeEnemyGraphic(const std::string& EnemyGraphFilePath);
	void StartBattleMusic() const;
	void ChangeMusic(const std::initializer_list<Status>& PlayerSideCharacterList, const Status& EnemyMonster);
	static bool Lose(const std::initializer_list<Status>& PlayerSideCharactersHP);
	static bool Win(const Status EnemySideCharacter);
	void ReloadMemberInfo(const std::initializer_list<Status>& PlayerSideCharacterList, const Status& EnemyMonster);
public:
	BaseBattleCreateImpl(KgDxGraphic::NoExcept::Picture& BackgroundGraph, const std::string& EnemyGraphicFilePath, 
		KgDxGraphic::ThrowException::String& StringHandle, DllFunctionCallManager& DllFuncMgr, int& MasterVolume);
};

namespace Offline {
	enum class InputMode { Back, MainCommand, NormalAttack, ChooseMagic, ChooseCureTarget, Guard, ChooseEnd };
	struct BattleCommonInformation {
		BattleCommonInformation(KgDxInput::Keyboard& KeyInput);
		int CurrentControlTurnArrReferencePoint;
		int Control;
		std::array<int, 3> CursorPoints;
		std::array<SkillA, 3> Commands;
		bool EngageAttack;
		KgDxGraphic::Color CursorColor;
		AbleToChangeMaxCursor PlayerCursor;
		AbleToChangeMaxCursor FirstPartnerCursor;
		AbleToChangeMaxCursor SecondPartnerCursor;
		void init(const KgDxGraphic::Color DefaultColor);
	};

	class BaseBattle : public BaseBattleCreateImpl {
	protected:
		standard::ArrayRef<Status, 4> Characters;
	private:
		MenuWindow GaugeModeParameterGraphField;
		MenuWindow NumModeParameterGraphField;
		AIManager<EnemyAI, OfflineExternalEnemyAI> Enemy;
		std::array<int, 4> TurnControlArray;
		// 共通ループ条件

		bool CommonLoopCondition(AbleToChangeMaxCursor& c, const bool ApplyDamageInfo = true) const;
	protected:
		bool CommonLoopCondition(const bool ApplyDamageInfo = true) const;

		// 共通描画系

		void IntroductEnemyMonster(AbleToChangeMaxCursor& c) const;
	private:
		void GraphGaugeGraphField(const int X, const int Y) const;
		void GraphNumGraphField(const int X, const int Y) const;
		void GraphParameterWithGaugeMode(const int X, const int Y) const;
		void GraphParameterWithNumMode(const int X, const int Y) const;
		void GraphEnemyCharacterParameter() const;
		void GraphAttackEffect(const int X1, const int Y1, const int X2, const int Y2, const std::vector<KgDxGraphic::NoExcept::Picture>& EffectGraphic, const int AddBlend, const std::string& AttackerName, const std::string& AttackName, const std::string& EffectSoundPath, const bool EngageAttack, const bool IgnorePlayerInstruction) const;
		void GraphAttackEffectOnEnemy(const std::string& AttackerName, const SkillA& AttackInfo, const bool EngageAttack = false, const bool IgnorePlayerInstruction = false) const;
		void GraphAttackEffectOnPlayerSide(const std::string& AttackerName, const SkillA& AttackInfo, const bool EngageAttack = false, const bool IgnorePlayerInstruction = false) const;
		void GraphAttackEffectOnPlayerSideAll(const std::string& AttackerName, const SkillA& AttackInfo, const bool EngageAttack = false, const bool IgnorePlayerInstruction = false) const;
		void GraphAttackEffectOnPlayerSide(const std::string& AttackerName, const size_t Target, const SkillA& AttackInfo, const bool EngageAttack = false, const bool IgnorePlayerInstruction = false) const;
		void CommonGraphic() const;
		void CommonGraphicDuringExecutingCommand(const std::string& AttackerName, const std::string& SkillName, const bool EngageAttack, const bool IgnorePlayerInstructionFlag) const;
	protected:
		void GraphPlayerSideCharacterParameter() const;
	private:
		void GraphMessageScreenBeforeExecuteAttackOrCureCommand(const std::string& Attacker, const std::string& AttackName, const KgDxSound::NoExcept::Sound& PlaySoundData, 
			const bool EngageAttack = false, const bool IgnorePlayerInstructionFlag = false) const;
		void GraphMessageScreenAfterExecuteAttackCommandToEnemy(const size_t Attacker, const SkillA& AttackInfo, const bool EngageAttack = false) const;
		void GraphMessageScreenAfterExecuteSingleCureCommand(const size_t Healer, const SkillA& CureInfo, const size_t Target, 
			const bool EngageAttack = false, const bool IgnorePlayerInstructionFlag = false) const;
		void GraphMessageScreenAfterExecuteResurrection(const size_t Healer, const size_t Target, const SkillA& CureInfo) const;
		void GraphMessageScreenAfterExecuteRangeCureCommand(const size_t Healer, const SkillA& CureInfo, const bool EngageAttack = false, const bool IgnorePlayerInstructionFlag = false) const;
		void GraphMessageScreenExecuteGuardCommand(const std::string& ExecuteCharacterName) const;
		void GraphMessageScreenAfterExecuteAttackCommandFromEnemy(const size_t Crasher, const SkillA& AttackInfo) const;
		void GraphMessageScreenAfterExecuteRangeAttackCommandFromEnemy(const size_t Crasher, const SkillA& AttackInfo) const;
		void GraphAttackMessage(const std::string& AttackerName, const std::string& SkillName, const bool EngageAttack, const bool IgnorePlayerInstructionFlag) const;
		void ApplyDamageToGraph(AbleToChangeMaxCursor& c) const;
		void ApplyDamageToGraph() const;

		// 初期化系

		void CreateTurnControlArray();
		void ReloadMemberInfo(const std::initializer_list<Status>& PlayerSideCharacterList, const Status& EnemyMonster);

		// コマンド入力・実行系作成ベース関数群

		int ExecuteAttackCommandToEnemy(const size_t Attacker, bool &CriticalFlag) const;
		int ExecuteAttackCommandToEnemy(const size_t Attacker, const SkillA& AttackInfo) const;
		int ExecuteCureCommand(const size_t Healer, const size_t Target, const SkillA& CureMagicInfo) const;
		int ExecuteCureCommand(const size_t Healer, const SkillA& CureMagicInfo) const;
		std::array<int, 3> ExecuteCureCommand(const size_t Healer, const SkillA& CureMagicInfo, const int NoCureCharacterInputNum) const;
		bool ExecuteResurrection(const size_t Healer, const size_t Target, const SkillA& CureMagicInfo) const;
		int ExecuteAttackCommandFromEnemy(const size_t Crasher, bool &CriticalFlag) const;
		int ExecuteAttackCommandFromEnemy(const size_t Crasher, const SkillA& AttackMagicInfo) const;
		int ExecuteAttackCommandFromEnemy(const SkillA& AttackMagicInfo) const;
		std::array<int, 3> ExecuteAttackCommandFromEnemy(const SkillA& AttackMagicInfo, const int DeadCharacterInputNum) const;
		void ControlParameter(AbleToChangeMaxCursor& cursor);
		void ControlEngage(bool& EngageAttack, PlayerCommand& cmd, AbleToChangeMaxCursor& cursor);
		void GraphScreenOnChoosePlayerCommand(const PlayerCommand& cmd, const InputMode& Mode, const int CursorPoint, const std::vector<SkillA>& AllMagic);

		// コマンド入力・実行系

		void ExecutePlayerCommand(const int CursorPoint, bool& EngageAttack, const SkillA& AttackInfo) const;
		void ExecutePartnerCommand(const SkillA& AttackInfo, const size_t AttackerID, const size_t LastCursorPoint, const bool IgnorePlayerInstructionFlag) const;
		void ExecuteEnemySideCommand();
		SkillA ChoosePlayerCommand(AbleToChangeMaxCursor& c, PlayerCommand& cmd, int& CursorPoint, bool& EngageAttack, KgDxGraphic::Color &CursorColor);

		// 判定系

		bool Win() const;
		bool Lose() const;

		// 検索系
		
		size_t GetEngagePartnerID() const noexcept;
		size_t GetAliveFrontCharacter() const noexcept;

		// コマンド入力系
		
		void InputCommand(BattleCommonInformation &bcm, PlayerCommand& cmd, PartnerCommandManager& FirstPartner, PartnerCommandManager& SecondPartner);

		// 音楽制御系
		void ChangeMusic();

		// メイン関数作成用
		void ChangeTurnControl(BattleCommonInformation& bcm, std::array<bool, 3> &AllowAttack, const KgDxGraphic::Color& CursorDefaultColor);
		void InitControl(std::array<bool, 3> &AllowAttack, bool &AlreadyDoubleAttack);
	protected:
		int BattleSection(BattleCommonInformation& bcm, const KgDxGraphic::Color CursorDefaultColor, PlayerCommand& cmd, PartnerCommandManager& FirstPartner, PartnerCommandManager& SecondPartner);
	public:
		BaseBattle(std::array<Status, 4>& Characters, KgDxGraphic::NoExcept::Picture& BackgroundGraph, const std::string& EnemyGraphicFilePath,
			KgDxGraphic::ThrowException::String& StringHandle, DllFunctionCallManager& DllFuncManager, int& MasterVolume);
		virtual long long main(const int FirstPartnerBattleStyle, const int SecondPartnerBattleStyle, const std::string& SavedGamesFolder, const bool LoadUserData) = 0;
	};

	class NormalBattle : public BaseBattle {
	public:
		NormalBattle(std::array<Status, 4>& Characters, KgDxGraphic::NoExcept::Picture& BackgroundGraph, const std::string& EnemyGraphicFilePath,
			KgDxGraphic::ThrowException::String& StringHandle, DllFunctionCallManager& DllFuncManager, int& MasterVolume)
			: BaseBattle(Characters, BackgroundGraph, EnemyGraphicFilePath, StringHandle, DllFuncManager, MasterVolume) {}
		long long main(const int FirstPartnerBattleStyle, const int SecondPartnerBattleStyle, const std::string&, const bool) override;
	};
	class ContinuousDefeat : public BaseBattle {
	private:
		int battlesection(const int FirstPartnerBattleStyle, const int SecondPartnerBattleStyle);
		void ResetMusic();
	public:
		ContinuousDefeat(std::array<Status, 4>& Characters, KgDxGraphic::NoExcept::Picture& BackgroundGraph, const std::string& EnemyGraphicFilePath,
			KgDxGraphic::ThrowException::String& StringHandle, DllFunctionCallManager& DllFuncManager, int& MasterVolume)
			: BaseBattle(Characters, BackgroundGraph, EnemyGraphicFilePath, StringHandle, DllFuncManager, MasterVolume) {}
		long long main(const int FirstPartnerBattleStyle, const int SecondPartnerBattleStyle, const std::string& SavedGamesFolder, const bool UserDataLoadFlag) override;
	};
	class TimeAttack : public BaseBattle {
	public:
		TimeAttack(std::array<Status, 4>& Characters, KgDxGraphic::NoExcept::Picture& BackgroundGraph, const std::string& EnemyGraphicFilePath,
			KgDxGraphic::ThrowException::String& StringHandle, DllFunctionCallManager& DllFuncManager, int& MasterVolume)
			: BaseBattle(Characters, BackgroundGraph, EnemyGraphicFilePath, StringHandle, DllFuncManager, MasterVolume) {}
		time_t main(const int FirstPartnerBattleStyle, const int SecondPartnerBattleStyle, const std::string&, const bool) override;
	};
}

/*namespace Online {
	class BaseBattle {

	};

	class BattleTwoPlayer {
		standard::ArrayRef<Status, 5> Characters;

	};

	class BattleBetweenPlayer {
		standard::ArrayRef<Status, 2> Players;
	};
}*/
#endif
