﻿#include "TitleInfo.hpp"
#include "Program.hpp"
#include "AddVector.hpp"
#include "PartnerCommandManager.hpp"
#include "Color.hpp"
#include "Battle.hpp"
#ifdef _DEBUG
#include "DxMacro.hpp"
#endif

namespace Offline {
	PartnerCommandManager::PartnerCommandManager(std::array<Status, 4>& Characters, const int BattleStyle, const size_t PartnerID, DllFunctionCallManager& DllFunction,
		KgDxGraphic::Color& CursorColor, KgDxGraphic::ThrowException::String& StringHandle, int& CursorPoint, int& NameWidth) 
		: PartnerID(PartnerID), Characters(Characters), AIControl(BattleStyle != 3),
		AI(
			PartnerAI(Characters, BattleStyle == 3 ? (Characters[PartnerID].MagicCure >= 170 ? 1 : 0) : BattleStyle, PartnerID),
			[&Characters, &PartnerID, &BattleStyle]() {
				try {
					DllFunctionCallManager dll(Characters[PartnerID].ExternalAIDllPath);
					auto func = dll.GetFunctionAddress<GetOfflineExternalPartnerAI>("GetOfflineExternalAI");
					return func(PartnerID, static_cast<AIBattleStyle>(BattleStyle == 3 ? 2 : BattleStyle));
				}
				catch (...) { 
					static OfflineExternalPartnerAI* a = nullptr;
					return a;
				}
			}()
		), PCmd(PartnerID, Characters, DllFunction, CursorColor, StringHandle, NameWidth), CursorPoint(CursorPoint) {}

	SkillA PartnerCommandManager::PlayerControl(std::function<SkillA(PlayerCommand&)> ChoosePlayerCommandFunc) {
		return ChoosePlayerCommandFunc(this->PCmd);
	}

	SkillA PartnerCommandManager::AICommand() {
		const SkillA RetData = this->AI.command(Status::CreateParameterArr(this->Characters.get()), this->PartnerID);
		if (RetData.Tag == "amguard") {
			this->Characters[this->PartnerID].Guard = true;
			return BaseBattleCreateImpl::Guard;
		}
		else if (RetData.Tag == "amnormal") return BaseBattleCreateImpl::NormalAttack;
		this->CursorPoint.get() = static_cast<int>(this->AI.GetTarget());
		return RetData;
	}

	bool PartnerCommandManager::IgnorePlayerInstruction(SkillA& PlayerInstruction) {
		// 単体回復魔法かどうか
		auto IsSingleCureMagic = [](const SkillA& JudgementSkill) { return JudgementSkill.SkillType == 1 && !JudgementSkill.ResurrectionEffect; };
		
		auto IsResurrection = [](const SkillA& JudgementSkill) { return JudgementSkill.SkillType == 1 && JudgementSkill.ResurrectionEffect; };

		// 範囲回復魔法かどうか
		auto IsRangeCureMagic = [](const SkillA& JudgementSkill) { return JudgementSkill.SkillType == 2; };
		const SkillA AISkill = this->AI.command(Status::CreateParameterArr(this->Characters.get()), this->PartnerID);
		if (IsSingleCureMagic(AISkill) || IsRangeCureMagic(AISkill)) {
			const int AICureTarget = static_cast<int>(this->AI.GetTarget());
			if (
				(IsSingleCureMagic(AISkill) && IsSingleCureMagic(PlayerInstruction) && this->CursorPoint.get() == AICureTarget) // どっちも単体回復魔法命令で対象がAIと同じケース
				|| IsRangeCureMagic(PlayerInstruction) // 範囲回復魔法命令のケース
				) return false;
			if (AISkill.SkillType != PlayerInstruction.SkillType || IsResurrection(PlayerInstruction)) PlayerInstruction = AISkill;
			if (IsSingleCureMagic(AISkill)) this->CursorPoint.get() = AICureTarget;
			if (this->Characters[this->PartnerID].Guard) this->Characters[this->PartnerID].Guard = false;
			return true;
		}
		else return false;
	}
}
