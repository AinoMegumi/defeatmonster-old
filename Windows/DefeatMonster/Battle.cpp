﻿#include "Battle.hpp"
#include "IncludedReservedData.hpp"
#include "GameEnd.hpp"
#include "Color.hpp"
#include "Setup.hpp"
#include "Win32API.hpp"

namespace Offline {
	long long NormalBattle::main(const int FirstPartnerBattleStyle, const int SecondPartnerBattleStyle, const std::string&, const bool) {
		KgDxInput::Keyboard KeyInput(false);
		BattleCommonInformation bcm(KeyInput);
		const KgDxGraphic::Color CursorDefaultColor = Color::Green;
		bcm.init(CursorDefaultColor);
		bcm.Control = 0;
		PlayerCommand cmd(0, this->Characters.get(), this->DllFuncMgr.get(), bcm.CursorColor, this->StringHandle.get(), this->NameWidthWithGaugeMode);
		PartnerCommandManager FirstPartner(this->Characters.get(), FirstPartnerBattleStyle, 1, this->DllFuncMgr, bcm.CursorColor, this->StringHandle, bcm.CursorPoints[1], this->NameWidthWithGaugeMode);
		PartnerCommandManager SecondPartner(this->Characters.get(), SecondPartnerBattleStyle, 2, this->DllFuncMgr, bcm.CursorColor, this->StringHandle, bcm.CursorPoints[2], this->NameWidthWithGaugeMode);
		this->StartBattleMusic();
		this->IntroductEnemyMonster(bcm.PlayerCursor);
		return this->BattleSection(bcm, CursorDefaultColor, cmd, FirstPartner, SecondPartner);
	}

	int ContinuousDefeat::battlesection(const int FirstPartnerBattleStyle, const int SecondPartnerBattleStyle) {
		KgDxInput::Keyboard KeyInput(false);
		BattleCommonInformation bcm(KeyInput);
		const KgDxGraphic::Color CursorDefaultColor = Color::Green;
		bcm.init(CursorDefaultColor);
		bcm.Control = 0;
		PlayerCommand cmd(0, this->Characters.get(), this->DllFuncMgr.get(), bcm.CursorColor, this->StringHandle.get(), this->NameWidthWithGaugeMode);
		PartnerCommandManager FirstPartner(this->Characters.get(), FirstPartnerBattleStyle, 1, this->DllFuncMgr, bcm.CursorColor, this->StringHandle, bcm.CursorPoints[1], this->NameWidthWithGaugeMode);
		PartnerCommandManager SecondPartner(this->Characters.get(), SecondPartnerBattleStyle, 2, this->DllFuncMgr, bcm.CursorColor, this->StringHandle, bcm.CursorPoints[2], this->NameWidthWithGaugeMode);
		this->IntroductEnemyMonster(bcm.PlayerCursor);
		return this->BattleSection(bcm, CursorDefaultColor, cmd, FirstPartner, SecondPartner);
	}

	void ContinuousDefeat::ResetMusic() {
		auto MusicPlaying = [](const KgDxSound::NoExcept::Sound& MusicData) { return KgDxSound::SoundState::Playing == MusicData.GetSoundState(); };
		if (MusicPlaying(this->LastSpurtBattleMusic)) {
			this->MainBattleMusic.Delete();
			this->MainBattleMusic.Load();
			this->DangerBattleMusic.Delete();
			this->DangerBattleMusic.Load();
			this->LastSpurtBattleMusic.Stop();
			this->MainBattleMusic.Play(KgDxSound::PlayType::Loop);
			this->LastSpurtBattleMusic.Delete();
			this->LastSpurtBattleMusic.Load();
		}
		else if (MusicPlaying(this->DangerBattleMusic)) {
			this->MainBattleMusic.Delete();
			this->MainBattleMusic.Load();
			this->LastSpurtBattleMusic.Delete();
			this->LastSpurtBattleMusic.Load();
		}
	}

	long long ContinuousDefeat::main(const int FirstPartnerBattleStyle, const int SecondPartnerBattleStyle, const std::string& SavedGamesFolder, const bool LoadUserDataFlag) {
		int DefeatCount = 0;
		this->StartBattleMusic();
		while (Program::LoopCondition()) {
			KgDxGraphic::NoExcept::ChangeCleannessLevel(100);
			const int Result = this->battlesection(FirstPartnerBattleStyle, SecondPartnerBattleStyle);
			if (Result != 2) break;
			for (size_t i = 0; i < 3; i++) this->Characters[i].CalcExp(100);
			DefeatCount++;
			for (size_t i = 0; i < 3; i++) this->Characters[i].MP.Refresh();
			const __int64 start = GetCurrentClockOnMilliSeconds();
			this->ResetMusic();
			COMMONWAIT(start, this->CommonLoopCondition()) {
				KgDxGraphic::NoExcept::Clear();
				KgDxGraphic::NoExcept::ChangeCleannessLevel(100);
				this->GraphBackground();
				KgDxGraphic::NoExcept::ChangeCleannessLevel(70);
				this->GraphPlayerSideCharacterParameter();
				this->MessageWindow.Print(MESSAGEWINDOWGRAPHPOINT);
				KgDxGraphic::NoExcept::Flip();
			}
			this->Characters[3] = ChooseEnemy(
				std::max({ this->Characters[0].Level.GetCurrentLevel(), this->Characters[1].Level.GetCurrentLevel(), this->Characters[2].Level.GetCurrentLevel() }),
				SavedGamesFolder, LoadUserDataFlag
			);
			const std::string EnemyGraphPath = FileExists(Program::ChangeFullPath(".\\Graphic\\Enemy\\" + Characters[3].IdentifiedTag + ".png"))
				? Program::ChangeFullPath(".\\Graphic\\Enemy\\" + Characters[3].IdentifiedTag + ".png")
				: Program::ChangeFullPath(SavedGamesFolder + "\\Kamioda Games\\Kamioda Warrior\\DefeatMonster\\Graphic\\Enemy\\" + Characters[3].IdentifiedTag + ".png");
			this->ChangeEnemyGraphic(EnemyGraphPath);
		}
		return DefeatCount;
	}

	time_t TimeAttack::main(const int FirstPartnerBattleStyle, const int SecondPartnerBattleStyle, const std::string&, const bool) {
		KgDxInput::Keyboard KeyInput(false);
		BattleCommonInformation bcm(KeyInput);
		const KgDxGraphic::Color CursorDefaultColor = Color::Green;
		bcm.init(CursorDefaultColor);
		bcm.Control = 0;
		PlayerCommand cmd(0, this->Characters.get(), this->DllFuncMgr.get(), bcm.CursorColor, this->StringHandle.get(), this->NameWidthWithGaugeMode);
		PartnerCommandManager FirstPartner(this->Characters.get(), FirstPartnerBattleStyle, 1, this->DllFuncMgr, bcm.CursorColor, this->StringHandle, bcm.CursorPoints[1], this->NameWidthWithGaugeMode);
		PartnerCommandManager SecondPartner(this->Characters.get(), SecondPartnerBattleStyle, 2, this->DllFuncMgr, bcm.CursorColor, this->StringHandle, bcm.CursorPoints[2], this->NameWidthWithGaugeMode);
		this->StartBattleMusic();
		this->IntroductEnemyMonster(bcm.PlayerCursor);
		const time_t start = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
		const int Result = this->BattleSection(bcm, CursorDefaultColor, cmd, FirstPartner, SecondPartner);
		return Result != 2 ? Result : std::chrono::system_clock::to_time_t(std::chrono::system_clock::now()) - start;
	}
}
