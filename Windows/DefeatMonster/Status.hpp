﻿#ifndef __STATUS_HPP__
#define __STATUS_HPP__
#include "BodyParameter.hpp"
#include "UseDamageCalculationStatus.hpp"
#include "SpeedManager.hpp"
#include "DamageManager.hpp"
#include "ExperienceManager.hpp"
#include "StatusRawData.hpp"
#include "LevelManager.hpp"
#include "Skill.hpp"
#include "AIParameterConverter.hpp"

class Status : public Parameter {
public:
	std::string Name;
	std::string IdentifiedTag;
	std::string ExternalAIDllPath;
	int SexID;
	LevelManager Level;
private:
	std::vector<SkillA> AttackMagic;
	std::vector<SkillA> SingleCureMagic;
	std::vector<SkillA> RangeCureMagic;
public:
	bool IsEngageCharacter;
	bool Guard;
private:
	ExperienceManager ExpManager;
	std::vector<SkillA> CreateAbleToUseMagicList(const std::vector<SkillA> BaseMagicList);
	void CreateMagicList(const int MPParam);
public:
	Status();
private:
	Status(const StatusRawData& Raw, LevelManager&& Level);
	Status(const StatusRawData& Raw);
public:
	static Status CreatePlayerStatus(const StatusRawData& Raw);
	static Status CreatePartnerStatus(const StatusRawData& Raw);
	static Status CreateEnemyStatus(const StatusRawData& Raw);
	// Status(const StatusRawData Raw, const size_t ReservedLevel);
	// Status(const StatusRawData Raw, const size_t MaxLevel);
	void CreateAbleToUseMagicList();
	int Crash(const Status& Attacker, bool &CriticalFlag);
	int Crash(const SkillA& AttackSkill, const Status& Attacker);
	int Cure(const Status Healer, const SkillA CureMagic);
	bool Resurrection(const Status Healer);
	bool Dead() const noexcept;
	void CureMP();
	void AddSpecialAttackMagic(const SkillA Magic);
	void AddSpecialSingleCureMagic(const SkillA Magic);
	void AddSpecialRangeCureMagic(const SkillA Magic);
	void ClearRangeCureMagic();
	void RemoveReservedMagic(const std::string Name);
	void ApplyEngageToAttack(BodyParameter<int> &PartnerMP);
	void ApplyEngageToMagicAttack(BodyParameter<int> &PartnerMP);
	void ApplyEngageToMagicCure(BodyParameter<int> &PartnerMP);
	void CleanEngage();
	size_t CalcExp(const size_t BaseExp);
	static std::array<Parameter, 4> CreateParameterArr(const std::array<Status, 4>& Arr) {
		return { Arr[0], Arr[1], Arr[2], Arr[3] };
	}
	static std::array<Parameter, 5> CreateParameterArr(const std::array<Status, 5> & Arr) {
		return { Arr[0], Arr[1], Arr[2], Arr[3], Arr[4] };
	}
	static std::array<AIParameter, 4> CreateParameterArr(const std::array<Parameter, 4>& Arr) {
		return { ToAIParameter(Arr[0]), ToAIParameter(Arr[1]), ToAIParameter(Arr[2]), ToAIParameter(Arr[3]) };
	}
	static std::array<AIParameter, 5> CreateParameterArr(const std::array<Parameter, 5> & Arr) {
		return { ToAIParameter(Arr[0]), ToAIParameter(Arr[1]), ToAIParameter(Arr[2]), ToAIParameter(Arr[3]), ToAIParameter(Arr[4]) };
	}

};
#endif
