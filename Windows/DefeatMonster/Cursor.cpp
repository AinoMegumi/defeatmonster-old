﻿#include "TitleInfo.hpp"
#include "Program.hpp"
#include "CppIniRead.hpp"
#include "Cursor.hpp"
#include "DxMacro.hpp"
#include "Win32API.hpp"

Cursor::Cursor(int& CursorVariable, const int ChooseTotal, const bool UseLongPress)
	: Max(ChooseTotal - 1), CursorPoint(CursorVariable), Key(UseLongPress) {
	auto MakeKeyPair = [](const std::string KeyID, const std::pair<unsigned long, unsigned long> Default) { 
		try {
			return std::make_pair(
				Program::RegistryReader::RegReadKeyConfig(KeyID + "Key1", Default.first), 
				Program::RegistryReader::RegReadKeyConfig(KeyID + "Key2", Default.second)
			);
		}
		catch (...) { return Default; }
	};
	static const std::array<std::string, 7> KeyIDList = { "Up", "Down", "Left", "Right", "Enter", "Cancel", "Engage" };
	static const std::array<std::pair<unsigned long, unsigned long>, 7> DefaultKeyList = { {
		{ W_KEY, UP_KEY }, { S_KEY, DOWN_KEY },{ A_KEY, LEFT_KEY }, { D_KEY, RIGHT_KEY }, { SPACE_KEY, RETURN_KEY }, { B_KEY, ESCAPE_KEY }, { LSHIFT_KEY, RSHIFT_KEY }
	} };
	for (size_t i = 0; i < 7 && Program::LoopCondition(); i++) this->KeyConfigList[i] = MakeKeyPair(KeyIDList[i], DefaultKeyList[i]);
}

bool Cursor::Fllush() {
	return this->Key.Fllush();
}

void Cursor::Update() {
	this->Key.Update();
}

bool Cursor::Pressed(const KeyID KeyPairID) const {
	return this->Key[this->KeyConfigList[static_cast<size_t>(KeyPairID)].first]
		|| this->Key[this->KeyConfigList[static_cast<size_t>(KeyPairID)].second];
}

bool Cursor::Up() const { return Pressed(KeyID::UpKey); }

bool Cursor::Down() const { return Pressed(KeyID::DownKey); }

bool Cursor::Left() const { return Pressed(KeyID::LeftKey); }

bool Cursor::Right() const { return Pressed(KeyID::RightKey); }

bool Cursor::Enter() const { return Pressed(KeyID::EnterKey); }

bool Cursor::Cancel() const {	return Pressed(KeyID::CancelKey); }

bool Cursor::UseEngage() const { return Pressed(KeyID::EngageKey); }

bool Cursor::GraphParameterWithNum() const {
	return this->Key[LCONTROL_KEY] || this->Key[RCONTROL_KEY];
}

void Cursor::MoveCursor() const {
	if (this->Up()) {
		this->CursorPoint.get() = (this->CursorPoint.get() == 0 ? this->Max : this->CursorPoint.get() - 1);
	}
	if (this->Down()) {
		this->CursorPoint.get() = (this->CursorPoint.get() == this->Max ? 0 : this->CursorPoint.get() + 1);
	}
}

void Cursor::ReloadKeyConfig() {
	IniRead ini(
		FileExists(Program::RegistryReader::UserIniConfigFilePath)
		? Program::RegistryReader::UserIniConfigFilePath
		: Program::ChangeFullPath(".\\System\\default.ini")
	);
	auto MakeKeyPair = [&ini](const std::string KeyID, const std::pair<unsigned long, unsigned long> Default) {
		try {
			return std::make_pair(
				ini.GetNum("KeyConfig", KeyID + "Key1", Default.first),
				ini.GetNum("KeyConfig", KeyID + "Key2", Default.second)
				);
		}
		catch (...) { return Default; }
	};
	static const std::array<std::string, 7> KeyIDList = { "Up", "Down", "Left", "Right", "Enter", "Cancel", "Engage" };
	static const std::array<std::pair<unsigned long, unsigned long>, 7> DefaultKeyList = { {
		{ W_KEY, UP_KEY }, { S_KEY, DOWN_KEY },{ A_KEY, LEFT_KEY }, { D_KEY, RIGHT_KEY }, { SPACE_KEY, RETURN_KEY }, { B_KEY, ESCAPE_KEY }, { LSHIFT_KEY, RSHIFT_KEY }
	} };
	for (size_t i = 0; i < 7 && Program::LoopCondition(); i++) this->KeyConfigList[i] = MakeKeyPair(KeyIDList[i], DefaultKeyList[i]);
}

AbleToChangeMaxCursor::AbleToChangeMaxCursor(KgDxInput::Keyboard& KeyInput, int& CursorVariable)
	: Key(KeyInput), ChooseTotal(), CursorPoint(CursorVariable) {
	auto MakeKeyPair = [](const std::string KeyID, const std::pair<unsigned long, unsigned long> Default) {
		try { return std::make_pair(Program::RegistryReader::RegReadKeyConfig(KeyID + "Key1", Default.first), Program::RegistryReader::RegReadKeyConfig(KeyID + "Key2", Default.second)); }
		catch (...) { return Default; }
	};
	static const std::array<std::string, 7> KeyIDList = { "Up", "Down", "Left", "Right", "Enter", "Cancel", "Engage" };
	static const std::array<std::pair<unsigned long, unsigned long>, 7> DefaultKeyList = { {
		{ W_KEY, UP_KEY },{ S_KEY, DOWN_KEY },{ A_KEY, LEFT_KEY },{ D_KEY, RIGHT_KEY },{ SPACE_KEY, RETURN_KEY },{ B_KEY, ESCAPE_KEY },{ LSHIFT_KEY, RSHIFT_KEY }
		} };
	for (size_t i = 0; i < 7 && Program::LoopCondition(); i++) this->KeyConfigList[i] = MakeKeyPair(KeyIDList[i], DefaultKeyList[i]);
}

void AbleToChangeMaxCursor::Reset() {
	this->Key.get().Reset();
}

bool AbleToChangeMaxCursor::Fllush() {
	return this->Key.get().Fllush();
}

void AbleToChangeMaxCursor::Update() {
	this->Key.get().Update();
}

bool AbleToChangeMaxCursor::Pressed(const KeyID KeyPairID) const {
	return this->Key.get()[this->KeyConfigList[static_cast<size_t>(KeyPairID)].first]
		|| this->Key.get()[this->KeyConfigList[static_cast<size_t>(KeyPairID)].second];
}

bool AbleToChangeMaxCursor::Up() const { return Pressed(KeyID::UpKey); }

bool AbleToChangeMaxCursor::Down() const { return Pressed(KeyID::DownKey); }

bool AbleToChangeMaxCursor::Left() const { return Pressed(KeyID::LeftKey); }

bool AbleToChangeMaxCursor::Right() const { return Pressed(KeyID::RightKey); }

bool AbleToChangeMaxCursor::Enter() const { 
	bool Flag = Pressed(KeyID::EnterKey);
	return Flag;
}

bool AbleToChangeMaxCursor::Cancel() const { return Pressed(KeyID::CancelKey); }

bool AbleToChangeMaxCursor::UseEngage() const { return Pressed(KeyID::EngageKey); }

bool AbleToChangeMaxCursor::ChangeParameterGraphMode() const {
	return this->Key.get()[LCONTROL_KEY] || this->Key.get()[RCONTROL_KEY];
}

bool AbleToChangeMaxCursor::SpeedApplyDamageInfo() const {
	return this->Key.get()[F3_KEY];
}

bool AbleToChangeMaxCursor::ReloadKey() const {
	return this->Key.get()[F5_KEY] ?
		IDOK == MessageBoxA(
			NULL,
			"iniファイルから現在のコンフィグをリロードします\nレジストリを変更した場合はゲームを再起動してください。\n"
			"※音楽の情報はボリュームのみリロードします。",
			Title, MB_ICONWARNING | MB_OKCANCEL) : false;
}

void AbleToChangeMaxCursor::MoveCursor() const {
	if (this->Up()) {
		this->CursorPoint.get() = (this->CursorPoint.get() == 0 ? this->ChooseTotal - 1 : this->CursorPoint.get() - 1);
	}
	if (this->Down()) {
		this->CursorPoint.get() = (this->CursorPoint.get() == this->ChooseTotal - 1 ? 0 : this->CursorPoint.get() + 1);
	}
}

void AbleToChangeMaxCursor::ReloadKeyInfo() {
	IniRead ini(
		FileExists(Program::RegistryReader::UserIniConfigFilePath)
		? Program::RegistryReader::UserIniConfigFilePath
		: Program::ChangeFullPath(".\\System\\default.ini")
	);
	auto MakeKeyPair = [&ini](const std::string KeyID, const std::pair<unsigned long, unsigned long> Default) {
		try {
			return std::make_pair(
				ini.GetNum("KeyConfig", KeyID + "Key1", Default.first),
				ini.GetNum("KeyConfig", KeyID + "Key2", Default.second)
			);
		}
		catch (...) { return Default; }
	};
	static const std::array<std::string, 7> KeyIDList = { "Up", "Down", "Left", "Right", "Enter", "Cancel", "Engage" };
	static const std::array<std::pair<unsigned long, unsigned long>, 7> DefaultKeyList = { {
		{ W_KEY, UP_KEY }, { S_KEY, DOWN_KEY },{ A_KEY, LEFT_KEY }, { D_KEY, RIGHT_KEY }, { SPACE_KEY, RETURN_KEY }, { B_KEY, ESCAPE_KEY }, { LSHIFT_KEY, RSHIFT_KEY }
	} };
	for (size_t i = 0; i < 7 && Program::LoopCondition(); i++) this->KeyConfigList[i] = MakeKeyPair(KeyIDList[i], DefaultKeyList[i]);
}

#ifdef _DEBUG
bool AbleToChangeMaxCursor::ChangeHP(const int SubKey) const {
	return this->Key.get()[SubKey];
}
#endif

