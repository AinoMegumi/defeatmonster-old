﻿#include "Battle.hpp"

namespace Offline {
	void BaseBattle::ExecutePlayerCommand(const int CursorPoint, bool& EngageAttack, const SkillA& AttackInfo) const {
		if (this->Characters[0].Guard) this->GraphMessageScreenExecuteGuardCommand(this->Characters[0].Name);
		else {
			const size_t EngagePartnerID = this->GetEngagePartnerID();
			if (EngageAttack && (EngagePartnerID == 0 || this->Characters[EngagePartnerID].Dead() || this->Characters[EngagePartnerID].MP.RealParameter.IsMin())) 
				EngageAttack = false;
			switch (AttackInfo.SkillType) {
				case 0: // 攻撃
					// エンゲージアタックをする場合、パラメーターを変更する
					// ここから描画系
					this->GraphMessageScreenBeforeExecuteAttackOrCureCommand(this->Characters[0].Name, AttackInfo.Name,
						AttackInfo.Name == "N/A" ? this->PlayerSideAttackSound : this->MagicCastSound, EngageAttack);
					this->GraphAttackEffectOnEnemy(this->Characters[0].Name, AttackInfo, EngageAttack);
					if (EngageAttack) {
						AttackInfo.Name == "N/A"
							? this->Characters[0].ApplyEngageToAttack(this->Characters[EngagePartnerID].MP)
							: this->Characters[0].ApplyEngageToMagicAttack(this->Characters[EngagePartnerID].MP);
					}
					this->GraphMessageScreenAfterExecuteAttackCommandToEnemy(0, AttackInfo, EngageAttack);
					break;
				case 1: // 単体回復
					this->GraphMessageScreenBeforeExecuteAttackOrCureCommand(this->Characters[0].Name, AttackInfo.Name, this->MagicCastSound, EngageAttack);
					if (EngageAttack) this->Characters[0].ApplyEngageToMagicCure(this->Characters[EngagePartnerID].MP);
					this->GraphAttackEffectOnPlayerSide(this->Characters[0].Name, static_cast<size_t>(CursorPoint), AttackInfo, EngageAttack);
					this->GraphMessageScreenAfterExecuteSingleCureCommand(0, AttackInfo, static_cast<size_t>(CursorPoint), EngageAttack);
					break;
				case 2: // 範囲回復
					this->GraphMessageScreenBeforeExecuteAttackOrCureCommand(this->Characters[0].Name, AttackInfo.Name, this->MagicCastSound, EngageAttack);
					if (EngageAttack) this->Characters[0].ApplyEngageToMagicCure(this->Characters[EngagePartnerID].MP);
					this->GraphAttackEffectOnPlayerSideAll(this->Characters[0].Name, AttackInfo, EngageAttack);
					this->GraphMessageScreenAfterExecuteRangeCureCommand(0, AttackInfo, EngageAttack);
					break;
				default:
					throw KgWinException("SkillType is wrong.");
			}
		}
		this->Characters[0].CleanEngage();
	}

	void BaseBattle::ExecutePartnerCommand(const SkillA& AttackInfo, const size_t AttackerID, const size_t LastCursorPoint, const bool IgnorePlayerInstructionFlag) const {
		if (this->Characters[AttackerID].Guard) this->GraphMessageScreenExecuteGuardCommand(this->Characters[AttackerID].Name);
		else {
			switch (AttackInfo.SkillType) {
				case 0: // 攻撃
					this->GraphMessageScreenBeforeExecuteAttackOrCureCommand(this->Characters[AttackerID].Name, AttackInfo.Name,
						AttackInfo.Name == "N/A" ? this->PlayerSideAttackSound : this->MagicCastSound, false, false);
					this->GraphAttackEffectOnEnemy(this->Characters[AttackerID].Name, AttackInfo);
					this->GraphMessageScreenAfterExecuteAttackCommandToEnemy(AttackerID, AttackInfo);
					break;
				case 1: // 単体回復
					this->GraphMessageScreenBeforeExecuteAttackOrCureCommand(this->Characters[AttackerID].Name, AttackInfo.Name, this->MagicCastSound, false, IgnorePlayerInstructionFlag);
					this->GraphAttackEffectOnPlayerSide(this->Characters[AttackerID].Name, static_cast<size_t>(LastCursorPoint), AttackInfo, false, IgnorePlayerInstructionFlag);
					this->GraphMessageScreenAfterExecuteSingleCureCommand(AttackerID, AttackInfo, LastCursorPoint, false, IgnorePlayerInstructionFlag);
					break;
				case 2: // 範囲回復
					this->GraphMessageScreenBeforeExecuteAttackOrCureCommand(this->Characters[AttackerID].Name, AttackInfo.Name, this->MagicCastSound, false, IgnorePlayerInstructionFlag);
					this->GraphAttackEffectOnPlayerSideAll(this->Characters[AttackerID].Name, AttackInfo, false, IgnorePlayerInstructionFlag);
					this->GraphMessageScreenAfterExecuteRangeCureCommand(AttackerID, AttackInfo, false, IgnorePlayerInstructionFlag);
					break;
				default:
					throw KgWinException("SkillType is wrong.");
			}
		}
	}

	void BaseBattle::ExecuteEnemySideCommand() {
		const SkillA AttackInfo = this->Enemy.command(Status::CreateParameterArr(this->Characters.get()), 3);
		if (AttackInfo.Tag == "amguard") this->Characters[3].Guard = true;
		if (this->Characters[3].Guard) this->GraphMessageScreenExecuteGuardCommand(this->Characters[3].Name);
		else {
			switch (AttackInfo.SkillType) {
				case 0: // 攻撃
					this->GraphMessageScreenBeforeExecuteAttackOrCureCommand(this->Characters[3].Name, AttackInfo.Name,
						AttackInfo.Name == "N/A" ? this->EnemySideAttackSound : this->MagicCastSound);
					AttackInfo.Range
						? [this, AttackInfo]() {
						this->GraphAttackEffectOnPlayerSide(this->Characters[3].Name, AttackInfo);
						this->GraphMessageScreenAfterExecuteRangeAttackCommandFromEnemy(this->GetAliveFrontCharacter(), AttackInfo);
					}()
						: [this, AttackInfo]() {
						this->GraphAttackEffectOnPlayerSide(this->Characters[3].Name, this->Enemy.GetTarget(), AttackInfo);
						this->GraphMessageScreenAfterExecuteAttackCommandFromEnemy(this->Enemy.GetTarget(), AttackInfo);
					}();
					break;
				case 1: // 単体回復
					this->GraphMessageScreenBeforeExecuteAttackOrCureCommand(this->Characters[3].Name, AttackInfo.Name, this->MagicCastSound);
					this->GraphAttackEffectOnEnemy(this->Characters[3].Name, AttackInfo);
					this->GraphMessageScreenAfterExecuteSingleCureCommand(3, AttackInfo, 3);
					break;
				default:
					throw KgWinException("SkillType is wrong.");
			}
		}
	}
}
