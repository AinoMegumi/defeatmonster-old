﻿#ifndef __EXTERNALAI_HPP__
#define __EXTERNALAI_HPP__
#include "AIParameter.h"
#include "AISkill.h"
#include <random>
typedef int (*RandGenerateFunc)(const int, const int);

enum class AIBattleStyle : unsigned int { AttackMain = 0, Balance = 1, CureMain = 2 };

class OfflineExternalPartnerAI {
public:
	virtual const char* command(const AIParameterList CurrentParameter, const AISkillList AttackMagic, const AISkillList SingleCureMagic, const AISkillList RangeCureMagic) noexcept = 0;
	virtual size_t GetCureTarget() noexcept = 0;
	size_t GetTarget() noexcept { return this->GetCureTarget(); }
};

class OfflineExternalEnemyAI {
public:
	virtual const char* command(const AIParameterList CurrentParameter, const AISkillList AttackMagic, const AISkillList SingleCureMagic, const AISkillList RangeCureMagic) noexcept = 0;
	virtual size_t GetAttackTarget() noexcept = 0;
	size_t GetTarget() noexcept { return this->GetAttackTarget(); }
};

class OnlineExternalPartnerAI {
	virtual const char* command(const AIParameterList CurrentParameter, const AISkillList AttackMagic, const AISkillList SingleCureMagic, const AISkillList RangeCureMagic) noexcept = 0;
	virtual size_t GetCureTarget() noexcept = 0;
	size_t GetTarget() noexcept { return this->GetCureTarget(); }
};

class OnlineExternalEnemyAI {
public:
	virtual const char* command(const AIParameterList CurrentParameter, const AISkillList AttackMagic, const AISkillList SingleCureMagic, const AISkillList RangeCureMagic) noexcept = 0;
	virtual size_t GetAttackTarget() noexcept = 0;
	size_t GetTarget() noexcept { return this->GetAttackTarget(); }
};
#endif
