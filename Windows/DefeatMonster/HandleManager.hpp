﻿#ifndef __HANDLEMANAGER_HPP__
#define __HANDLEMANAGER_HPP__
#include <Windows.h>
#include <functional>
#include <utility>

namespace ClassMember {
	class HandleManager {
	private:
		std::reference_wrapper<HANDLE> ManagementHandle;
	public:
		HandleManager() = delete;
		HandleManager(HANDLE& handle) : ManagementHandle(handle) {}
		~HandleManager() { CloseHandle(this->ManagementHandle.get()); }
	};
}

namespace Normal {
	class HandleManager {
	private:
		HANDLE ManagementHandle;
	public:
		HandleManager() = default;
		HandleManager(HANDLE&& handle) : ManagementHandle(std::move(handle)) {}
		HandleManager(const HandleManager&) = delete;
		HandleManager(HandleManager&& hdmgr) noexcept : ManagementHandle(hdmgr.ManagementHandle) {}
		HandleManager& operator = (const HandleManager&) = delete;
		HandleManager& operator = (HandleManager&& hdmgr) noexcept {
			this->ManagementHandle = std::move(hdmgr.ManagementHandle);
			return *this;
		}
		~HandleManager() { CloseHandle(this->ManagementHandle); }
		HANDLE* operator & () { return &this->ManagementHandle; }
		HANDLE Get() const noexcept { return this->ManagementHandle; }
	};
}
#endif
