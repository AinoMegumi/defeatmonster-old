﻿#ifdef _DEBUG
#include "BattleSimulator.hpp"
constexpr int MinusNumFromWidth = WindowWidth % 3;
constexpr int BaseWidth = (WindowWidth - MinusNumFromWidth) / 3;

static inline std::vector<KgDxGraphic::NoExcept::Picture> LoadEffectGraphic(const std::string& EffectGraphPath) {
	return KgDxGraphic::NoExcept::Picture::DivLoad(EffectGraphPath, [EffectGraphPath]() {
		KgDxGraphic::DivLoadSettings div{};
		KgDxGraphic::NoExcept::Picture p(EffectGraphPath);
		div.YNum = 1;
		div.AllNum = div.XNum = p.Width / p.Height;
		div.XSize = p.Width / div.XNum;
		div.YSize = p.Height;
		return div;
	}());
}

void BattleSimulator::GraphEnemy(const KgDxGraphic::NoExcept::Picture& EnemyGraphicInfo) const {
	EnemyGraphicInfo.Print((WindowWidth - EnemyGraphicInfo.Width) / 2, (WindowHeight - EnemyGraphicInfo.Height) / 2, true);
}

void BattleSimulator::CommonGraphicDuringExecutingCommand(const std::string& AttackerName, const std::string& SkillName, const KgDxGraphic::NoExcept::Picture& EnemyGraphicInfo, const bool EngageAttack, const bool IgnorePlayerInstructionFlag) const {
	this->CommonGraphic(EnemyGraphicInfo);
	this->MessageWindow.Print(MESSAGEWINDOWGRAPHPOINT);
	KgDxGraphic::NoExcept::ChangeCleannessLevel(100);
	this->GraphAttackMessage(AttackerName, SkillName, EngageAttack, IgnorePlayerInstructionFlag);
}

void BattleSimulator::GraphPlayerSideCharacterParameter() const {
	this->GraphParameterWithNumMode(10, 10);
}

void BattleSimulator::GraphMessageScreenAfterExecuteAttackCommandToEnemy(const size_t Attacker, const SkillA& AttackInfo, const KgDxGraphic::NoExcept::Picture& EnemyGraphicInfo, const bool EngageAttack) const {
	bool CriticalFlag = false;
	const int DamagePoint = (AttackInfo.Name == "N/A" || AttackInfo.Name.empty()) ? this->ExecuteAttackCommandToEnemy(Attacker, CriticalFlag) : this->ExecuteAttackCommandToEnemy(Attacker, AttackInfo);
	const __int64 start = GetCurrentClockOnMilliSeconds();
	COMMONWAIT(start + CommonWaitTime, this->CommonLoopCondition()) {
		KgDxGraphic::NoExcept::Clear();
		this->CommonGraphicDuringExecutingCommand(this->Characters[Attacker].Name, AttackInfo.Name, EnemyGraphicInfo, EngageAttack, false);
		this->msg.DamageToEnemy(MESSAGEWINDOWSTRINGGRAPHPOINT(2), this->Characters[3].Name, DamagePoint, CriticalFlag);
		if (this->Characters[3].HP.IsMin()) this->msg.Defeat(MESSAGEWINDOWSTRINGGRAPHPOINT(3), this->Characters[3].Name);
		KgDxGraphic::NoExcept::Flip();
	}
	this->OutputCommandLog(this->Characters[Attacker].Name, AttackInfo.Name, this->Characters[3].Name, DamagePoint);
}

void BattleSimulator::GraphMessageScreenAfterExecuteSingleCureCommand(const size_t Healer, const SkillA& CureInfo, const size_t Target, const KgDxGraphic::NoExcept::Picture& EnemyGraphicInfo, const bool EngageAttack, const bool IgnorePlayerInstructionFlag) const {
	// リザレクションの実行は生きているかによって変わる
	if (CureInfo.ResurrectionEffect && this->Characters[Target].Dead()) return this->GraphMessageScreenAfterExecuteResurrection(Healer, Target, CureInfo, EnemyGraphicInfo);
	// リザレクションを実行したが生きている場合は0として処理
	const int CurePoint = CureInfo.ResurrectionEffect ? 0 : this->ExecuteCureCommand(Healer, Target, CureInfo);
	const __int64 start = GetCurrentClockOnMilliSeconds();
	COMMONWAIT(start + CommonWaitTime, this->CommonLoopCondition()) {
		KgDxGraphic::NoExcept::Clear();
		this->CommonGraphicDuringExecutingCommand(this->Characters[Healer].Name, CureInfo.Name, EnemyGraphicInfo, EngageAttack, IgnorePlayerInstructionFlag);
		this->msg.Cure(MESSAGEWINDOWSTRINGGRAPHPOINT(2), this->Characters[Target].Name, CurePoint);
		KgDxGraphic::NoExcept::Flip();
	}
	this->OutputCommandLog(this->Characters[Healer].Name, CureInfo.Name, this->Characters[Target].Name, CurePoint);
}

void BattleSimulator::GraphMessageScreenAfterExecuteResurrection(const size_t Healer, const size_t Target, const SkillA& CureInfo, const KgDxGraphic::NoExcept::Picture& EnemyGraphicInfo) const {
	const bool Result = this->ExecuteResurrection(Healer, Target, CureInfo);
	const __int64 start = GetCurrentClockOnMilliSeconds();
	COMMONWAIT(start + CommonWaitTime, this->CommonLoopCondition()) {
		KgDxGraphic::NoExcept::Clear();
		this->CommonGraphicDuringExecutingCommand(this->Characters[Healer].Name, CureInfo.Name, EnemyGraphicInfo, false, false);
		this->msg.Ressurection(MESSAGEWINDOWSTRINGGRAPHPOINT(2), this->Characters[Target].Name, Result);
		KgDxGraphic::NoExcept::Flip();
	}
	this->OutputCommandLog(this->Characters[Healer].Name, CureInfo.Name, this->Characters[Target].Name, this->Characters[Target].HP.RealParameter.Get());
}

void BattleSimulator::GraphMessageScreenAfterExecuteRangeCureCommand(const size_t Healer, const SkillA& CureInfo, const KgDxGraphic::NoExcept::Picture& EnemyGraphicInfo, const bool EngageAttack, const bool IgnorePlayerInstructionFlag) const {
	const __int64 start = GetCurrentClockOnMilliSeconds();
	const int CureAverage = this->ExecuteCureCommand(Healer, CureInfo);
	COMMONWAIT(start + CommonWaitTime, this->CommonLoopCondition()) {
		KgDxGraphic::NoExcept::Clear();
		this->CommonGraphicDuringExecutingCommand(this->Characters[Healer].Name, CureInfo.Name, EnemyGraphicInfo, EngageAttack, IgnorePlayerInstructionFlag);
		this->msg.RangeCure(MESSAGEWINDOWSTRINGGRAPHPOINT(2), this->Characters[Healer].Name, CureAverage);
		KgDxGraphic::NoExcept::Flip();
	}
	this->OutputCommandLog(this->Characters[Healer].Name, CureInfo.Name, CureAverage);
}

void BattleSimulator::GraphMessageScreenExecuteGuardCommand(const std::string& ExecuteCharacterName, const KgDxGraphic::NoExcept::Picture& EnemyGraphicInfo) const {
	const __int64 start = GetCurrentClockOnMilliSeconds();
	COMMONWAIT(start + CommonWaitTime, this->CommonLoopCondition()) {
		KgDxGraphic::NoExcept::Clear();
		this->CommonGraphic(EnemyGraphicInfo);
		this->MessageWindow.Print(MESSAGEWINDOWGRAPHPOINT);
		KgDxGraphic::NoExcept::ChangeCleannessLevel(100);
		this->msg.Defence(MESSAGEWINDOWSTRINGGRAPHPOINT(1), ExecuteCharacterName);
		KgDxGraphic::NoExcept::Flip();
	}
	this->OutputCommandLog(ExecuteCharacterName, "Defence", this->Characters[3].Name, 0);
}

void BattleSimulator::GraphAttackMessage(const std::string& AttackerName, const std::string& SkillName, const bool EngageAttack, const bool IgnorePlayerInstructionFlag) const {
	if (SkillName == "N/A" || SkillName.empty()) EngageAttack ? this->msg.EngageAttack(MESSAGEWINDOWSTRINGGRAPHPOINT(1), AttackerName) : this->msg.Attack(MESSAGEWINDOWSTRINGGRAPHPOINT(1), AttackerName);
	else EngageAttack ? this->msg.EngageMagic(MESSAGEWINDOWSTRINGGRAPHPOINT(1), AttackerName, SkillName) : this->msg.CastSpell(MESSAGEWINDOWSTRINGGRAPHPOINT(1), AttackerName, SkillName, IgnorePlayerInstructionFlag);
}

void BattleSimulator::GraphMessageScreenAfterExecuteAttackCommandFromEnemy(const size_t Crasher, const SkillA& AttackInfo, const KgDxGraphic::NoExcept::Picture& EnemyGraphicInfo) const {
	bool CriticalFlag = false;
	const int DamagePoint = (AttackInfo.Name == "N/A" || AttackInfo.Name.empty()) ? this->ExecuteAttackCommandFromEnemy(Crasher, CriticalFlag) : this->ExecuteAttackCommandFromEnemy(Crasher, AttackInfo);
	const __int64 start = GetCurrentClockOnMilliSeconds();
	COMMONWAIT(start + CommonWaitTime, this->CommonLoopCondition()) {
		KgDxGraphic::NoExcept::Clear();
		this->CommonGraphicDuringExecutingCommand(this->Characters[3].Name, AttackInfo.Name, EnemyGraphicInfo, false, false);
		this->msg.Crash(MESSAGEWINDOWSTRINGGRAPHPOINT(2), this->Characters[Crasher].Name, DamagePoint, CriticalFlag);
		if (this->Characters[Crasher].HP.IsMin()) this->msg.Dead(MESSAGEWINDOWSTRINGGRAPHPOINT(3), this->Characters[Crasher].Name);
		KgDxGraphic::NoExcept::Flip();
	}
	this->OutputCommandLog(this->Characters[3].Name, AttackInfo.Name, this->Characters[Crasher].Name, DamagePoint);
}

void BattleSimulator::GraphMessageScreenAfterExecuteRangeAttackCommandFromEnemy(const size_t Crasher, const SkillA& AttackInfo, const KgDxGraphic::NoExcept::Picture& EnemyGraphicInfo) const {
	const int DamagePoint = this->ExecuteAttackCommandFromEnemy(AttackInfo);
	const __int64 start = GetCurrentClockOnMilliSeconds();
	COMMONWAIT(start + CommonWaitTime, this->CommonLoopCondition()) {
		KgDxGraphic::NoExcept::Clear();
		this->CommonGraphicDuringExecutingCommand(this->Characters[3].Name, AttackInfo.Name, EnemyGraphicInfo, false, false);
		this->msg.RangeCrash(MESSAGEWINDOWSTRINGGRAPHPOINT(2), this->Characters[Crasher].Name, DamagePoint);
		KgDxGraphic::NoExcept::Flip();
	}
	this->OutputCommandLog(this->Characters[3].Name, AttackInfo.Name, DamagePoint);
}
#endif
