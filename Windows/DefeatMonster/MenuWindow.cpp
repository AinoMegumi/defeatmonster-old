﻿#include "MenuWindow.hpp"

MenuWindow::MenuWindow(const int Width, const int Height, const KgDxGraphic::Color WindowColor, const KgDxGraphic::Color FrameColor)
	: Main(KgDxGraphic::NoExcept::Box(Width, Height, WindowColor)), Frame(KgDxGraphic::NoExcept::Box(Width, Height, FrameColor)), Width(Width), Height(Height) {}

void MenuWindow::Print(const int X, const int Y) const {
	this->Main.Print(X, Y, true);
	this->Frame.Print(X, Y, false);
}
