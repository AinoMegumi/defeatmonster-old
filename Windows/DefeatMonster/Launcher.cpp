﻿#include "TitleInfo.hpp"
#include "Program.hpp"
#include "Color.hpp"
#include "Cursor.hpp"
#include "DxMacro.hpp"
#include <Windows.h>

void Launch() {
#ifndef _DEBUG
	KgDxGraphic::NoExcept::Picture LaunchGraphic(Program::ChangeFullPath(".\\Graphic\\System\\Launch.png"));
	LaunchGraphic.Load();
	for (int i = 0; i <= 100 && Program::LoopCondition(); i++) {
		Program::Fps.Update();
		KgDxGraphic::NoExcept::Clear();
		KgDxGraphic::NoExcept::ChangeBrightLevel(i);
		LaunchGraphic.Print(0, 0, false);
		KgDxGraphic::NoExcept::Flip();
		Program::Fps.Wait();
	}
	Sleep(5000);
	for (int i = 0; i <= 100 && Program::LoopCondition(); i++) {
		Program::Fps.Update();
		KgDxGraphic::NoExcept::Clear();
		KgDxGraphic::NoExcept::ChangeBrightLevel(100 - i);
		LaunchGraphic.Print(0, 0, false);
		KgDxGraphic::NoExcept::Flip();
		Program::Fps.Wait();
	}
#endif
}

void GraphTitle(const KgDxGraphic::NoExcept::Picture TitleGraphic) {
	int temp = 0;
	Cursor key(temp);
	while (Program::LoopCondition() && !key.Enter()) {
		key.Fllush();
		Program::Fps.Update();
		KgDxGraphic::NoExcept::Clear();
		TitleGraphic.Print(0, 0, false);
		KgDxGraphic::NoExcept::Flip();
		Program::Fps.Wait();
		key.Update();
	}
}

static inline int GetMaxWidth(const std::array<std::string, 6>& MenuList, const KgDxGraphic::ThrowException::String& StringHandle) {
	std::array<std::string, 6> List = MenuList;
	std::sort(
		List.begin(), List.end(), 
		[&StringHandle](const std::string& a, const std::string& b) { return StringHandle.GetPrintStringWidth(a) > StringHandle.GetPrintStringWidth(b); }
	);
	return StringHandle.GetPrintStringWidth(List.front());
}

int Menu(KgDxGraphic::NoExcept::Picture& TitleGraphic, const KgDxGraphic::ThrowException::String& StringHandle) {
	const std::array<std::string, 6> Buf = []() {
		static const std::string list[] = { "NormalBattle", "ContinuousDefeat", "TimeAttack", "BattleTwoPlayer", "BattleBetweenPlayers", "Exit" };
		std::array<std::string, 6> Buf;
		for (size_t i = 0; i < 6; i++) Buf[i] = Program::LangIniFile.GetString("Launcher", list[i], "");
		return Buf;
	}();
	const int MenuWidth = GetMaxWidth(Buf, StringHandle) + 2;
#ifdef _DEBUG
	const KgDxGraphic::NoExcept::Box Window(MenuWidth, Program::StringSize * 7 + 2, Color::Gray);
	const KgDxGraphic::NoExcept::Box Frame(MenuWidth, Program::StringSize * 7 + 2, Color::White);
#else
	const KgDxGraphic::NoExcept::Box Window(MenuWidth, Program::StringSize * 6 + 2, Color::Gray);
	const KgDxGraphic::NoExcept::Box Frame(MenuWidth, Program::StringSize * 6 + 2, Color::White);
#endif
	const KgDxGraphic::NoExcept::Box CursorGraphic(MenuWidth - 2, Program::StringSize, Color::Green);
	int CursorPoint = 0;
	//Cursor Key(CursorPoint, 6);
#ifdef _DEBUG
	Cursor Key(CursorPoint, 7);
#else
	Cursor Key(CursorPoint, 6);
#endif
	auto ChooseStringColor = [&CursorPoint](const int MenuID) { return CursorPoint == MenuID ? Color::Black : Color::White; };
	while (Program::LoopCondition() && !Key.Enter()) {
		Key.Fllush();
		Program::Fps.Update();
		KgDxGraphic::NoExcept::Clear();
		TitleGraphic.Print(0, 0, WindowWidth, WindowHeight, false);
		Window.Print((WindowWidth - Window.Width) / 2, (WindowHeight - Window.Height) / 2, true);
		Frame.Print((WindowWidth - Window.Width) / 2, (WindowHeight - Window.Height) / 2, false);
		CursorGraphic.Print((WindowWidth - Window.Width) / 2 + 1, (WindowHeight - Window.Height) / 2 + 1 + CursorPoint * Program::StringSize, true);
		StringHandle.Print((WindowWidth - Window.Width) / 2 + 1, (WindowHeight - Window.Height) / 2 + 1, Buf.at(0), ChooseStringColor(0));
		StringHandle.Print((WindowWidth - Window.Width) / 2 + 1, (WindowHeight - Window.Height) / 2 + 1 + Program::StringSize, Buf.at(1), ChooseStringColor(1));
		StringHandle.Print((WindowWidth - Window.Width) / 2 + 1, (WindowHeight - Window.Height) / 2 + 1 + Program::StringSize * 2, Buf.at(2), ChooseStringColor(2));
		StringHandle.Print((WindowWidth - Window.Width) / 2 + 1, (WindowHeight - Window.Height) / 2 + 1 + Program::StringSize * 3, Buf.at(3), ChooseStringColor(3));
		StringHandle.Print((WindowWidth - Window.Width) / 2 + 1, (WindowHeight - Window.Height) / 2 + 1 + Program::StringSize * 4, Buf.at(4), ChooseStringColor(4));
		StringHandle.Print((WindowWidth - Window.Width) / 2 + 1, (WindowHeight - Window.Height) / 2 + 1 + Program::StringSize * 5, Buf.at(5), ChooseStringColor(5));
#ifdef _DEBUG
		StringHandle.Print((WindowWidth - Window.Width) / 2 + 1, (WindowHeight - Window.Height) / 2 + 1 + Program::StringSize * 6, "シミュレーション", ChooseStringColor(6));
#endif
		KgDxGraphic::NoExcept::Flip();
		Program::Fps.Wait();
		Key.Update();
		Key.MoveCursor();
	}
	return CursorPoint;
}

int StartMenu(const KgDxGraphic::ThrowException::String& StringHandle) {
	KgDxGraphic::NoExcept::Clear();
	KgDxGraphic::NoExcept::ChangeBrightLevel(100);
	KgDxGraphic::NoExcept::Picture TitleGraphic(Program::ChangeFullPath(".\\Graphic\\System\\opening.png"));
	TitleGraphic.Load();
	//GraphTitle(TitleGraphic);
	return Menu(TitleGraphic, StringHandle);
}
