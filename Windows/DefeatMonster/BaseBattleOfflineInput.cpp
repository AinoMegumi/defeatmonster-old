﻿#include "Battle.hpp"
#include "Color.hpp"
#include "AddVector.hpp"
#ifdef _DEBUG
#include "DxMacro.hpp"
#endif
namespace Offline {
	static inline InputMode GetNewMode(const InputMode& CurrentMode, AbleToChangeMaxCursor& cursor, 
		int& CursorPoint, const int NewChooseTotalInputedEnter, const int NewChooseTotalInputedCancel,
		const InputMode NewModeInputedEnter, const InputMode NewModeInputedCancel) {
		if (cursor.Cancel()) {
			if (CurrentMode == NewModeInputedCancel) return CurrentMode;
			CursorPoint = 0;
			cursor.ChangeChooseTotal<int>(NewChooseTotalInputedCancel);
			return NewModeInputedCancel;
		}
		else if (cursor.Enter()) {
			if (NewModeInputedEnter != InputMode::ChooseEnd) CursorPoint = 0;
			cursor.ChangeChooseTotal(NewChooseTotalInputedEnter);
			return NewModeInputedEnter;
		}
		return CurrentMode;
	}

	void BaseBattle::ControlParameter(AbleToChangeMaxCursor& cursor) {
#ifdef _DEBUG // パラメーターの増減デバッグ用キーコンフィグ
		if (cursor.ChangeHP(KEY_1)) this->Characters[0].HP -= std::min(25, this->Characters[0].HP.RealParameter.Get() - 1);
		if (cursor.ChangeHP(KEY_2)) this->Characters[1].HP -= std::min(25, this->Characters[1].HP.RealParameter.Get() - 1);
		if (cursor.ChangeHP(KEY_3)) this->Characters[2].HP -= std::min(25, this->Characters[2].HP.RealParameter.Get() - 1);
		if (cursor.ChangeHP(KEY_4)) this->Characters[0].HP -= this->Characters[0].HP.RealParameter.Get() - 1;
		if (cursor.ChangeHP(KEY_5)) this->Characters[1].HP -= this->Characters[1].HP.RealParameter.GetMax();
		if (cursor.ChangeHP(KEY_6)) this->Characters[2].HP -= this->Characters[2].HP.RealParameter.GetMax();
		if (cursor.ChangeHP(KEY_7)) this->Characters[0].HP += 25;
		if (cursor.ChangeHP(KEY_8)) this->Characters[1].HP += 25;
		if (cursor.ChangeHP(KEY_9)) this->Characters[2].HP += 25;
		if (cursor.ChangeHP(KEY_0)) this->Characters[3].HP -= std::min(250, this->Characters[3].HP.RealParameter.Get() - 1);
		if (cursor.ChangeHP(MINUS_KEY)) this->Characters[3].HP += 250;
#else
		UNREFERENCED_PARAMETER(cursor);
#endif
	}

	static inline void ControlModeOnMainCommand(const size_t AllMagicListSize, int& CursorPoint, const std::function<void(const int, const int, const InputMode, const InputMode)>& ChangeMode) {
		switch (CursorPoint) {
			case 0:
				ChangeMode(3, 3, InputMode::NormalAttack, InputMode::Back);
				break;
			case 1:
				ChangeMode(AllMagicListSize == 0 ? 3 : static_cast<int>(AllMagicListSize), 3, AllMagicListSize == 0 ? InputMode::MainCommand : InputMode::ChooseMagic, InputMode::Back);
				break;
			case 2:
				ChangeMode(3, 3, InputMode::Guard, InputMode::Back);
				break;
			default:
				CursorPoint = 0;
				break;
		}
	}

	void BaseBattle::ControlEngage(bool &EngageAttack, PlayerCommand& cmd, AbleToChangeMaxCursor& cursor) {
		if (cmd.CharacterID == 0) {
			if (cursor.UseEngage()) EngageAttack = !EngageAttack;
			if (EngageAttack) {
				if (const size_t EngagePartnerID = this->GetEngagePartnerID();
					EngagePartnerID == 0 || this->Characters[EngagePartnerID].Dead() || this->Characters[EngagePartnerID].MP.RealParameter.IsMin()
					) EngageAttack = false;
			}
		}
		else if (EngageAttack) EngageAttack = false;
	}

	static inline void GraphByMode(const InputMode Mode, const PlayerCommand& cmd, const int CursorPoint, const std::vector<SkillA>& AllMagic) {
		switch (Mode) {
			case InputMode::MainCommand:
				cmd.GraphMainCommand(10, WindowHeight - Program::StringSize * 6, CursorPoint, AllMagic.size());
				break;
			case InputMode::ChooseMagic:
				cmd.GraphMagicList(10, WindowHeight - Program::StringSize * 7, CursorPoint, AllMagic);
				break;
			case InputMode::ChooseCureTarget:
				cmd.GraphTargetList(10, WindowHeight - Program::StringSize * 5, CursorPoint);
				break;
			default:
				break;
		}
	}

	void BaseBattle::GraphScreenOnChoosePlayerCommand(const PlayerCommand& cmd, const InputMode& Mode, const int CursorPoint, const std::vector<SkillA>& AllMagic) {
		KgDxGraphic::NoExcept::Clear();
		KgDxGraphic::NoExcept::ChangeCleannessLevel(100);
		this->GraphBackground();
		this->GraphEnemy();
		KgDxGraphic::NoExcept::ChangeCleannessLevel(70);
		this->GraphPlayerSideCharacterParameter();
		this->GraphEnemyCharacterParameter();
		GraphByMode(Mode, cmd, CursorPoint, AllMagic);
		KgDxGraphic::NoExcept::Flip();
	}

	SkillA BaseBattle::ChoosePlayerCommand(AbleToChangeMaxCursor& cursor, PlayerCommand& cmd, int& CursorPoint, bool& EngageAttack, KgDxGraphic::Color &CursorColor) {
		const std::vector<SkillA> AllMagic = this->Characters[cmd.CharacterID].AbleToUseAttackMagic + this->Characters[cmd.CharacterID].AbleToUseSingleCureMagic + this->Characters[cmd.CharacterID].AbleToUseRangeCureMagic;
		InputMode Mode = InputMode::MainCommand;
		bool ChooseEndFlag = false;
		SkillA RetData = NormalAttack;
		auto ChangeMode = [&Mode, &cursor, &CursorPoint](const int NewChooseTotalInputedEnter, const int NewChooseTotalInputedCancel,
			const InputMode NewModeInputedEnter, const InputMode NewModeInputedCancel) {
			Mode = GetNewMode(Mode, cursor, CursorPoint, NewChooseTotalInputedEnter, NewChooseTotalInputedCancel, NewModeInputedEnter, NewModeInputedCancel);
		};
		while (this->CommonLoopCondition(cursor) && !ChooseEndFlag) {
			if (!EngageAttack && CursorColor.GetColorCode() != Color::Green.GetColorCode()) CursorColor = Color::Green;
			if (EngageAttack && CursorColor.GetColorCode() != Color::Cyan.GetColorCode()) CursorColor = Color::Cyan;
			this->GraphScreenOnChoosePlayerCommand(cmd, Mode, CursorPoint, AllMagic);
			switch (Mode) {
				case InputMode::Back:
					RetData.Name = "back";
					ChooseEndFlag = true;
					break;
				case InputMode::MainCommand:
					cursor.Update();
					cursor.MoveCursor();
					if (cursor.ReloadKey()) {
						this->ReloadMemberInfo({ this->Characters[0], this->Characters[1], this->Characters[2] }, this->Characters[3]);
						cursor.ReloadKeyInfo();
						cmd.UpdateMemberInfo();
					}
					ControlModeOnMainCommand(AllMagic.size(), CursorPoint, ChangeMode);
					break;
				case InputMode::NormalAttack:
					RetData = NormalAttack;
					Mode = InputMode::ChooseEnd;
					break;
				case InputMode::ChooseMagic:
					cursor.Update();
					cursor.MoveCursor();
					RetData = AllMagic[CursorPoint];
					ChangeMode(3, 3, InputMode::ChooseCureTarget, InputMode::MainCommand);
					if (Mode == InputMode::ChooseCureTarget && RetData.SkillType != 1) Mode = InputMode::ChooseEnd;
					break;
				case InputMode::ChooseCureTarget:
					cursor.Update();
					cursor.MoveCursor();
					ChangeMode(3, static_cast<int>(AllMagic.size()), InputMode::ChooseEnd, InputMode::ChooseMagic);
					break;
				case InputMode::Guard:
					this->Characters[cmd.CharacterID].Guard = true;
					RetData = Guard;
					Mode = InputMode::ChooseEnd;
					if (InputMode::ChooseEnd == Mode && (
						(!this->Characters[CursorPoint].Dead() && RetData.ResurrectionEffect)
						|| (this->Characters[CursorPoint].Dead() && !RetData.ResurrectionEffect)
						)
						) Mode = InputMode::ChooseCureTarget;
					break;
				case InputMode::ChooseEnd:
					ChooseEndFlag = true;
					break;
				default:
					throw KgWinException("Player input error");
			}
			if (cursor.ChangeParameterGraphMode()) GraphNumMode = !GraphNumMode;
			this->ControlEngage(EngageAttack, cmd, cursor);
			this->ControlParameter(cursor);
		}
		return RetData;
	}

	void BaseBattle::InputCommand(BattleCommonInformation &bcm, PlayerCommand& cmd, PartnerCommandManager& FirstPartner, PartnerCommandManager& SecondPartner) {
		int SwitchControl = 0;
		bool ChooseEndFlag = false;
		while (this->CommonLoopCondition(bcm.PlayerCursor) && !ChooseEndFlag) {
			switch (SwitchControl) {
				case 0:
					if (!this->Characters[0].HP.IsMin()) {
						bcm.Commands[0] = this->ChoosePlayerCommand(bcm.PlayerCursor, cmd, bcm.CursorPoints[0], bcm.EngageAttack, bcm.CursorColor);
						if (bcm.Commands[0].Name != "back") SwitchControl = 1;
					}
					else SwitchControl = 1;
					break;
				case 1:
					if (!this->Characters[1].HP.IsMin() && !FirstPartner.AIControl) {
						bcm.Commands[1] = FirstPartner.PlayerControl([this, &bcm](PlayerCommand& cmd) {
							bool temp = false;
							return this->ChoosePlayerCommand(bcm.FirstPartnerCursor, cmd, bcm.CursorPoints[1], temp, bcm.CursorColor);
						});
						if (bcm.Commands[1].Name == "back") SwitchControl = 0;
						else {
							bcm.Commands[1] = FirstPartner.AICommand();
							if (bcm.Commands[1] == Guard) this->Characters[1].Guard = true;
							SwitchControl = 2;
						}
					}
					else SwitchControl = 2;
					break;
				case 2:
					if (!this->Characters[2].HP.IsMin() && !SecondPartner.AIControl) {
						bcm.Commands[2] = SecondPartner.PlayerControl([this, &bcm](PlayerCommand& cmd) {
							bool temp = false;
							return this->ChoosePlayerCommand(bcm.SecondPartnerCursor, cmd, bcm.CursorPoints[2], temp, bcm.CursorColor);
						});
						if (bcm.Commands[2].Name == "back") {
							if (this->Characters[1].Dead() || FirstPartner.AIControl) SwitchControl = 0;
							else SwitchControl = 1;
						}
						else {
							bcm.Commands[2] = FirstPartner.AICommand();
							if (bcm.Commands[2] == Guard) this->Characters[2].Guard = true;
							SwitchControl = 3;
						}
					}
					else SwitchControl = 3;
					break;
				case 3:
					ChooseEndFlag = true;
					break;
				default:
					throw KgWinException("SwitchControl is not out of range.");
			}
		}
	}

}
