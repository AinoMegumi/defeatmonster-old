﻿#ifndef __TITLEINFO_HPP__
#define __TITLEINFO_HPP__
constexpr int WindowWidth = 1280;
constexpr int WindowHeight = 720;
constexpr const char* Title = "モンスター討伐ゲーム　アンチ核家族化";
#endif
