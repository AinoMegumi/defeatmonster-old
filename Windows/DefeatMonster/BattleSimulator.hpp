﻿#ifndef __BATTLESIMULATOR_HPP__
#define __BATTLESIMULATOR_HPP__
#include "Battle.hpp"
#ifdef _DEBUG

class BattleSimulator : public BaseBattleCreateImpl {
private:
	using EnemyAI = Offline::EnemyAI;
	using PlayerCommand = Offline::PlayerCommand;
	using BattleCommonInformation = Offline::BattleCommonInformation;
	using PartnerCommandManager = Offline::PartnerCommandManager;
protected:
	standard::ArrayRef<Status, 4> Characters;
private:
	MenuWindow GaugeModeParameterGraphField;
	MenuWindow NumModeParameterGraphField;
	EnemyAI Enemy;
	std::array<int, 4> TurnControlArray;
	// 共通ループ条件

protected:
	bool CommonLoopCondition(const bool ApplyDamageInfo = true) const;

	// 共通描画系
private:
	void GraphNumGraphField(const int X, const int Y) const;
	void GraphParameterWithNumMode(const int X, const int Y) const;
	void GraphEnemyCharacterParameter() const;
	void CommonGraphic(const KgDxGraphic::NoExcept::Picture& EnemyGraphicInfo) const;
	void CommonGraphicDuringExecutingCommand(const std::string& AttackerName, const std::string& SkillName, const KgDxGraphic::NoExcept::Picture& EnemyGraphicInfo, const bool EngageAttack, const bool IgnorePlayerInstructionFlag) const;
	void GraphEnemy(const KgDxGraphic::NoExcept::Picture& EnemyGraphicInfo) const;
protected:
	void GraphPlayerSideCharacterParameter() const;
private:
	void GraphMessageScreenAfterExecuteAttackCommandToEnemy(const size_t Attacker, const SkillA& AttackInfo, const KgDxGraphic::NoExcept::Picture& EnemyGraphicInfo, const bool EngageAttack = false) const;
	void GraphMessageScreenAfterExecuteSingleCureCommand(const size_t Healer, const SkillA& CureInfo, const size_t Target, const KgDxGraphic::NoExcept::Picture& EnemyGraphicInfo,
		const bool EngageAttack = false, const bool IgnorePlayerInstructionFlag = false) const;
	void GraphMessageScreenAfterExecuteResurrection(const size_t Healer, const size_t Target, const SkillA& CureInfo, const KgDxGraphic::NoExcept::Picture& EnemyGraphicInfo) const;
	void GraphMessageScreenAfterExecuteRangeCureCommand(const size_t Healer, const SkillA& CureInfo, const KgDxGraphic::NoExcept::Picture& EnemyGraphicInfo, const bool EngageAttack = false, const bool IgnorePlayerInstructionFlag = false) const;
	void GraphMessageScreenExecuteGuardCommand(const std::string& ExecuteCharacterName, const KgDxGraphic::NoExcept::Picture& EnemyGraphicInfo) const;
	void GraphMessageScreenAfterExecuteAttackCommandFromEnemy(const size_t Crasher, const SkillA& AttackInfo, const KgDxGraphic::NoExcept::Picture& EnemyGraphicInfo) const;
	void GraphMessageScreenAfterExecuteRangeAttackCommandFromEnemy(const size_t Crasher, const SkillA& AttackInfo, const KgDxGraphic::NoExcept::Picture& EnemyGraphicInfo) const;
	void GraphAttackMessage(const std::string& AttackerName, const std::string& SkillName, const bool EngageAttack, const bool IgnorePlayerInstructionFlag) const;
	void ApplyDamageToGraph(AbleToChangeMaxCursor& c) const;
	void ApplyDamageToGraph() const;

	// 初期化系

	void CreateTurnControlArray();
	void ReloadMemberInfo(const std::initializer_list<Status>& PlayerSideCharacterList, const Status& EnemyMonster);

	// コマンド入力・実行系作成ベース関数群

	int ExecuteAttackCommandToEnemy(const size_t Attacker, bool &CriticalFlag) const;
	int ExecuteAttackCommandToEnemy(const size_t Attacker, const SkillA& AttackInfo) const;
	int ExecuteCureCommand(const size_t Healer, const size_t Target, const SkillA& CureMagicInfo) const;
	int ExecuteCureCommand(const size_t Healer, const SkillA& CureMagicInfo) const;
	std::array<int, 3> ExecuteCureCommand(const size_t Healer, const SkillA& CureMagicInfo, const int NoCureCharacterInputNum) const;
	bool ExecuteResurrection(const size_t Healer, const size_t Target, const SkillA& CureMagicInfo) const;
	int ExecuteAttackCommandFromEnemy(const size_t Crasher, bool &CriticalFlag) const;
	int ExecuteAttackCommandFromEnemy(const size_t Crasher, const SkillA& AttackMagicInfo) const;
	int ExecuteAttackCommandFromEnemy(const SkillA& AttackMagicInfo) const;
	std::array<int, 3> ExecuteAttackCommandFromEnemy(const SkillA& AttackMagicInfo, const int DeadCharacterInputNum) const;

	// コマンド入力・実行系

	void ExecutePlayerCommand(const int CursorPoint, bool& EngageAttack, const SkillA& AttackInfo, const KgDxGraphic::NoExcept::Picture& EnemyGraphicInfo) const;
	void ExecutePartnerCommand(const SkillA& AttackInfo, const size_t AttackerID, const size_t LastCursorPoint, const KgDxGraphic::NoExcept::Picture& EnemyGraphicInfo, const bool IgnorePlayerInstructionFlag) const;
	void ExecuteEnemySideCommand(const KgDxGraphic::NoExcept::Picture& EnemyGraphicInfo);

	// 判定系

	bool Win() const;
	bool Lose() const;

	// 検索系

	size_t GetEngagePartnerID() const noexcept;
	size_t GetAliveFrontCharacter() const noexcept;

	// メイン関数作成用
	void ChangeTurnControl(BattleCommonInformation& bcm, std::array<bool, 3> &AllowAttack, const KgDxGraphic::Color& CursorDefaultColor);
	void InitControl(std::array<bool, 3> &AllowAttack, bool &AlreadyDoubleAttack);
protected:
	int BattleSection(BattleCommonInformation& bcm, const KgDxGraphic::Color CursorDefaultColor, PartnerCommandManager& Player, PartnerCommandManager& FirstPartner, PartnerCommandManager& SecondPartner, const KgDxGraphic::NoExcept::Picture& EnemyGraphicInfo);
	std::string CommandLogFilePath;
	void OutputCommandLog(const std::string& Attacker, const std::string& AttackName, const std::string& Target, const int Damage) const;
	void OutputCommandLog(const std::string& Attacker, const std::string& AttackName, const int Average) const;
public:
	BattleSimulator(std::array<Status, 4>& Characters, KgDxGraphic::NoExcept::Picture& BackgroundGraph, KgDxGraphic::ThrowException::String& StringHandle, DllFunctionCallManager& DllFuncManager, int& MasterVolume);
	int main();
};
#endif
#endif
