﻿#ifndef __AIPARAMETER_H__
#define __AIPARAMETER_H__
typedef struct {
	int HP;
	int MaxHP;
	int MP;
	int MaxMP;
	int Attack;
	int Defence;
	int MagicAttack;
	int MagicDefence;
	int MagicCure;
	int Speed;
	int Cleverness;
} AIParameter;

typedef struct {
	AIParameter* Parameters;
	unsigned int CharacterNum;
} AIParameterList;
#endif
