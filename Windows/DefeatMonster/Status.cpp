﻿#include "Program.hpp"
#include "Status.hpp"
#include "PropertiesRead.hpp"
#include "Split.hpp"
#include "KgWinException.hpp"
#include "Random.hpp"
#include "XmlRead.hpp"
#include "IsPlayerJudgement.hpp"
#include "PropertiesWrite.hpp"
#include <array>
#include <cmath>
#include <cstdint>
constexpr double CriticalRate = 0.1;

static std::vector<std::array<int, 9>> CreateLevelUpAddParameterList(const std::string Tag) {
	std::vector<std::array<int, 9>> Arr;
	PropertiesRead Prop(Program::ChangeFullPath(".\\Level\\" + Tag + ".properties"));
	for (auto& i : Prop.GetData()) {
		if (!Program::LoopCondition()) break;
		Arr.emplace_back([](const std::string Data) {
			const std::vector<std::string> tempbuf = SplitString(Data, '/');
			if (tempbuf.size() != 9) throw KgWinException("LevelUp add parameter list is wrong.");
			std::array<int, 9> data;
			for (size_t i = 0; i < 9; i++) data[i] = std::stoi(tempbuf[i]);
			return data;
		}(i.Data));
	}
	return Arr;
}

static inline StatusRawData CreateAddRawData(const LevelManager& Level, const std::string& Tag) {
	const std::vector<std::array<int, 9>> LevelUpAddParameterList = CreateLevelUpAddParameterList(Tag);
	StatusRawData temp{};
	for (size_t i = 2; i <= Level.GetCurrentLevel() && Program::LoopCondition(); i++) {
		temp.HP += LevelUpAddParameterList[i - 2][0];
		temp.MP += LevelUpAddParameterList[i - 2][1];
		temp.Attack += LevelUpAddParameterList[i - 2][2];
		temp.Defence += LevelUpAddParameterList[i - 2][3];
		temp.MagicAttack += LevelUpAddParameterList[i - 2][4];
		temp.MagicDefence += LevelUpAddParameterList[i - 2][5];
		temp.MagicCure += LevelUpAddParameterList[i - 2][6];
		temp.Speed += LevelUpAddParameterList[i - 2][7];
		temp.Cleverness += LevelUpAddParameterList[i - 2][8];
	}
	temp.Tag = Tag;
	return temp;
}

static inline void SexPlayerStatusChange(int &sta, const int SexID, const std::array<int, 4> ParameterPatternChangeList) {
	sta = sta * ParameterPatternChangeList.at(SexID) / 10;
}

static inline void SexPlayerStatusChange(StatusRawData& sta) {
	auto change = [&sta](int& ChangeParameter, const std::array<int, 4> ParameterPatternChangeList) {
		SexPlayerStatusChange(ChangeParameter, sta.SexID, ParameterPatternChangeList);
	};
	change(sta.HP, { 15, 9, 11, 10 });
	change(sta.MP, { 9, 15, 10, 11 });
	change(sta.Attack, { 12, 8, 11, 10 });
	change(sta.Defence, { 11, 9, 10, 10 });
	change(sta.MagicAttack, { 8, 12, 10, 11 });
	change(sta.MagicDefence, { 9, 11, 10, 10 });
}

Status::Status() : Status(StatusRawData(), LevelManager()) {}

Status::Status(const StatusRawData& Raw, LevelManager&& Level)
	: Name(Raw.Name), SexID(Raw.SexID), IdentifiedTag(Raw.Tag), ExternalAIDllPath(Raw.ExternalAIDllPath),
	Parameter({ Raw.HP, { 0, Raw.HP } }, { Raw.MP, { 0, Raw.MP } }, { Raw.Attack, Raw.Defence }, { Raw.MagicAttack, Raw.MagicDefence }, Raw.MagicCure,
		{ Raw.Speed, Raw.Speed * 3, std::max(Raw.Speed - 200, 0) }, 
		{ Raw.Cleverness, Raw.Cleverness * 3, std::max(Raw.Cleverness - 200, 0) }, 
		std::make_pair(Element::ElementCast(Raw.Element1), Element::ElementCast(Raw.Element2))),
	Level(std::move(Level)), AttackMagic(), SingleCureMagic(), RangeCureMagic(), IsEngageCharacter(false), Guard(false), ExpManager() {

}

Status::Status(const StatusRawData& Raw) : Status(Raw, LevelManager()) {}

Status Status::CreatePlayerStatus(const StatusRawData& Raw) {
	LevelManager Level(Raw.LevelUpBorderPointList, Raw.CurrentExp);
	const StatusRawData LevelAddPoint = CreateAddRawData(Level, Raw.Tag);
	StatusRawData BaseData = Raw;
	SexPlayerStatusChange(BaseData);
	auto i = BaseData + LevelAddPoint;
	Status s(i, std::move(Level));
	s.CreateMagicList(Raw.MP + LevelAddPoint.MP);
	s.RangeCureMagic.emplace_back([]() { // エクステンドケアルリングはプレイヤー限定回復魔法であるためここで追加
		SkillA skill{};
		skill.Name = "エクステンドケアルリング";
		skill.Tag = "rcmextend";
		skill.UseMP = 35;
		skill.BasePower = 210;
		skill.Description = Program::PrintInKanji
			? "仲間全員を回復。死んだ仲間も蘇生させる"
			: "なかまぜんいんをかいふく。しんだなかまもいきかえらせる";
		skill.SkillType = 2;
		skill.Range = true;
		skill.ResurrectionEffect = true;
		skill.SkillElement = ElementInfo::Shine;
		skill.EffectGraphPath = Program::ChangeFullPath(".\\Graphic\\Effect\\cure.png");
		skill.EffectSoundPath = Program::ChangeFullPath(".\\Sound\\Effect\\cure.mp3");
		skill.AddBlendLevel = -1;
		return skill;
	}());
	return s;
}

Status Status::CreatePartnerStatus(const StatusRawData& Raw) {
	LevelManager Level(Raw.LevelUpBorderPointList, Raw.CurrentExp);
	const StatusRawData LevelAddPoint = CreateAddRawData(Level, Raw.Tag);
	Status s(Raw + LevelAddPoint, std::move(Level));
	s.CreateMagicList(Raw.MP);
	return s;
}

Status Status::CreateEnemyStatus(const StatusRawData& Raw) {
	auto ChangeEnemyParamByGameMode = [](const int param) {
#ifdef _DEBUG // デバッグモードではユーザーモードと同様の環境をゲームマスターモードで作成する
		return param;
#else // ゲームマスターモードではパラメーターを1.5倍にする
		return param * (Program::GameMasterMode ? 3 : 2) / 2;
#endif
	};
	StatusRawData data = Raw;
	data.HP = ChangeEnemyParamByGameMode(Raw.HP);
	data.MP = ChangeEnemyParamByGameMode(Raw.MP);
	data.Attack = ChangeEnemyParamByGameMode(Raw.Attack);
	data.Defence = ChangeEnemyParamByGameMode(Raw.Defence);
	data.MagicAttack = ChangeEnemyParamByGameMode(Raw.MagicAttack);
	data.MagicDefence = ChangeEnemyParamByGameMode(Raw.MagicDefence);
	data.MagicCure = ChangeEnemyParamByGameMode(Raw.MagicCure);
	Status s(data);
	s.CreateMagicList(Raw.MP);
	s.ClearRangeCureMagic();
	s.RemoveReservedMagic("パーフェクトケアル");
	s.RemoveReservedMagic("リザレクション");
	return s;
}

std::vector<SkillA> Status::CreateAbleToUseMagicList(const std::vector<SkillA> BaseMagicList) {
	std::vector<SkillA> Arr;
	for (auto& i : BaseMagicList) {
		if (!Program::LoopCondition()) break;
		if (this->MP.RealParameter >= i.UseMP) Arr.emplace_back(i);
	}
	return Arr;
}

void Status::CreateMagicList(const int MPParam) {
	if (MPParam != 0) {
		const auto MagicList = LoadAllMagic();
		for (auto& i : std::get<0>(MagicList)) {
			if (!Program::LoopCondition()) break;
			if (i.LessMP <= MPParam) this->AttackMagic.emplace_back(i);
		}
		if (this->IdentifiedTag == "crerick") {
			this->SingleCureMagic = std::get<1>(MagicList);
			this->RangeCureMagic = std::get<2>(MagicList);
		}
		else {
			for (auto& i : std::get<1>(MagicList)) {
				if (!Program::LoopCondition()) break;
				if (i.LessMP <= MPParam) this->SingleCureMagic.emplace_back(i);
			}
			for (auto& i : std::get<2>(MagicList)) {
				if (!Program::LoopCondition()) break;
				if (i.LessMP <= MPParam) this->RangeCureMagic.emplace_back(i);
			}
		}
	}
}

void Status::CreateAbleToUseMagicList() {
	if (!this->AttackMagic.empty()) this->AbleToUseAttackMagic.clear();
	this->AbleToUseAttackMagic = this->CreateAbleToUseMagicList(this->AttackMagic);
	if (!this->SingleCureMagic.empty()) this->AbleToUseSingleCureMagic.clear();
	this->AbleToUseSingleCureMagic = this->CreateAbleToUseMagicList(this->SingleCureMagic);
	if (!this->RangeCureMagic.empty()) this->AbleToUseRangeCureMagic.clear();
	this->AbleToUseRangeCureMagic = this->CreateAbleToUseMagicList(this->RangeCureMagic);
}

static inline void CorrectMinusDamage(int &DamagePoint, const bool GuardFlag) {
	if (DamagePoint <= 0) {
		if (GuardFlag) DamagePoint = 0;
		std::bernoulli_distribution DamageJudge(0.5);
		DamagePoint = DamageJudge(Random::GetEngine()) ? 1 : 0;
	}
}

int Status::Crash(const Status& Attacker, bool& CriticalFlag) {
	CriticalFlag = false;
	int DamagePoint = (Attacker.Physical.AttackParam.Get() - (this->Physical.DefenceParam.Get() / 2)) / 2;
	if (DamagePoint > 0) {
		std::uniform_int_distribution<int> DamageRand(-DamagePoint / 16, DamagePoint / 16);
		DamagePoint += DamageRand(Random::GetEngine());
		std::bernoulli_distribution Critical(CriticalRate);
		if (CriticalFlag = Critical(Random::GetEngine()); CriticalFlag) {
			if (DamagePoint > 0) DamagePoint = static_cast<int>(static_cast<float>(DamagePoint) * 1.3 + 0.5); // 小数点以下四捨五入
			else DamagePoint = 2;
		}
		if (this->Guard) DamagePoint /= 2;

	}
	CorrectMinusDamage(DamagePoint, this->Guard);
	this->HP -= DamagePoint;
	if (this->Dead()) this->ExpManager.AddDeadCount();
	return DamagePoint;
}

int Status::Crash(const SkillA& AttackSkill, const Status& Attacker) {
	const auto CrashAdvantage = this->Damage.GetCrashAdvantage(AttackSkill.SkillElement);
	int DamagePoint = static_cast<int>(
		(static_cast<double>(Attacker.Magic.AttackParam.Get() + AttackSkill.BasePower) * Attacker.Damage.GetAttackAdvantage(AttackSkill.SkillElement)) * CrashAdvantage
		- (static_cast<double>(this->Magic.DefenceParam.Get()) / 2)
		) / 2;
	if (DamagePoint > 0) {
		std::uniform_int_distribution<int> DamageRand(-DamagePoint / 16, DamagePoint / 16);
		DamagePoint += DamageRand(Random::GetEngine());
		if (this->Guard) DamagePoint /= 2;
	}
	CorrectMinusDamage(DamagePoint, this->Guard);
	this->HP -= DamagePoint;
	if (this->Dead()) this->ExpManager.AddDeadCount();
	switch (static_cast<int>(CrashAdvantage * 100)) {
		case 400: // 4倍
			this->ExpManager.AddElementalAdvantageAttackCount();
			this->ExpManager.AddElementalAdvantageAttackCount();
			break;
		case 200: // 2倍
			this->ExpManager.AddElementalAdvantageAttackCount();
			break;
		case 50:  // 0.5倍
			this->ExpManager.AddElementalInferiorityAttackCount();
			break;
		case 25:  // 0.25倍
			this->ExpManager.AddElementalInferiorityAttackCount();
			this->ExpManager.AddElementalInferiorityAttackCount();
			break;
		default:
			break;
	}
	return DamagePoint;
}

int Status::Cure(const Status Healer, const SkillA CureMagic) {
	std::uniform_int_distribution<int> rand(0, Healer.MagicCure.Get() / 4);
	const int CurePoint = CureMagic.BasePower + rand(Random::GetEngine());
	const auto BeforeCureHP = this->HP.RealParameter;
	this->HP += CurePoint;
	return (this->HP.RealParameter - BeforeCureHP).Get();
}

bool Status::Resurrection(const Status Healer) {
	std::bernoulli_distribution successjudge(0.5);
	const bool Result = successjudge(Random::GetEngine());
	if (Result) {
		std::uniform_int_distribution<int> rand(0, Healer.MagicCure.Get() / 20);
		this->HP += Healer.MagicCure.Get() + rand(Random::GetEngine());
	}
	return Result;
}

bool Status::Dead() const noexcept {
	return this->HP.RealParameter.IsMin();
}

void Status::CureMP() {
	this->MP += std::max(static_cast<int>(static_cast<float>(this->MP.RealParameter.GetMax() * 0.05f)), 1);
}

void Status::AddSpecialAttackMagic(const SkillA MagicInfo) {
	this->AttackMagic.emplace_back(MagicInfo);
}

void Status::AddSpecialSingleCureMagic(const SkillA MagicInfo) {
	this->SingleCureMagic.emplace_back(MagicInfo);
}

void Status::AddSpecialRangeCureMagic(const SkillA MagicInfo) {
	this->RangeCureMagic.emplace_back(MagicInfo);
}

void Status::ClearRangeCureMagic() {
	this->RangeCureMagic.clear();
}

static constexpr int GetEngageUseMP(const PossibleChangeStatus<int> PartnerMP) {
	return std::min(PartnerMP.GetMax() / 20, PartnerMP.Get());
}

constexpr int DivRound(const int l, const int r) noexcept {
	constexpr int table[] = { 0, 1 };
	return l / r + table[(l % r) < 50];
}

void Status::ApplyEngageToAttack(BodyParameter<int> &PartnerMP) {
	const auto UseMP = std::int16_t(GetEngageUseMP(PartnerMP.RealParameter));
	this->Physical.AttackParam += DivRound(this->Physical.AttackParam.Get() * UseMP, 100);
	PartnerMP -= UseMP;
}

void Status::ApplyEngageToMagicAttack(BodyParameter<int> &PartnerMP) {
	const int UseMP = GetEngageUseMP(PartnerMP.RealParameter);
	this->Magic.AttackParam += DivRound(this->Magic.AttackParam.Get() * UseMP, 100);
	PartnerMP -= UseMP;
}

void Status::ApplyEngageToMagicCure(BodyParameter<int> &PartnerMP) {
	const int UseMP = GetEngageUseMP(PartnerMP.RealParameter);
	this->MagicCure += DivRound(this->MagicCure.Get() * UseMP, 100);
	PartnerMP -= UseMP;
}

void Status::CleanEngage() {
	this->Physical.AttackReset();
	this->Magic.AttackReset();
	this->MagicCure.Reset();
}

bool EraseReservedMagic(std::vector<SkillA> &MagicList, const std::string Name) {
	if (MagicList.empty()) return false;
	for (size_t i = 0; i < MagicList.size() && Program::LoopCondition(); i++) {
		if (MagicList[i].Name == Name) {
			MagicList.erase(MagicList.begin() + i);
			return true;
		}
	}
	return false;
}

void Status::RemoveReservedMagic(const std::string MagicName) {
	if (EraseReservedMagic(this->AttackMagic, MagicName)) return;
	if (EraseReservedMagic(this->SingleCureMagic, MagicName)) return;
	EraseReservedMagic(this->RangeCureMagic, MagicName);
}

size_t Status::CalcExp(const size_t BaseExp) {
	const size_t GetExperience = this->ExpManager.CalcExperience(BaseExp, this->HP);
	const size_t CurrentLevel = this->Level.GetCurrentLevel();
	const size_t UpLevel = this->Level.AddExp(GetExperience);
	if (CurrentLevel < UpLevel) {
		const std::vector<std::array<int, 9>> LevelUpAddParameterList = CreateLevelUpAddParameterList(this->IdentifiedTag);
		for (size_t i = CurrentLevel + 1; i <= UpLevel && Program::LoopCondition(); i++) {
			this->HP = BodyParameterManager<int>(
				{ this->HP.RealParameter.Get(), this->HP.RealParameter.GetMax() + LevelUpAddParameterList[i - 2][0] },
				{ 0, this->HP.RealParameter.GetMax() + LevelUpAddParameterList[i - 2][0] }
			);
			this->MP = BodyParameterManager<int>(
				{ this->MP.RealParameter.Get(), this->MP.RealParameter.GetMax() + LevelUpAddParameterList[i - 2][0] },
				{ 0, this->MP.RealParameter.GetMax() + LevelUpAddParameterList[i - 2][0] }
			);
			//this->HP.GraphParameter = { this->HP.GraphParameter.Get(), this->HP.GraphParameter.GetMax() + LevelUpAddParameterList[i - 2][0] };
			//this->MPRealParam = { this->MP.RealParameter.Get(), this->MP.RealParameter.GetMax() + LevelUpAddParameterList[i - 2][1] };
			//this->MP.GraphParameter = { this->HP.GraphParameter.Get(), this->MP.GraphParameter.GetMax() + LevelUpAddParameterList[i - 2][1] };
			this->Physical = UseDamageCalculationStatus<int>(
				this->Physical.AttackParam.Get() + LevelUpAddParameterList[i - 2][2],
				this->Physical.DefenceParam.Get() +LevelUpAddParameterList[i - 2][3]
				);
			this->Magic = UseDamageCalculationStatus<int>(
				this->Magic.AttackParam.Get() + LevelUpAddParameterList[i - 2][4],
				this->Magic.DefenceParam.Get() + LevelUpAddParameterList[i - 2][5]
				);
			this->MagicCure = UseDamageCalculationParameter<int>(
				this->MagicCure.Get() + LevelUpAddParameterList[i - 2][6],
				(this->MagicCure.Get() + LevelUpAddParameterList[i - 2][6]) * 3,
				std::max(this->MagicCure.Get() - 200, 0)
				);
			this->Speed = SpeedManager<int>(
				this->Speed.Get() + LevelUpAddParameterList[i - 2][7],
				(this->Speed.Get() + LevelUpAddParameterList[i - 2][7]) * 3,
				std::max(this->Speed.Get() + LevelUpAddParameterList[i - 2][7] - 200, 0)
				);
			this->Cleverness = UseDamageCalculationParameter<int>(
				this->Cleverness.Get() + LevelUpAddParameterList[i - 2][8],
				(this->Cleverness.Get() + LevelUpAddParameterList[i - 2][8]) * 3,
				std::max(this->Cleverness.Get() + LevelUpAddParameterList[i - 2][8] - 200, 0)
				);
		}
	}
	this->ExpManager.Reset();
	if (IsPlayer(this->IdentifiedTag)) {
		this->AttackMagic.clear();
		this->SingleCureMagic.clear();
		this->RangeCureMagic.clear();
		this->CreateMagicList(this->MP.RealParameter.GetMax());
	}
	PropertiesWrite Prop(Program::SaveDataFilePath);
	Prop.AddData(IsPlayer(this->IdentifiedTag) ? "player" : this->IdentifiedTag, std::to_string(this->Level.GetCurrentExp()));
	Prop.Write();
	return GetExperience;
}
