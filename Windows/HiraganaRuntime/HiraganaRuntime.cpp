// HiraganaRuntime.cpp : DLL アプリケーション用にエクスポートされる関数を定義します。
//

#include "stdafx.h"
#include <stdio.h>

__declspec(dllexport) void WINAPI IntroductEnemy(char dest[1024], const char* Name) {
	sprintf_s(dest, 1024, "%sがあらわれた！", Name);
}

__declspec(dllexport) void WINAPI Attack(char dest[1024], const char* Name) {
	sprintf_s(dest, 1024, "%sのこうげき！", Name);
}

__declspec(dllexport) void WINAPI EngageAttack(char dest[1024], const char* Name) {
	sprintf_s(dest, 1024, "%sのエンゲージアタック！", Name);
}

__declspec(dllexport) void WINAPI CastSpell(char dest[1024], const char* Name, const char* SkillName, const bool IgnorePlayerInstructionFlag) {
	IgnorePlayerInstructionFlag
		? sprintf_s(dest, 1024, "%sはめいれいをむしして%sをとなえた！", Name, SkillName)
		: sprintf_s(dest, 1024, "%sは%sをとなえた！", Name, SkillName);
}

__declspec(dllexport) void WINAPI EngageMagic(char dest[1024], const char* Name, const char* SkillName) {
	sprintf_s(dest, 1024, "%sのエンゲージ・%s！", Name, SkillName);
}

__declspec(dllexport) void WINAPI DamageToEnemy(char dest[1024], const char* CrasherName, const int DamagePoint, const bool Critical) {
	DamagePoint == 0
		? sprintf_s(dest, 1024, "ミス！%sにダメージをあたえられない！", CrasherName)
		: Critical ? sprintf_s(dest, 1024, "%sに%dのクリティカルダメージ！", CrasherName, DamagePoint) : sprintf_s(dest, 1024, "%sに%dのダメージ！", CrasherName, DamagePoint);
}

__declspec(dllexport) void WINAPI Crash(char dest[1024], const char* CrasherName, const int DamagePoint, const bool Critical) {
	DamagePoint == 0
		? sprintf_s(dest, 1024, "ミス！%sはダメージをうけない！", CrasherName)
		: Critical ? sprintf_s(dest, 1024, "%sは%dのクリティカルダメージをうけた！", CrasherName, DamagePoint) : sprintf_s(dest, 1024, "%sは%dのダメージをうけた！", CrasherName, DamagePoint);
}

__declspec(dllexport) void WINAPI RangeCrash(char dest[1024], const char* CrasherName, const int DamagePoint) {
	sprintf_s(dest, 1024, "%sたちはへいきん%dダメージをうけた！", CrasherName, DamagePoint);
}

__declspec(dllexport) void WINAPI Cure(char dest[1024], const char* HealedCharacterName, const int CurePoint) {
	sprintf_s(dest, 1024, "%sは%dかいふくした！", HealedCharacterName, CurePoint);
}

__declspec(dllexport) void WINAPI RangeCure(char dest[1024], const char* HealerName, const int CurePoint) {
	sprintf_s(dest, 1024, "%sたちはへいきん%dかいふくした！", HealerName, CurePoint);
}

__declspec(dllexport) void WINAPI Ressurection(char dest[1024], const char* ResuscitationCharacter, const bool Succeeded) {
	if (Succeeded) sprintf_s(dest, 1024, "なんと、%sがいきかえった！", ResuscitationCharacter);
	else sprintf_s(dest, 1024, "%sはいきかえらなかった！", ResuscitationCharacter);
}

__declspec(dllexport) void WINAPI LowMP(char dest[1024]) {
	strcpy_s(dest, 1024, "しかし、MPがたりなかった！");
}

__declspec(dllexport) void WINAPI GraphUseMP(char dest[1024], const int UseMP) {
	sprintf_s(dest, 1024, "しょうひMP : %--3d", UseMP);
}

__declspec(dllexport) void WINAPI Dead(char dest[1024], const char* DeadCharacterName) {
	sprintf_s(dest, 1024, "%sはしんでしまった！", DeadCharacterName);
}

__declspec(dllexport) void WINAPI Defeat(char dest[1024], const char* EnemyName) {
	sprintf_s(dest, 1024, "%sをたおした！", EnemyName);
}

__declspec(dllexport) void WINAPI Defence(char dest[1024], const char* GuardCharacterName) {
	sprintf_s(dest, 1024, "%sはみをまもっている！", GuardCharacterName);
}

__declspec(dllexport) void WINAPI ResultWin() {
	MessageBoxA(NULL, "キミのかちだよ", "モンスター討伐ゲーム　アンチ核家族化", MB_ICONINFORMATION | MB_OK);
}

__declspec(dllexport) void WINAPI ResultLose() {
	MessageBoxA(NULL, "キミのまけだよ", "モンスター討伐ゲーム　アンチ核家族化", MB_ICONINFORMATION | MB_OK);
}

__declspec(dllexport) void WINAPI ResultDraw() {
	MessageBoxA(NULL, "ひきわけだよ", "モンスター討伐ゲーム　アンチ核家族化", MB_ICONINFORMATION | MB_OK);
}

__declspec(dllexport) void WINAPI InPreparation() {
	MessageBoxA(NULL, "じゅんびちゅうだよ", "モンスター討伐ゲーム　アンチ核家族化", MB_ICONINFORMATION | MB_OK);
}
