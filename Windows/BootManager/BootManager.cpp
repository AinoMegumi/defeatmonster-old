// BootManager.cpp : DLL アプリケーション用にエクスポートされる関数を定義します。
//

#include "stdafx.h"
#include "../DefeatMonster/HandleManager.hpp"
#include "KgWinException.hpp"
#include "RegistryRead.hpp"
#include "ApplicationDirectory.hpp"
#include "EnvironmentVariable.hpp"
#include "Win32API.hpp"
#include <shellapi.h>
#include <direct.h>
#include <AclAPI.h>

void xcopy(const std::string SourceDirectory, const std::string DestDirectory) {
	TCHAR               szAccountName[256];
	PACL                pDacl;
	DWORD               dwSize;
	EXPLICIT_ACCESS     explicitAccess;
	SECURITY_ATTRIBUTES securityAttributes;
	SECURITY_DESCRIPTOR securityDescriptor;
	STARTUPINFOA        startupInfo;
	PROCESS_INFORMATION processInformation;

	dwSize = sizeof(szAccountName) / sizeof(TCHAR);
	GetUserName(szAccountName, &dwSize);
	BuildExplicitAccessWithName(&explicitAccess, szAccountName, PROCESS_ALL_ACCESS, GRANT_ACCESS, 0);
	SetEntriesInAcl(1, &explicitAccess, NULL, &pDacl);

	InitializeSecurityDescriptor(&securityDescriptor, SECURITY_DESCRIPTOR_REVISION);
	SetSecurityDescriptorDacl(&securityDescriptor, TRUE, pDacl, FALSE);
	securityAttributes.nLength = sizeof(SECURITY_ATTRIBUTES);
	securityAttributes.lpSecurityDescriptor = &securityDescriptor;
	securityAttributes.bInheritHandle = FALSE;

	startupInfo.dwFlags = 0;
	GetStartupInfoA(&startupInfo);
	char CommandLine[1024];
	strcpy_s(CommandLine, 1024, (SourceDirectory + " " + DestDirectory + "/e /i").c_str());
	CreateProcessA("C:\\Windows\\System32\\xcopy.exe", CommandLine, &securityAttributes, NULL, FALSE, 0, NULL, NULL, &startupInfo, &processInformation);
	DWORD dwExitCode = STILL_ACTIVE;
	if (processInformation.hProcess) {
		WaitForSingleObject(processInformation.hProcess, INFINITE);
		GetExitCodeProcess(processInformation.hProcess, &dwExitCode);
		
		if (dwExitCode != 0) throw std::runtime_error("Error in xcopy.exe");
	}
	else throw std::runtime_error("Failed to copy directory.");
}

std::string GetInstallPath(const std::string ProgramData) {
	MSXMLRead xml(ProgramData + "\\Kamioda Games\\installinfo.xml", "softwarelist/software");
	xml.Load("productname", "source");
	for (size_t i = 0; i < xml.size(); i++) {
		if (xml[0][i] == "DefeatMonster"&& DirectoryExists(xml[1][i])) return xml[1][i];
	}
	throw std::runtime_error("Install path is not found.");
}

static inline void CreateProcessAndWait(const std::string& ExeFilePath, char CommandLine[1024], const std::string WorkingDirectory) {
	STARTUPINFOA        si{ 0 };
	PROCESS_INFORMATION pi{ 0 };
	ClassMember::HandleManager ProcessHandle(pi.hProcess);
	ClassMember::HandleManager ThreadHandle(pi.hThread);
	if (0 == CreateProcessA(ExeFilePath.c_str(), CommandLine, NULL, NULL, FALSE, 0, NULL, WorkingDirectory.c_str(), &si, &pi))
		throw KgWinException();
	DWORD dwExitCode = STILL_ACTIVE;
	if (pi.hProcess) {
		if (WAIT_FAILED == WaitForSingleObject(pi.hProcess, INFINITE)) throw KgWinException();
		if (FALSE == GetExitCodeProcess(pi.hProcess, &dwExitCode)) throw KgWinException();
		if (dwExitCode != 0) throw std::runtime_error("Error in setting menu");
	}
	else throw std::runtime_error("Failed to show setup window.");
}

__declspec(dllexport) int boot(const char* LanguageRuntimePath, const int FPS) {
	try {
		const std::string ProgramData = EnvironmentVariableA("PROGRAMDATA").Get();
		const std::string InstallPath = GetInstallPath(ProgramData);
		const std::string UserSID = GetSIDStringA();
		const std::string CopyPath = []() {
			try {
				return RegistryRead(HKEY_CURRENT_USER, "SOFTWARE\\Kamioda Games").ReadString("workingpath") + "\\Kamioda Warrior\\DefeatMonster";
			}
			catch (KgWinException) {
				return RegistryRead(HKEY_CURRENT_USER, "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Explorer\\Shell Folders").ReadString("{4C5C32FF-BB9D-43B0-B5B4-2D72E54EAAA4}") + "\\Kamioda Games\\Kamioda Warrior\\DefeatMonster";
			}
		}();
		xcopy(InstallPath, CopyPath); 
		_mkdir((CopyPath + "\\Temp").c_str());
		SetEnvironmentVariableA("DMTEMP", (CopyPath + "\\Temp").c_str());
		char CommandLine[1024];
		strcpy_s(CommandLine, 
			("/savepath " + EnvironmentVariableA("KGSAVEROOT").Get() + "\\Kamioda Warrior\\DefeatMonster"
			+ " /sid " + UserSID + "/lang " + LanguageRuntimePath + " /fps " + std::to_string(FPS)).c_str());
		CreateProcessAndWait((CopyPath + "\\DefeatMonster.exe").c_str(), CommandLine, CopyPath.c_str());
		return 0;
	}
	catch (const KgWinException& kex) { kex.GraphErrorMessageOnMessageBox("DefeatMonster Boot Manager"); }
	catch (const std::exception& er) { KgWinException(er.what()).GraphErrorMessageOnMessageBox("DefeatMonster Boot Manager"); }
	return 1;
}
