// KanjiRuntime.cpp : DLL アプリケーション用にエクスポートされる関数を定義します。
//

#include "stdafx.h"
#include <stdio.h>

__declspec(dllexport) void WINAPI IntroductEnemy(char dest[1024], const char* Name) {
	sprintf_s(dest, 1024, "%sが現れた！", Name);
}

__declspec(dllexport) void WINAPI Attack(char dest[1024], const char* Name) {
	sprintf_s(dest, 1024, "%sの攻撃！", Name);
}

__declspec(dllexport) void WINAPI EngageAttack(char dest[1024], const char* Name) {
	sprintf_s(dest, 1024, "%sのエンゲージアタック！", Name);
}

__declspec(dllexport) void WINAPI CastSpell(char dest[1024], const char* Name, const char* SkillName, const bool IgnorePlayerInstructionFlag) {
	IgnorePlayerInstructionFlag
		? sprintf_s(dest, 1024, "%sは命令を無視して%sを唱えた！", Name, SkillName)
		: sprintf_s(dest, 1024, "%sは%sを唱えた！", Name, SkillName);
}

__declspec(dllexport) void WINAPI EngageMagic(char dest[1024], const char* Name, const char* SkillName) {
	sprintf_s(dest, 1024, "%sのエンゲージ・%s！", Name, SkillName);
}

__declspec(dllexport) void WINAPI DamageToEnemy(char dest[1024], const char* CrasherName, const int DamagePoint, const bool Critical) {
	DamagePoint == 0 
		? sprintf_s(dest, 1024, "ミス！%sにダメージを与えられない！", CrasherName)
		: Critical ? sprintf_s(dest, 1024, "%sに%dのクリティカルダメージ！", CrasherName, DamagePoint) : sprintf_s(dest, 1024, "%sに%dのダメージ！", CrasherName, DamagePoint);
}

__declspec(dllexport) void WINAPI Crash(char dest[1024], const char* CrasherName, const int DamagePoint, const bool Critical) {
	DamagePoint == 0
		? sprintf_s(dest, 1024, "ミス！%sはダメージを受けない！", CrasherName)
		: Critical ? sprintf_s(dest, 1024, "%sは%dのクリティカルダメージを受けた！", CrasherName, DamagePoint) : sprintf_s(dest, 1024, "%sは%dのダメージを受けた！", CrasherName, DamagePoint);
}

__declspec(dllexport) void WINAPI RangeCrash(char dest[1024], const char* CrasherName, const int DamagePoint) {
	sprintf_s(dest, 1024, "%sたちは平均%dのダメージを受けた！", CrasherName, DamagePoint);
}

__declspec(dllexport) void WINAPI Cure(char dest[1024], const char* HealedCharacterName, const int CurePoint) {
	sprintf_s(dest, 1024, "%sは%d回復した！", HealedCharacterName, CurePoint);
}

__declspec(dllexport) void WINAPI RangeCure(char dest[1024], const char* HealerName, const int CurePoint) {
	sprintf_s(dest, 1024, "%sたちは平均%d回復した！", HealerName, CurePoint);
}

__declspec(dllexport) void WINAPI Ressurection(char dest[1024], const char* ResuscitationCharacter, const bool Succeeded) {
	if (Succeeded) sprintf_s(dest, 1024, "なんと、%sが生き返った！", ResuscitationCharacter);
	else sprintf_s(dest, 1024, "%sは生き返らなかった！", ResuscitationCharacter);
}

__declspec(dllexport) void WINAPI LowMP(char dest[1024]) {
	strcpy_s(dest, 1024, "しかし、MPが足りなかった！");
}

__declspec(dllexport) void WINAPI GraphUseMP(char dest[1024], const int UseMP) {
	sprintf_s(dest, 1024, "消費MP : %--3d", UseMP);
}

__declspec(dllexport) void WINAPI Dead(char dest[1024], const char* DeadCharacterName) {
	sprintf_s(dest, 1024, "%sは死んでしまった！", DeadCharacterName);
}

__declspec(dllexport) void WINAPI Defeat(char dest[1024], const char* EnemyName) {
	sprintf_s(dest, 1024, "%sを倒した！", EnemyName);
}

__declspec(dllexport) void WINAPI Defence(char dest[1024], const char* GuardCharacterName) {
	sprintf_s(dest, 1024, "%sは身を守っている！", GuardCharacterName);
}

__declspec(dllexport) void WINAPI ResultWin() {
	MessageBoxA(NULL, "あなたの勝ちです", "モンスター討伐ゲーム　アンチ核家族化", MB_ICONINFORMATION | MB_OK);
}

__declspec(dllexport) void WINAPI ResultLose() {
	MessageBoxA(NULL, "あなたの負けです", "モンスター討伐ゲーム　アンチ核家族化", MB_ICONINFORMATION | MB_OK);
}

__declspec(dllexport) void WINAPI ResultDraw() {
	MessageBoxA(NULL, "引き分けです", "モンスター討伐ゲーム　アンチ核家族化", MB_ICONINFORMATION | MB_OK);
}

__declspec(dllexport) void WINAPI InPreparation() {
	MessageBoxA(NULL, "準備中です", "モンスター討伐ゲーム　アンチ核家族化", MB_ICONINFORMATION | MB_OK);
}

