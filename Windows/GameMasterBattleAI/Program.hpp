#ifndef __PROGRAM_HPP__
#define __PROGRAM_HPP__
#include <string>

namespace Program {
	std::string ChangeFullPath(const std::string FilePath);
}
#endif
