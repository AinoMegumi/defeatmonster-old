﻿/*
	MIT License

	Copyright (c) 2017 シャーリィ＠Windowsゲームプログラマー(学生)

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all
	copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
	SOFTWARE.
*/
#include "MSXMLRead.hpp"
#include <Shlwapi.h>
#include <comdef.h>
#include "KgWinException.hpp"
#pragma comment(lib, "Shlwapi.lib")

namespace Win32LetterConvert {
	// code copy from Storage.cpp in repositry "DefeatMonster.old"(Author:AinoMegumi)
	std::string WStringToString(const std::wstring oWString) {
		const int iBufferSize = WideCharToMultiByte(CP_OEMCP, 0, oWString.c_str(), -1, (char *)NULL, 0, NULL, NULL);
		if (0 == iBufferSize) throw KgWinException();
		char* cpMultiByte = new char[iBufferSize];
		WideCharToMultiByte(CP_OEMCP, 0, oWString.c_str(), -1, cpMultiByte, iBufferSize, NULL, NULL);
		std::string oRet(cpMultiByte, cpMultiByte + iBufferSize - 1);
		delete[] cpMultiByte;
		return oRet;
	}
	std::string WStringToString(const std::string& oWString) {
		return oWString;
	}
	std::wstring StringToWString(const std::string oString) {
		const int iBufferSize = MultiByteToWideChar(CP_OEMCP, 0, oString.c_str(), -1, (wchar_t*)NULL, 0);
		if (0 == iBufferSize) throw KgWinException();
		wchar_t* cpWideChar = new wchar_t[iBufferSize];
		MultiByteToWideChar(CP_OEMCP, 0, oString.c_str(), -1, cpWideChar, iBufferSize);
		std::wstring oRet(cpWideChar, cpWideChar + iBufferSize - 1);
		delete[] cpWideChar;
		return oRet;
	}
	std::wstring StringToWString(const std::wstring oString) {
		return oString;
	}
}

namespace Replace {
	// code copy from make_array.h in repositry xml_text_cooking_quiz(Author:yumetodo)
	std::basic_string<TCHAR> LF(std::basic_string<TCHAR>&& Str) {
		if (_T("empty") == Str) return _T("");
		const std::basic_string<TCHAR> ReplaceTarget = _T("\\n");
		const std::basic_string<TCHAR> AfterReplace = _T("\n");
		for (size_t pos = Str.find(ReplaceTarget); std::string::npos != pos; pos = Str.find(ReplaceTarget, pos + AfterReplace.length())) {
			Str.replace(pos, ReplaceTarget.length(), AfterReplace);
		}
		return Str;
	}
}

Node::Node(const std::basic_string<TCHAR> NodePath, const Microsoft::WRL::ComPtr<IXMLDOMNodeList> List)
	: NodePath(NodePath){
	long Length;
	List->get_length(&Length);
	for (long i = 0; i < Length; i++) {
		IXMLDOMNode* lpItem;
		List->get_item(i, &lpItem);
		BSTR Temp;
		lpItem->get_text(&Temp);
		std::basic_string<TCHAR> Str = Replace::LF(Win32LetterConvert::WStringToString(Temp));
		Str.resize(std::char_traits<TCHAR>::length(Str.c_str()));
		this->Buf.emplace_back(Str);
	}
}

MSXMLRead::MSXMLRead(const std::basic_string<TCHAR> FileName, const std::basic_string<TCHAR> CommonPath) {
	if (FALSE == PathFileExists(FileName.c_str())) throw std::runtime_error(Win32LetterConvert::WStringToString(FileName) + " : file is not found.");
	CoInitialize(nullptr);
	IXMLDOMDocument* XmlDoc;
	const HRESULT ErrorCode = CoCreateInstance(CLSID_DOMDocument, nullptr, CLSCTX_INPROC_SERVER, IID_IXMLDOMDocument, (void**)&XmlDoc);
	if (XmlDoc == nullptr) throw KgWinException("xml open failed.", ErrorCode);
	this->lpXmlDoc = XmlDoc;
	VARIANT_BOOL Result;
	this->lpXmlDoc->put_async(VARIANT_FALSE);
	this->lpXmlDoc->load(_variant_t(FileName.c_str()), &Result);
	if (0 == Result) {
		CoUninitialize();
		throw std::runtime_error("xml read failed");
	}
	this->CommonPath = CommonPath;
}

MSXMLRead::~MSXMLRead() {
	CoUninitialize();
}

IXMLDOMNodeList* MSXMLRead::XmlSetNodeList(const std::basic_string<TCHAR> NodePath) {
	BSTR Path = SysAllocString(Win32LetterConvert::StringToWString(this->CommonPath + NodePath).c_str());
	if (NodePath.empty() || 0 == SysStringLen(Path)) {
		SysFreeString(Path);
		return NULL;
	}
	IXMLDOMNodeList* lpNodeList;
	if (E_INVALIDARG == this->lpXmlDoc->selectNodes(Path, &lpNodeList)) {
		SysFreeString(Path);
		lpNodeList->Release();
		return NULL;
	}
	SysFreeString(Path);
	return lpNodeList;
}

void MSXMLRead::LoadFromFile(const std::basic_string<TCHAR> NodePath) {
	this->Data.emplace_back(this->CommonPath + NodePath, this->XmlSetNodeList(NodePath));
}

void MSXMLRead::ChangeCommonPath(const std::basic_string<TCHAR> NewRoot) {
	this->clear();
	this->CommonPath = NewRoot;
}

bool MSXMLRead::NodeDataExists(const std::basic_string<TCHAR> NodePath) const noexcept {
	const std::basic_string<TCHAR> FullPath = this->CommonPath + NodePath;
	return std::any_of(this->Data.begin(), this->Data.end(), [FullPath](const Node i) { return i.NodePath == FullPath; });
}

Node MSXMLRead::operator [] (const std::basic_string<TCHAR> NodePath) const {
	for (const auto& i : this->Data) if (i.NodePath == this->CommonPath + NodePath) return i;
	throw std::runtime_error(Win32LetterConvert::WStringToString(this->CommonPath + NodePath) + " : not found such node.");
}
