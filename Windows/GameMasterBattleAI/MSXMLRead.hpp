﻿/*
	MIT License

	Copyright (c) 2017 シャーリィ＠Windowsゲームプログラマー(学生)

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in all
	copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
	SOFTWARE.
*/
#ifndef __MSXMLREAD_HPP__
#define __MSXMLREAD_HPP__
#include <msxml.h>
#include <tchar.h>
#include <wrl/client.h>
#include <string>
#include <vector>
#include <utility>
#include <algorithm>
#include <type_traits>

class Node {
private:
	std::vector<std::basic_string<TCHAR>> Buf;
	typedef std::vector<std::basic_string<TCHAR>>::iterator iterator;
	typedef std::vector<std::basic_string<TCHAR>>::const_iterator const_iterator;
	typedef std::vector<std::basic_string<TCHAR>>::reverse_iterator reverse_iterator;
	typedef std::vector<std::basic_string<TCHAR>>::const_reverse_iterator const_reverse_iterator;
public:
	Node() = default;
	Node(const std::basic_string<TCHAR> NodePath, const Microsoft::WRL::ComPtr<IXMLDOMNodeList> List);
	std::basic_string<TCHAR> Get(const size_t Count) const { return this->Buf.at(Count); }
	template<typename T, std::enable_if_t<std::is_integral<T>::value && std::is_signed<T>::value, std::nullptr_t> = nullptr>
	T Get(const size_t Count) const { return static_cast<T>(std::stoll(this->Get(Count))); }
	template<typename T, std::enable_if_t<std::is_integral<T>::value && std::is_unsigned<T>::value, std::nullptr_t> = nullptr>
	T Get(const size_t Count) const { return static_cast<T>(std::stoull(this->Get(Count))); }
	template<typename T, std::enable_if_t<std::is_floating_point<T>::value, std::nullptr_t> = nullptr>
	T Get(const size_t Count) const { return static_cast<T>(std::stod(this->Get(Count))); }
	size_t size() const noexcept { return this->Buf.size(); }
	iterator begin() noexcept { return this->Buf.begin(); }
	const_iterator begin() const noexcept { return this->Buf.begin(); }
	iterator end() noexcept { return this->Buf.end(); }
	const_iterator end() const noexcept { return this->Buf.end(); }
	const_iterator cbegin() const noexcept { return this->Buf.cbegin(); }
	const_iterator cend() const noexcept { return this->Buf.cend(); }
	reverse_iterator rbegin() noexcept { return this->Buf.rbegin(); }
	const_reverse_iterator rbegin() const noexcept { return this->Buf.rbegin(); }
	reverse_iterator rend() noexcept { return this->Buf.rend(); }
	const_reverse_iterator rend() const noexcept { return this->Buf.rend(); }
	const_reverse_iterator crbegin() const noexcept { return this->Buf.crbegin(); }
	const_reverse_iterator crend() const noexcept { return this->Buf.crend(); }
	std::basic_string<TCHAR> operator [] (const size_t Count) const { return this->Get(Count); }
	std::basic_string<TCHAR> NodePath;
};

class MSXMLRead {
private:
	Microsoft::WRL::ComPtr<IXMLDOMDocument> lpXmlDoc;
	IXMLDOMNodeList* XmlSetNodeList(const std::basic_string<TCHAR> NodePath);
	std::vector<Node> Data;
	std::basic_string<TCHAR> CommonPath;
	typedef std::vector<Node>::iterator iterator;
	typedef std::vector<Node>::const_iterator const_iterator;
	typedef std::vector<Node>::reverse_iterator reverse_iterator;
	typedef std::vector<Node>::const_reverse_iterator const_reverse_iterator;
	void LoadFromFile(const std::basic_string<TCHAR> NodePath);
public:
	MSXMLRead(const std::basic_string<TCHAR> FileName, const std::basic_string<TCHAR> CommonPath = TEXT(""));
	~MSXMLRead();
	void Load(const std::basic_string<TCHAR> NodePath) { this->LoadFromFile(NodePath); }
	template<class ...Args> void Load(const std::basic_string<TCHAR> NodePath, Args&& ...arg) {
		this->LoadFromFile(NodePath);
		this->Load(std::forward<Args>(arg)...);
	}
	bool NodeDataExists(const std::basic_string<TCHAR> NodePath) const noexcept;
	size_t size() const noexcept { return this->Data.size(); }
	Node at(const size_t Count) const { return this->Data.at(Count); }
	void clear() { this->Data.clear(); }
	void ChangeCommonPath(const std::basic_string<TCHAR> NewRoot);
	Node operator [] (const size_t Count) const { return this->at(Count); }
	Node operator [] (const std::basic_string<TCHAR> NodePath) const;
	iterator begin() noexcept { return this->Data.begin(); }
	const_iterator begin() const noexcept { return this->Data.begin(); }
	iterator end() noexcept { return this->Data.end(); }
	const_iterator end() const noexcept { return this->Data.end(); }
	const_iterator cbegin() const noexcept { return this->Data.cbegin(); }
	const_iterator cend() const noexcept { return this->Data.cend(); }
	reverse_iterator rbegin() noexcept { return this->Data.rbegin(); }
	const_reverse_iterator rbegin() const noexcept { return this->Data.rbegin(); }
	reverse_iterator rend() noexcept { return this->Data.rend(); }
	const_reverse_iterator rend() const noexcept { return this->Data.rend(); }
	const_reverse_iterator crbegin() const noexcept { return this->Data.crbegin(); }
	const_reverse_iterator crend() const noexcept { return this->Data.crend(); }
};
#endif
