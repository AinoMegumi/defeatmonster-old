#ifndef __BATTLE_HPP__
#define __BATTLE_HPP__
#include "Status.hpp"
#include "ref.hpp"

class Battle {
private:
	standard::ArrayRef<Status, 3> Characters;
	std::pair<SkillA, SkillA> ReceptPlayerCommand();
	void SendAICommand(const SkillA& AttackInfo);
public:
	Battle(std::array<Status, 3>& Characters);
};
#endif
