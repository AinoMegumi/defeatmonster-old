﻿#ifndef __XMLREAD_HPP__
#define __XMLREAD_HPP__
#include "StatusRawData.hpp"
#include "Skill.hpp"
#include <tuple>

std::vector<StatusRawData> ReadPlayerData();
std::vector<StatusRawData> ReadPartnerData();
std::vector<StatusRawData> ReadEnemyData();
std::tuple<std::vector<SkillA>, std::vector<SkillA>, std::vector<SkillA>> LoadAllMagic();
std::vector<size_t> GetLevelUpBorderPointList(const std::string Tag);
#endif
