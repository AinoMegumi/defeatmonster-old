#include "Battle.hpp"

Battle::Battle(std::array<Status, 3>& Characters) 
	: Characters(Characters) {}

static inline SkillA ConvertMagic(const int CommandID, const std::vector<SkillA>& AbleToUseAttackMagicList) {
	if (static_cast<int>(AbleToUseAttackMagicList.size()) <= CommandID) throw std::out_of_range("");
	return AbleToUseAttackMagicList.at(CommandID);
}

static inline SkillA ConvertAttackMagic(const int CommandID, const Status& CharacterInfo) {
	return ConvertMagic(CommandID - 1, CharacterInfo.AbleToUseAttackMagic);
}

static inline SkillA ConvertSingleCureMagic(const int CommandID, const Status& CharacterInfo) {
	return ConvertMagic(CommandID - static_cast<int>(CharacterInfo.AbleToUseAttackMagic.size()), CharacterInfo.AbleToUseSingleCureMagic);
}

static inline SkillA ConvertRangeCureMagic(const int CommandID, const Status& CharacterInfo) {
	return ConvertMagic(
		CommandID - static_cast<int>(CharacterInfo.AbleToUseAttackMagic.size() + CharacterInfo.AbleToUseSingleCureMagic.size()) + 1, 
		CharacterInfo.AbleToUseRangeCureMagic
	);
}

static inline SkillA ConvertSkillInfo(const int CommandID, const Status& CharacterInfo) {
	if (0 == CommandID) return SkillA("N/A", "", 0, 0, "N/A", ElementInfo::Normal, "N/A", "N/A", false, false, 0, 0, -1);
	else {
		try {
			return ConvertAttackMagic(CommandID, CharacterInfo);
		}
		catch (std::out_of_range) {}
		try {
			return ConvertSingleCureMagic(CommandID, CharacterInfo);
		}
		catch (std::out_of_range) {
			return ConvertRangeCureMagic(CommandID, CharacterInfo);
		}
	}
}

std::pair<SkillA, SkillA> Battle::ReceptPlayerCommand() {
	int PlayerCommandID = 0;
	int PartnerCommandID = 0;
	// 受信部分をここに書く

	return std::make_pair(ConvertSkillInfo(PlayerCommandID, this->Characters[0]), ConvertSkillInfo(PartnerCommandID, this->Characters[1]));
}
