﻿#include "StatusRawData.hpp"
#include "XmlRead.hpp"
#include "EnvironmentVariable.hpp"
#include "Win32API.hpp"

StatusRawData::StatusRawData(const std::string Name, const std::string Tag, const int SexID, const int HP, const int MP, const int Attack, const int Defence, 
	const int MagicAttack, const int MagicDefence, const int MagicCure, const int Speed, const int Cleverness, const std::string Element1, const std::string Element2)
	: Name(Name), Tag(Tag), SexID(SexID), HP(HP), MP(MP), Attack(Attack), Defence(Defence), MagicAttack(MagicAttack), MagicDefence(MagicDefence), MagicCure(MagicCure),
	Speed(Speed), Cleverness(Cleverness), 
	LevelUpBorderPointList(
		GetLevelUpBorderPointList(
			(Tag == "balance" || Tag == "warrior" || Tag == "wizard" || Tag == "crerick")
			? "player" : Tag
		)
	), Element1(Element1), Element2(Element2) {
	this->CurrentExp = 0;
}
