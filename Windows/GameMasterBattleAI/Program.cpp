#include "ApplicationDirectory.hpp"

namespace Program {
	ApplicationDirectoryA AppPath;
	std::string ChangeFullPath(const std::string FilePath) {
		return AppPath.ChangeFullPath(FilePath, true);
	}
	void InitializeComponent() {
		AppPath = ApplicationDirectoryA::Initialize(true);
	}
}
