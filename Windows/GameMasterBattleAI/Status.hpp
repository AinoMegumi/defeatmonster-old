﻿#ifndef __STATUS_HPP__
#define __STATUS_HPP__
#include "PossibleChangeStatusArrange.hpp"
#include "UseDamageCalculationStatus.hpp"
#include "SpeedManager.hpp"
#include "DamageManager.hpp"
#include "StatusRawData.hpp"
#include "LevelManager.hpp"
#include "Skill.hpp"

class Status {
public:
	std::string Name;
	std::string IdentifiedTag;
private:
	int SexID;
public:
	PossibleChangeStatusArrange<int> HP;
	PossibleChangeStatusArrange<int> MP;
	UseDamageCalculationStatus<int> Physical;
	UseDamageCalculationStatus<int> Magic;
	UseDamageCalculationParameter<int> MagicCure;
	SpeedManager<int> Speed;
	UseDamageCalculationParameter<int> Cleverness;
private:
	DamageManager Damage;
public:
	LevelManager Level;
private:
	std::vector<SkillA> AttackMagic;
	std::vector<SkillA> SingleCureMagic;
	std::vector<SkillA> RangeCureMagic;
public:
	std::vector<SkillA> AbleToUseAttackMagic;
	std::vector<SkillA> AbleToUseSingleCureMagic;
	std::vector<SkillA> AbleToUseRangeCureMagic;
	bool IsEngageCharacter;
	bool Guard;
private:
	std::vector<SkillA> CreateAbleToUseMagicList(const std::vector<SkillA> BaseMagicList);
public:
	Status() = default;
	Status(const StatusRawData Raw);
	// Status(const StatusRawData Raw, const size_t ReservedLevel);
	// Status(const StatusRawData Raw, const size_t MaxLevel);
	void CreateAbleToUseMagicList();
	int Crash(const Status Attacker, bool &CriticalFlag);
	int Crash(const SkillA AttackSkill, const Status Attacker);
	int AttackTestForInternalAI(const Status Attacker) const;
	int AttackTestForInternalAI(const SkillA AttackSkill, const Status Attacker) const;
	int Cure(const Status Healer, const SkillA CureMagic);
	bool Resurrection(const Status Healer);
	bool Dead() const noexcept;
	void AddSpecialAttackMagic(const SkillA Magic);
	void AddSpecialSingleCureMagic(const SkillA Magic);
	void AddSpecialRangeCureMagic(const SkillA Magic);
	void ClearRangeCureMagic();
	void RemoveReservedMagic(const std::string Name);
	void CleanEngage();
};
#endif
