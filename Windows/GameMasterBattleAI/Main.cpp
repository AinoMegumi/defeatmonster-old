#ifdef _DEBUG
#include "KgWinException.hpp"
#include <iostream>

int main() {
	try {

	}
	catch (const std::exception& er) {
		std::cout << er.what() << std::endl;
	}
	catch (const KgWinException& kex) {
		kex.GraphErrorMessageOnConsole();
	}
}
#else
#include "KgWinException.hpp"
#include <Windows.h>
#include <strsafe.h>
#include <process.h>
static SERVICE_STATUS SvcStatus;
static SERVICE_STATUS_HANDLE SvcStatusHandle;

DWORD WINAPI HandlerEx(DWORD, DWORD, LPVOID, LPVOID) {

	return NO_ERROR;
}

UINT WINAPI ServiceThread(PVOID) {
	return 0u;
}

void WINAPI ServiceMain(DWORD, LPTSTR lpszArgv[]) {
	SvcStatusHandle = RegisterServiceCtrlHandlerEx(lpszArgv[0], HandlerEx, NULL);

}

int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int) {
	SERVICE_TABLE_ENTRYA SvcTable[] = {
		{ "kwdmgmai", ServiceMain },
		{ NULL, NULL }
	};
}
#endif
