﻿#ifndef __STATUSRAWDATA_HPP__
#define __STATUSRAWDATA_HPP__
#include <string>
#include <vector>

struct StatusRawData {
	StatusRawData() = default;
	StatusRawData(const std::string Name, const std::string Tag, const int SexID, const int HP, const int MP, const int Attack, const int Defence, const int MagicAttack,
		const int MagicDefence,	const int MagicCure, const int Speed, const int Cleverness, const std::string Element1, const std::string Element2);
	std::string Name, Tag;
	int SexID, HP, MP, Attack, Defence, MagicAttack, MagicDefence, MagicCure, Speed, Cleverness;
	std::vector<size_t> LevelUpBorderPointList;
	size_t CurrentExp;
	std::string Element1, Element2;
};
#endif
