﻿#include "Status.hpp"
#include "Split.hpp"
#include "KgWinException.hpp"
#include "Random.hpp"
#include "XmlRead.hpp"
#include <array>
#include <cmath>
constexpr double CriticalRate = 0.1;

Status::Status(const StatusRawData Raw)
	: Name(Raw.Name), IdentifiedTag(Raw.Tag), HP(Raw.HP), MP(Raw.MP), Physical(Raw.Attack, Raw.Defence), Magic(Raw.MagicAttack, Raw.MagicDefence),
	MagicCure(Raw.MagicCure), Speed(Raw.Speed), Cleverness(Raw.Cleverness),
	Damage(std::make_pair(Element::ElementCast(Raw.Element1), Element::ElementCast(Raw.Element2))),
	Level(LevelManager(Raw.LevelUpBorderPointList, Raw.CurrentExp)) {
	if (Raw.MP != 0) {
		const auto MagicList = LoadAllMagic();
		for (auto& i : std::get<0>(MagicList)) if (i.LessMP <= Raw.MP) this->AttackMagic.emplace_back(i);
		for (auto& i : std::get<1>(MagicList)) if (i.LessMP <= Raw.MP) this->SingleCureMagic.emplace_back(i);
		for (auto& i : std::get<2>(MagicList)) if (i.LessMP <= Raw.MP) this->RangeCureMagic.emplace_back(i);
	}
}

std::vector<SkillA> Status::CreateAbleToUseMagicList(const std::vector<SkillA> BaseMagicList) {
	std::vector<SkillA> Arr;
	for (auto& i : BaseMagicList) if (this->MP >= i.UseMP) Arr.emplace_back(i);
	return Arr;
}

void Status::CreateAbleToUseMagicList() {
	if (!this->AttackMagic.empty()) this->AbleToUseAttackMagic.clear();
	this->AbleToUseAttackMagic = this->CreateAbleToUseMagicList(this->AttackMagic);
	if (!this->SingleCureMagic.empty()) this->AbleToUseSingleCureMagic.clear();
	this->AbleToUseSingleCureMagic = this->CreateAbleToUseMagicList(this->SingleCureMagic);
	if (!this->RangeCureMagic.empty()) this->AbleToUseRangeCureMagic.clear();
	this->AbleToUseRangeCureMagic = this->CreateAbleToUseMagicList(this->RangeCureMagic);
}

static inline void CorrectMinusDamage(int &DamagePoint) {
	if (DamagePoint <= 0) {
		std::bernoulli_distribution DamageJudge(0.5);
		DamagePoint = DamageJudge(Random::GetEngine()) ? 1 : 0;
	}
}

int Status::Crash(const Status Attacker, bool& CriticalFlag) {
	CriticalFlag = false;
	int DamagePoint = ((Attacker.Physical.AttackParam - (this->Physical.DefenceParam / (this->Guard ? 1 : 2))) / 2).Get();
	if (DamagePoint > 0) {
		std::uniform_int_distribution<int> DamageRand(-DamagePoint / 16, DamagePoint / 16);
		DamagePoint += DamageRand(Random::GetEngine());
		std::bernoulli_distribution Critical(CriticalRate);
		if (CriticalFlag = Critical(Random::GetEngine()); CriticalFlag) {
			if (DamagePoint > 0) DamagePoint = static_cast<int>(static_cast<float>(DamagePoint) * 1.3 + 0.5); // 小数点以下四捨五入
			else DamagePoint = 2;
		}
	}
	CorrectMinusDamage(DamagePoint);
	this->HP -= DamagePoint;
	return DamagePoint;
}

int Status::Crash(const SkillA AttackSkill, const Status Attacker) {
	int DamagePoint = static_cast<int>(
		(static_cast<double>(Attacker.Magic.AttackParam.Get()) * Attacker.Damage.GetAttackAdvantage(AttackSkill.SkillElement))
		- (static_cast<double>(this->Magic.DefenceParam.Get()) / ((this->Guard ? 1 : 2) * this->Damage.GetCrashAdvantage(AttackSkill.SkillElement)))
		) / 2;
	if (DamagePoint > 0) {
		std::uniform_int_distribution<int> DamageRand(-DamagePoint / 16, DamagePoint / 16);
		DamagePoint += DamageRand(Random::GetEngine());
	}
	CorrectMinusDamage(DamagePoint);
	this->HP -= DamagePoint;
	return DamagePoint;
}

int Status::AttackTestForInternalAI(const Status Attacker) const {
	return std::max(0, (Attacker.Physical.AttackParam - (this->Physical.DefenceParam / (this->Guard ? 1 : 2))).Get() * 15 / 32);
}

int Status::AttackTestForInternalAI(const SkillA AttackSkill, const Status Attacker) const {
	return std::max(0, static_cast<int>(
			(static_cast<double>(Attacker.Magic.AttackParam.Get()) * Attacker.Damage.GetAttackAdvantage(AttackSkill.SkillElement))
			- (static_cast<double>(this->Magic.DefenceParam.Get()) / ((this->Guard ? 1 : 2) * this->Damage.GetCrashAdvantage(AttackSkill.SkillElement)))
		) * 15 / 32
	);
}

int Status::Cure(const Status Healer, const SkillA CureMagic) {
	std::uniform_int_distribution<int> rand(0, Healer.MagicCure.Get() / 4);
	const int CurePoint = CureMagic.BasePower + rand(Random::GetEngine());
	const auto BeforeCureHP = this->HP;
	this->HP += CurePoint;
	return (this->HP - BeforeCureHP).Get();
}

bool Status::Resurrection(const Status Healer) {
	std::bernoulli_distribution successjudge(0.5);
	const bool Result = successjudge(Random::GetEngine());
	if (Result) {
		std::uniform_int_distribution<int> rand(0, Healer.MagicCure.Get() / 20);
		this->HP += Healer.MagicCure.Get() + rand(Random::GetEngine());
	}
	return Result;
}

bool Status::Dead() const noexcept {
	return this->HP.IsMin();
}

void Status::AddSpecialAttackMagic(const SkillA MagicInfo) {
	this->AttackMagic.emplace_back(MagicInfo);
}

void Status::AddSpecialSingleCureMagic(const SkillA MagicInfo) {
	this->SingleCureMagic.emplace_back(MagicInfo);
}

void Status::AddSpecialRangeCureMagic(const SkillA MagicInfo) {
	this->RangeCureMagic.emplace_back(MagicInfo);
}

void Status::ClearRangeCureMagic() {
	this->RangeCureMagic.clear();
}

static constexpr int GetEngageUseMP(const PossibleChangeStatus<int> PartnerMP) {
	return std::min(PartnerMP.GetMax() / 20, PartnerMP.Get());
}

void Status::CleanEngage() {
	this->Physical.AttackReset();
	this->Magic.AttackReset();
}

bool EraseReservedMagic(std::vector<SkillA> &MagicList, const std::string Name) {
	if (MagicList.empty()) return false;
	for (size_t i = 0; i < MagicList.size(); i++) {
		if (MagicList[i].Name == Name) {
			MagicList.erase(MagicList.begin() + i);
			return true;
		}
	}
	return false;
}

void Status::RemoveReservedMagic(const std::string MagicName) {
	if (EraseReservedMagic(this->AttackMagic, MagicName)) return;
	if (EraseReservedMagic(this->SingleCureMagic, MagicName)) return;
	EraseReservedMagic(this->RangeCureMagic, MagicName);
}
