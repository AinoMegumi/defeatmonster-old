﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OnlineRelayServer
{
    static class Program
    {
        public static string GameMasterReferenceNumber = "a0000001";
        /// <summary>
        /// アプリケーションのメイン エントリ ポイントです。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MatchingWindow());
            Application.Run(new Chat("ＧＭ神御田", "プレイヤー"));
        }
    }
}
