﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OnlineRelayServer
{
    public partial class Chat : Form
    {
        private string PlayerName;
        private string PartnerName;
        public Chat(string Player, string Partner)
        {
            PlayerName = Player;
            PartnerName = Partner;
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (richTextBox1.Lines.Length == 100)
            {
                int st_idx = richTextBox1.GetFirstCharIndexFromLine(0);
                int nx_idx = richTextBox1.GetFirstCharIndexFromLine(1);
                richTextBox1.Text = richTextBox1.Text.Remove(st_idx, nx_idx - st_idx);
            }
            richTextBox1.AppendText(PlayerName + " : " + textBox1.Text + "\n");
            textBox1.Text = "";
        }
    }
}
