#ifndef NOMINMAX
#define NOMINMAX
#endif
#include <Windows.h>
#include <Shlwapi.h>
#include "../../EnvironmentVariable/EnvironmentVariable.hpp"
#include "KgWinException.hpp"
#include "ApplicationDirectory.hpp"
#include "XmlWriter.hpp"
#include "XmlReader.hpp"
#include <boost/property_tree/xml_parser.hpp>
#include <random>
#include <algorithm>
#include <iostream>
#pragma comment(lib, "Shlwapi.lib")

auto RandEngine() {
	std::random_device rnd;
	std::vector<std::uint_least32_t> v(10);// 初期化用ベクタ
	std::generate(v.begin(), v.end(), std::ref(rnd));// ベクタの初期化
	std::seed_seq seed(v.begin(), v.end());
	return std::mt19937(seed);// 乱数エンジン
}

std::mt19937& MakeMt() {
	static std::mt19937 mt = RandEngine();
	return mt;
}

unsigned __int64 CreateRandNum(const unsigned __int64 max, const unsigned __int64 min) {
	std::uniform_int_distribution<unsigned __int64> rand(std::min(min, max), std::max(max, min));
	return rand(MakeMt());
}

void SetUsingDllDirectory() {
#ifndef WIN64
	SetCurrentDirectoryA((EnvironmentVariableA("BOOST_SHARED_LIB").Get() + "\\Win32").c_str());
#else
	SetCurrentDirectoryA((EnvironmentVariableA("BOOST_SHARED_LIB").Get() + "\\x64").c_str());
#endif
}

std::vector<std::pair<std::string, std::string>> LoadTag() {
	std::vector<std::pair<std::string, std::string>> Arr;
	Arr.emplace_back(std::make_pair("プレイヤー", "player"));
	boost::property_tree::ptree pt;
	boost::property_tree::xml_parser::read_xml("D:\\Users\\AinoMegumi\\Bitbucket\\Kamioda Games\\Kamioda Warrior\\Files\\DefeatMonster\\Status\\partner.xml", pt);
	for (auto& i : pt.get_child("characterdatabook")) Arr.emplace_back(std::make_pair(GetTextFromNodeA(i.second, "name"), GetTextFromNodeA(i.second, "tag")));
	return Arr;
}

#include <thread>
#include <mutex>

bool Stop = false;
std::mutex mutex;
std::vector<std::pair<std::string, unsigned __int64>> Buffer;

void PrintThread() {
	size_t LastSize = 0;
	while (!Stop) {
		std::lock_guard<std::mutex> Guard(mutex);
		if (Buffer.size() == LastSize) continue;
		LastSize = Buffer.size();
		std::cout <<  Buffer.back().first <<"のレベル100までの合計経験値 : " << Buffer.back().second << std::endl;
	}
}

int main(int argc, char* argv[]) {
	try {
		SetUsingDllDirectory();
		std::thread th(PrintThread);
		const bool Recreate = [](int argc, char* argv[]) {
			if (argc == 1) return false;
			return std::string(argv[1]) == "recreate";
		}(argc, argv);
		if (TRUE == PathFileExistsA("D:\\Users\\AinoMegumi\\BitBucket\\Kamioda Games\\Kamioda Warrior\\DefeatMonster\\DefeatMonster\\DefeatMonster\\Status\\exp.xml")
			&& !Recreate
			) throw std::runtime_error("経験値情報は既に生成されています。");
		boost::property_tree::ptree Root;
		XmlWriteInformation Info(
#ifdef _DEBUG
			"D:\\Users\\AinoMegumi\\BitBucket\\Kamioda Games\\Kamioda Warrior\\DefeatMonster\\LevelUpStatusCreatorForDefeatMonster\\LevelUpExpCreatorForDefeatMonster\\exptest.xml"
#else
			"D:\\Users\\AinoMegumi\\BitBucket\\Kamioda Games\\Kamioda Warrior\\DefeatMonster\\DefeatMonster\\DefeatMonster\\Status\\exp.xml"
#endif
			, "expdatabook"
			);
		const std::vector<std::pair<std::string, std::string>> Buf = LoadTag();
		th.detach();
		for (auto& i : Buf) {
			std::vector<Child> Children;
			Children.emplace_back(XmlWrite::CreateChild("tag", i.second));
			unsigned __int64 Total = 0;
			std::vector<unsigned __int64> Arr;
			while (Total < 100000) {
				unsigned __int64 Max = 30, Min = 15;
				Total = 0;
				for (int p = 2; p <= 100; p++) {
					const unsigned __int64 RandNum = CreateRandNum(Max, Min);
					Arr.emplace_back(RandNum);
					Total += RandNum;
					Min = RandNum;
					Max = RandNum * 11 / 10;
					if (Min > Max) {
						std::cout << "range bug" << std::endl;
						std::cout << "Max : " << Max << "　Min : " << Min << std::endl;
						std::system("pause");
					}
				}
			}
			Buffer.emplace_back(std::make_pair(i.first, Total));
			for (int p = 2; p <= 100; p++) {
				Children.emplace_back(XmlWrite::CreateChild("lv" + std::to_string(p), std::to_string(Arr[p - 2])));
			}
			Info.TreeDataList.emplace_back(XmlWrite::CreateTree("expdata", Children));
			Sleep(100);
		}
		XmlWrite::WriteXml(Info);
	}
	catch (const KgWinException& kex) {
		kex.GraphErrorMessageOnConsole();
	}
	catch (const std::exception& er) {
		std::cout << er.what() << std::endl;
	}
	std::lock_guard<std::mutex> Guard(mutex);
	Stop = true;
	return 0;
}
