﻿#pragma once
#include <string>
#include <vector>

struct ParameterInfoA {
	ParameterInfoA(const std::string Name, std::string Tag, const int HP, const int MP, const int Attack, const int Defence,
		const int MagicAttack, const int MagicDefence, const int MagicCure, const int Speed, const int Cleverness)
		: Name(Name), Tag(Tag), HP(HP), MP(MP), Attack(Attack), Defence(Defence), MagicAttack(MagicAttack),
		MagicDefence(MagicDefence), MagicCure(MagicCure), Speed(Speed), Cleverness(Cleverness) {}
	std::string Name, Tag;
	int HP, MP, Attack, Defence, MagicAttack, MagicDefence, MagicCure, Speed, Cleverness;
};

struct ParameterInfoW {
	ParameterInfoW(const std::wstring Name, std::wstring Tag, const int HP, const int MP, const int Attack, const int Defence,
		const int MagicAttack, const int MagicDefence, const int MagicCure, const int Speed, const int Cleverness)
		: Name(Name), Tag(Tag), HP(HP), MP(MP), Attack(Attack), Defence(Defence), MagicAttack(MagicAttack), 
		MagicDefence(MagicDefence),	MagicCure(MagicCure), Speed(Speed), Cleverness(Cleverness) {}
	std::wstring Name, Tag;
	int HP, MP, Attack, Defence, MagicAttack, MagicDefence, MagicCure, Speed, Cleverness;
};

#ifdef UNICODE
typedef ParameterInfoW ParameterInfo;
#else
typedef ParameterInfoA ParameterInfo;
#endif

std::vector<ParameterInfo> GetParameterInfo(const std::string FilePath);
