﻿#pragma once
#include <string>
#include <vector>
#include <utility>

class WritePropertiesFileA {
private:
	std::string FileName;
	std::ios::openmode Mode;
	std::vector<std::pair<std::string, std::string>> WriteData;
	std::vector<std::string> Comment;
public:
	WritePropertiesFileA(const std::string FileName, const bool CreateNewFile = false);
	void AddData(const std::string Tag, const std::string Data);
	void AddComment(const std::string Comment);
	void Write();
};

class WritePropertiesFileW {
private:
	std::wstring FileName;
	std::ios::openmode Mode;
	std::vector<std::pair<std::wstring, std::wstring>> WriteData;
	std::vector<std::wstring> Comment;
public:
	WritePropertiesFileW(const std::wstring FileName, const bool CreateNewFile = false);
	void AddData(const std::wstring Tag, const std::wstring Data);
	void AddComment(const std::wstring CommentMessage);
	void Write();
};

#ifdef UNICODE
typedef WritePropertiesFileW WritePropertiesFile;
#else
typedef WritePropertiesFileA WritePropertiesFile;
#endif
