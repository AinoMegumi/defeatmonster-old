﻿#include "PropertiesFileWriter.h"
#include <Shlwapi.h>
#include <fstream>
#pragma comment(lib, "shlwapi.lib")

WritePropertiesFileA::WritePropertiesFileA(const std::string FileName, const bool CreateNewFile) : FileName(FileName) {
	this->Mode = std::ios::binary | std::ios::out | ((CreateNewFile && FALSE != PathFileExistsA(FileName.c_str())) ? std::ios::trunc : std::ios::app);
}

void WritePropertiesFileA::AddData(const std::string Tag, const std::string Data) {
	this->WriteData.emplace_back(std::make_pair(Tag, Data));
}

void WritePropertiesFileA::AddComment(const std::string CommentMessage) {
	this->Comment.emplace_back("#" + CommentMessage);
}

void WritePropertiesFileA::Write() {
	std::ofstream ofs(this->FileName, this->Mode);
	// for (const std::string c : this->Comment) ofs << c << std::endl;
	for (const std::pair<std::string, std::string> i : this->WriteData) ofs << i.first << "=" << i.second << std::endl;
	ofs.close();
}

WritePropertiesFileW::WritePropertiesFileW(const std::wstring FileName, const bool CreateNewFile) : FileName(FileName) {
	this->Mode = std::ios::binary | std::ios::out | ((CreateNewFile && FALSE != PathFileExistsW(FileName.c_str())) ?  std::ios::trunc : std::ios::app);
}

void WritePropertiesFileW::AddData(const std::wstring Tag, const std::wstring Data) {
	this->WriteData.emplace_back(std::make_pair(Tag, Data));
}

void WritePropertiesFileW::AddComment(const std::wstring CommentMessage) {
	this->Comment.emplace_back(L"#" + CommentMessage);
}

#include <codecvt>
#include <locale>

void WritePropertiesFileW::Write() {
	std::wofstream ofs(this->FileName, this->Mode);
	ofs.imbue(std::locale(std::locale(), new std::codecvt_utf8<wchar_t>));
	// for (const std::wstring c : this->Comment) ofs << c << std::endl;
	for (const std::pair<std::wstring, std::wstring> i : this->WriteData) ofs << i.first << "=" << i.second << std::endl;
	ofs.close();
}
