﻿#include "Random.h"
#include "PropertiesFileWriter.h"
#include "ParameterInfo.h"
#include <algorithm>
#ifndef NOMINMAX
#define NOMINMAX
#endif
#include <Windows.h>
#include <Shlwapi.h>
#include <iostream>
#include <cstdlib>
#pragma comment(lib, "shlwapi.lib")

static inline int CreateRandNum(const int BaseNum) {
	if (0 == BaseNum) return 0;
	return CreateRandNum(std::max(2, BaseNum / 100 + 2), std::max(1, BaseNum / 100));
}

static inline std::basic_string<TCHAR> CreateFilePath(const std::basic_string<TCHAR> Tag) {
	return 
#ifdef _DEBUG
	TEXT("D:\\Users\\AinoMegumi\\BitBucket\\Kamioda Games\\Kamioda Warrior\\DefeatMonster\\LevelUpStatusCreatorForDefeatMonster\\LevelUpStatusCreatorForDefeatMonster\\TestProp\\")
#else
	TEXT("D:\\Users\\AinoMegumi\\BitBucket\\Kamioda Games\\Kamioda Warrior\\DefeatMonster\\LevelUpStatusCreatorForDefeatMonster\\LevelUpStatusCreatorForDefeatMonster\\Level\\")
#endif
		+ Tag + TEXT(".properties");
}

static inline std::basic_string<TCHAR> CreateWriteData(const ParameterInfo& i) {
	return
#ifdef UNICODE
		std::to_wstring(CreateRandNum(i.HP)) + TEXT("/") + std::to_wstring(CreateRandNum(i.MP)) + TEXT("/")
		+ std::to_wstring(CreateRandNum(i.Attack)) + TEXT("/") + std::to_wstring(CreateRandNum(i.Defence)) + TEXT("/")
		+ std::to_wstring(CreateRandNum(i.MagicAttack)) + TEXT("/")	+ std::to_wstring(CreateRandNum(i.MagicDefence)) + TEXT("/")
		+ std::to_wstring(CreateRandNum(i.MagicCure)) + TEXT("/") + std::to_wstring(CreateRandNum(i.Speed)) + TEXT("/")
		+ std::to_wstring(CreateRandNum(i.Cleverness))
#else
		std::to_string(CreateRandNum(i.HP)) + TEXT("/")	+ std::to_string(CreateRandNum(i.MP)) + TEXT("/")
		+ std::to_string(CreateRandNum(i.Attack)) + TEXT("/") + std::to_string(CreateRandNum(i.Defence)) + TEXT("/")
		+ std::to_string(CreateRandNum(i.MagicAttack)) + TEXT("/") + std::to_string(CreateRandNum(i.MagicDefence)) + TEXT("/")
		+ std::to_string(CreateRandNum(i.MagicCure)) + TEXT("/") + std::to_string(CreateRandNum(i.Speed)) + TEXT("/")
		+ std::to_string(CreateRandNum(i.Cleverness))
#endif
		;
}

static inline std::basic_string<TCHAR> CreateTag(const int LoopCounter) {
	return
#ifdef UNICODE
		std::to_wstring(LoopCounter + 2)
#else
		std::to_string(LoopCounter + 2)
#endif
		;
}

void println(const std::basic_string<TCHAR> Message) {
#ifdef UNICODE
	std::wcout << Message << std::endl;
#else
	std::cout << Message << std::endl;
#endif
}

std::string WStringToString(const std::wstring oWString) {
	int iBufferSize = WideCharToMultiByte(CP_OEMCP, 0, oWString.c_str(), -1, (char *)NULL, 0, NULL, NULL);
	char* cpMultiByte = new char[iBufferSize];
	WideCharToMultiByte(CP_OEMCP, 0, oWString.c_str(), -1, cpMultiByte, iBufferSize, NULL, NULL);
	std::string oRet(cpMultiByte, cpMultiByte + iBufferSize - 1);
	delete[] cpMultiByte;
	return oRet;
}

std::wstring StringToWString(const std::string oString) {
	const int iBufferSize = MultiByteToWideChar(CP_ACP, 0, oString.c_str(), -1, nullptr, 0);
	wchar_t* cpWideChar = new wchar_t[iBufferSize];
	MultiByteToWideChar(CP_ACP, 0, oString.c_str(), -1, cpWideChar, iBufferSize);
	std::wstring oRet(cpWideChar, cpWideChar + iBufferSize - 1);
	delete[] cpWideChar;
	return oRet;
}

struct CommandLine {
	CommandLine(int argc, char* argv[]) {
		if (argc == 1) throw std::exception("ターゲットを指定してください。");
		std::vector<std::string> Temp;
		for (int i = 1; i < argc; i++) Temp.emplace_back(std::string(argv[i]));
		this->Target = Temp[0];
		if (this->Target != "player" && this->Target != "partner") throw std::exception("第１引数はplayerまたはpartnerを指定してください。");
		this->Recreate = Temp.back() == "recreate";
		if (Temp.back() == "recreate") Temp.pop_back();
		Temp.erase(Temp.begin());
		this->ReservedCharacter = Temp;
	}
	bool Recreate;
	std::string Target;
	std::vector<std::string> ReservedCharacter;
};



std::vector<ParameterInfo> Pickup(std::vector<std::string> ArgList, const std::vector<ParameterInfo> Arr) {
	std::vector<ParameterInfo> Buffer;
	for (auto& i : ArgList) {
		for (auto& j : Arr) {
			if (
#ifdef UNICODE
				StringToWString(i)
#else
				i 
#endif
				== j.Name) {
				Buffer.emplace_back(j);
				break;
			}
		}
	}
	return Buffer;
}

constexpr const char* XmlDir = "D:\\Users\\AinoMegumi\\Bitbucket\\Kamioda Games\\Kamioda Warrior\\DefeatMonster\\Windows\\DefeatMonster\\Status\\";

int main(int argc, char* argv[]) {
#ifdef UNICODE
	std::wcout.imbue(std::locale(""));
#endif
	try {
		CommandLine Cmd = CommandLine(argc, argv);
#ifdef _DEBUG
		const std::string FilePath = XmlDir + Cmd.Target + ".xml";
		std::cout << FilePath << std::endl;
#endif
		const std::vector<ParameterInfo> Arr = Cmd.ReservedCharacter.empty()
			? GetParameterInfo(XmlDir + Cmd.Target + ".xml")
			: Pickup(Cmd.ReservedCharacter, GetParameterInfo(XmlDir + Cmd.Target + ".xml"));
		for (auto& i : Arr) {
			try {
				std::basic_string<TCHAR> FilePath = CreateFilePath(i.Tag);
#ifndef _DEBUG // リリース分はコロコロ変わると困るけど、デバッグ時は逆に変わってくれたほうが分かりやすい
				if (!Cmd.Recreate && FALSE != PathFileExists(FilePath.c_str())) throw std::runtime_error("既にファイルが存在します。");
#endif
				WritePropertiesFile prop(FilePath, true);
				for (int t = 0; t < 99; t++) prop.AddData(CreateTag(t), CreateWriteData(i));
				prop.Write();
				println(i.Name + (i.Name.size() <= 3 ? TEXT("\t") : TEXT("")) + TEXT("\t : 成功"));
			}
			catch (const std::exception& er) {
				Cmd.Target == "partner"
					? std::cout << WStringToString(i.Name) << (i.Name.size() <= 3 ? "\t" : "") << "\t : 失敗(" << er.what() << ")" << std::endl
					: std::cout << WStringToString(i.Tag) << ".properties\t : 失敗(" << er.what() << ")" << std::endl;
			}
			Sleep(500);
		}
		return 0;
	}
	catch (const std::exception& er) {
		return MessageBoxA(NULL, er.what(), "エラー", MB_ICONERROR | MB_OK);
	}
	/*
	if (argc < 2) return 0;
	const bool Reset =
		argc == 3
		? (std::string(argv[2]) == "recreate")
		: false;
	if (std::string(argv[1]) == "partner") {
#ifdef UNICODE
		std::wcout.imbue(std::locale(""));
#endif
		try {
			for (auto& i : GetParameterInfo(partner.xml")) {
				try {
					std::basic_string<TCHAR> FilePath = CreateFilePath(i.Tag);
#ifndef _DEBUG // リリース分はコロコロ変わると困るけど、デバッグ時は逆に変わってくれたほうが分かりやすい
					if (!Reset && FALSE != PathFileExists(FilePath.c_str())) throw std::runtime_error("既にファイルが存在します。");
#endif
					WritePropertiesFile prop(FilePath, true);
					for (int t = 0; t < 99; t++) prop.AddData(CreateTag(t), CreateWriteData(i));
					prop.Write();
					println(i.Name + (i.Name.size() <= 3 ? TEXT("\t") : TEXT("")) + TEXT("\t : 成功"));
				}
				catch (const std::exception& er) {
					std::cout << WStringToString(i.Name) << (i.Name.size() <= 3 ? "\t" : "") << "\t : 失敗(" << er.what() << ")" << std::endl;
				}
				Sleep(500);
			} // range-base for
		} // try
		catch (const std::exception& er) {
			MessageBoxA(NULL, er.what(), "エラー", MB_ICONERROR | MB_OK);
			return -1;
		}
	}
	else {
#ifdef UNICODE
		std::wcout.imbue(std::locale(""));
#endif
		try {
			for (auto& i : GetParameterInfo("D:\\Users\\AinoMegumi\\BitBucket\\Kamioda Games\\Kamioda Warrior\\DefeatMonster\\DefeatMonster\\DefeatMonster.old\\Status\\player.xml")) {
				try {
					std::basic_string<TCHAR> FilePath = CreateFilePath(i.Tag);
#ifndef _DEBUG // リリース分はコロコロ変わると困るけど、デバッグ時は逆に変わってくれたほうが分かりやすい
					if (!Reset && FALSE != PathFileExists(FilePath.c_str())) throw std::runtime_error("既にファイルが存在します。");
#endif
					WritePropertiesFile prop(FilePath, true);
					for (int t = 0; t < 99; t++) prop.AddData(CreateTag(t), CreateWriteData(i));
					prop.Write();
					println(i.Tag + TEXT("\t.properties : 成功"));
				}
				catch (const std::exception& er) {
					std::cout << WStringToString(i.Tag) << ".properties\t : 失敗(" << er.what() << ")" << std::endl;
				}
				Sleep(500);
			} // range-base for
		} // try
		catch (const std::exception &er) {
			MessageBoxA(NULL, er.what(), "エラー", MB_ICONERROR | MB_OK);
			return -1;
		}
	}
	*/
}
