#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>
#include <codecvt>
#include <Windows.h>

static std::string WStringToString(std::wstring oWString) {
	int iBufferSize = WideCharToMultiByte(CP_OEMCP, 0, oWString.c_str(), -1, (char *)NULL, 0, NULL, NULL);
	char* cpMultiByte = new char[iBufferSize];
	WideCharToMultiByte(CP_OEMCP, 0, oWString.c_str(), -1, cpMultiByte, iBufferSize, NULL, NULL);
	std::string oRet(cpMultiByte, cpMultiByte + iBufferSize - 1);
	delete[] cpMultiByte;
	return oRet;
}

static inline void ThrowReadNodeErrorMessage() {
	throw std::runtime_error("xmlのnode読み込みに失敗しました。");
}

static std::wstring GetTextFromNodeW(boost::property_tree::ptree& p, const std::string Path) {
	const boost::optional<std::string> TargetNode = p.get_optional<std::string>(Path);
	if (!TargetNode) ThrowReadNodeErrorMessage();
	static_assert(sizeof(wchar_t) == 2, "In function 'read_xml' : Please check usage of 'std::codecvt_utf8_utf16'");
	std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>, wchar_t> Convert;
	return Convert.from_bytes(TargetNode.get());
}

static std::string GetTextFromNodeA(boost::property_tree::ptree& p, const std::string Path) {
	const boost::optional<std::string> TargetNode = p.get_optional<std::string>(Path);
	if (!TargetNode) ThrowReadNodeErrorMessage();
	static_assert(sizeof(wchar_t) == 2, "In function 'read_xml' : Please check usage of 'std::codecvt_utf8_utf16'");
	std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>, wchar_t> Convert;
	return WStringToString(Convert.from_bytes(TargetNode.get()));
}

#ifdef UNICODE
#define GetTextFromNode GetTextFromNodeW
#else
#define GetTextFromNode GetTextFromNodeA
#endif

template<typename T> static T GetNumFromNode(boost::property_tree::ptree& p, const std::string Path) {
	const boost::optional<T> TargetNode = p.get_optional<T>(Path);
	if (!TargetNode) ThrowReadNodeErrorMessage();
	return TargetNode.get();
}

#include "Status.h"
#include <iostream>

std::vector<Status<int>> GetParameterInfo(const std::string FilePath) {
	std::vector<Status<int>> Arr;
	boost::property_tree::ptree pt;
	boost::property_tree::xml_parser::read_xml(FilePath, pt);
	auto GetIntDataFromNode = [](boost::property_tree::ptree& p, const std::string Tag) { return GetNumFromNode<int>(p, Tag); };
	for (auto& i : pt.get_child("characterdatabook")) {
		try {
			Arr.emplace_back(
				GetTextFromNodeA(i.second, "name"),
				GetTextFromNodeA(i.second, "tag"),
				Parameter<int>(
					GetIntDataFromNode(i.second, "hp"),
					GetIntDataFromNode(i.second, "mp"),
					GetIntDataFromNode(i.second, "attack"),
					GetIntDataFromNode(i.second, "defence"),
					GetIntDataFromNode(i.second, "magicattack"),
					GetIntDataFromNode(i.second, "magicdefence"),
					GetIntDataFromNode(i.second, "magiccure"),
					GetIntDataFromNode(i.second, "speed"),
					[GetIntDataFromNode](boost::property_tree::ptree& i) {
					try {
						return GetIntDataFromNode(i, "clever");
					}
					catch (std::exception) { return 0; }
					}(i.second)
				)
			);
		}
		catch (const std::exception& er) {
			std::cout << GetTextFromNodeA(i.second, "name") << " : " << er.what() << std::endl;
			continue;
		}
	}
	return Arr;
}
