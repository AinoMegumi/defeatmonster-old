#pragma once
template<typename T> std::vector<std::basic_string<T>> SplitString(const std::basic_string<T> &str, T sep) {
	std::vector<std::basic_string<T>> Arr;
	std::basic_string<T> Item;
	for (T i : str) {
		if (i == sep) {
			if (!Item.empty()) Arr.push_back(Item);
			Item.clear();
		}
		else Item += i;
	}
	if (!Item.empty()) Arr.push_back(Item);
	return Arr;
}

#define SplitA SplitString<char>
#define SplitW SplitString<wchar_t>
