#include "PropertiesRead.h"
#include "Split.h"
#include <Windows.h>
#include <Shlwapi.h>
#include <fstream>
#include <array>
#include <sstream>
#pragma comment(lib, "shlwapi.lib")

bool FileExists(const std::string FilePath) {
	return FALSE != PathFileExistsA(FilePath.c_str());
}

static std::string WStringToString(std::wstring oWString) {
	int iBufferSize = WideCharToMultiByte(CP_OEMCP, 0, oWString.c_str(), -1, (char *)NULL, 0, NULL, NULL);
	char* cpMultiByte = new char[iBufferSize];
	WideCharToMultiByte(CP_OEMCP, 0, oWString.c_str(), -1, cpMultiByte, iBufferSize, NULL, NULL);
	std::string oRet(cpMultiByte, cpMultiByte + iBufferSize - 1);
	delete[] cpMultiByte;
	return oRet;
}

static inline std::array<std::string, 2> Split(const std::string& str, char sep) {
	const std::vector<std::string> Data = SplitA(str, sep);
	if (Data.size() != 2) throw std::runtime_error("サイズが間違っています。");
	return{ Data[0], Data[1] };
}

static inline std::array<std::wstring, 2> Split(const std::wstring& str, wchar_t sep) {
	const std::vector<std::wstring> Data = SplitW(str, sep);
	if (Data.size() != 2) throw std::runtime_error("サイズが間違っています。");
	return{ Data[0], Data[1] };
}

PropertiesRead::PropertiesRead(const std::string FilePath, const std::string Encode) {
	if (!FileExists(FilePath)) throw std::ios_base::failure(FilePath + " : file is not exist");
	int ctr = 1;
	if (Encode == "UTF-8" || Encode == "UTF-16") {
		std::wifstream ifs(FilePath);
		for (std::wstring str; std::getline(ifs, str);) {
			if (L'#' == str.at(0)) continue;
			const std::array<std::wstring, 2> Temp = Split(str, L'=');
			this->List.emplace_back(WStringToString(Temp[0]), WStringToString(Temp[1]));
			ctr++;
			Sleep(500);
		}
	}
	else {
		std::ifstream ifs(FilePath);
		for (std::string str; std::getline(ifs, str);) {
			if ('#' == str.at(0)) continue;
			const std::array<std::string, 2> Temp = Split(str, '=');
			this->List.emplace_back(Temp[0], Temp[1]);
			ctr++;
		}
	}
}

std::string PropertiesRead::GetData(const std::string Tag) const {
	for (const PropertiesData i : this->List) if (i.Tag == Tag) return i.Data;
	throw std::runtime_error("当該タグとそれに対応するデータがありません。");
}

std::vector<PropertiesData> PropertiesRead::GetData() const {
	return this->List;
}
