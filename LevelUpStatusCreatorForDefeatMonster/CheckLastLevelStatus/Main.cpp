#include "PropertiesRead.h"
#include "XmlRead.h"
#include "Split.h"
#include <Windows.h>
#include <iostream>

static std::array<std::array<int, 9>, 98> ReadApplyLevelData(const PropertiesRead& prop) {
	std::array<std::array<int, 9>, 98> Arr;
	for (int i = 2; i < 100; i++) {
		const std::vector<std::string> Temp = SplitA(prop.GetData(std::to_string(i)), '/');
		if (Temp.size() != 9) throw std::runtime_error("レベル" + std::to_string(i) + "のデータが不正です。");
		std::array<int, 9> Buffer;
		for (int j = 0; j < 9; j++) {
			Buffer[j] = std::stoi(Temp[j]);
		}
		Arr[i - 2] = Buffer;
	}
	return Arr;
}

static inline void AllGraph(const std::vector<Status<int>> Arr, const std::vector<Status<int>> Arr2) {
	for (int p = 0; p < (int)Arr.size(); p++) {
		auto i = Arr[p];
		auto j = Arr2[p];
		std::cout << i.Name << std::endl;
		printf(
			"HP   : %3d / %4d (+%4d)　MP   : %3d / %4d (+%4d)\n"
			"ATK  : %3d / %4d (+%4d)　DEF  : %3d / %4d (+%4d)\n"
			"MATK : %3d / %4d (+%4d)　MDEF : %3d / %4d (+%4d)\n"
			"MCR  : %3d / %4d (+%4d)　SPD  : %3d / %4d (+%4d)\n"
			"CVR  : %3d / %4d (+%4d)\n",
			j.Param.HP.MaximumStatus, i.Param.HP.MaximumStatus, i.Param.HP.MaximumStatus - j.Param.HP.MaximumStatus,
			j.Param.MP.MaximumStatus, i.Param.MP.MaximumStatus, i.Param.MP.MaximumStatus - j.Param.MP.MaximumStatus,
			*j.Param.Physical.Attack, *i.Param.Physical.Attack, *i.Param.Physical.Attack - *j.Param.Physical.Attack,
			*j.Param.Physical.Defence, *i.Param.Physical.Defence, *i.Param.Physical.Defence - *j.Param.Physical.Defence,
			*j.Param.Magic.Attack, *i.Param.Magic.Attack, *i.Param.Magic.Attack - *j.Param.Magic.Attack,
			*j.Param.Magic.Defence, *i.Param.Magic.Defence, *i.Param.Magic.Defence - *j.Param.Magic.Defence,
			*j.Param.MagicCure, *i.Param.MagicCure, *i.Param.MagicCure - *j.Param.MagicCure,
			*j.Param.Speed, *i.Param.Speed, *i.Param.Speed - *j.Param.Speed,
			*j.Param.Cleverness, *i.Param.Cleverness, *i.Param.Cleverness - *j.Param.Cleverness
		);
		std::getchar();
	}

}

static void ApplyLevelInfo(std::vector<Status<int>> &Arr) {
	for (int i = 0; i < (int)Arr.size(); i++) {
		PropertiesRead prop("D:\\Users\\AinoMegumi\\BitBucket\\Kamioda Games\\Kamioda Warrior\\Files\\DefeatMonster\\Level\\" + Arr[i].Tag + ".properties");
		for (auto& j : ReadApplyLevelData(prop)) Arr[i].Param.ApplyLevelInfo(j);
	}
	MessageBoxA(NULL, "全員のデータにレベル100の情報を適用しました。", "準備完了", MB_ICONINFORMATION | MB_OK);
}

std::vector<std::string> ConvertArgs(int argc, char* argv[]) {
	std::vector<std::string> Arr;
	for (int i = 0; i < argc; i++) Arr.emplace_back(std::string(argv[i]));
	return Arr;
}

std::vector<Status<int>> Pickup(std::vector<std::string> ArgList, const std::vector<Status<int>> Arr) {
	std::vector<Status<int>> Buffer;
	for (auto& i : ArgList) {
		for (auto& j : Arr) {
			if (i == j.Name) {
				Buffer.emplace_back(j);
				break;
			}
		}
	}
	return Buffer;
}

int main(int argc, char* argv[]) {
	try {
		std::cout << "Now Loading..." << std::endl;
		std::vector<Status<int>> Arr = 
			argc == 1 ? GetParameterInfo(
				"D:\\Users\\AinoMegumi\\Bitbucket\\Kamioda Games\\Kamioda Warrior\\Files\\DefeatMonster\\Status\\partner.xml"
			)
			: Pickup(
				ConvertArgs(argc, argv),
				GetParameterInfo(
					"D:\\Users\\AinoMegumi\\Bitbucket\\Kamioda Games\\Kamioda Warrior\\Files\\DefeatMonster\\Status\\partner.xml"
				)
			);
		const std::vector<Status<int>> Arr2 = Arr;
		ApplyLevelInfo(Arr);
		std::system("cls");
		AllGraph(Arr, Arr2);
	}
	catch (const std::exception &er) {
		std::cout << er.what() << std::endl;
	}
	return 0;
}
