#ifndef __STATUS_H__
#define __STATUS_H__
#include <algorithm>
template<class T> class PossibleChangeStatus {
	T MinimizeStatus;
public:
	PossibleChangeStatus() = default;
	PossibleChangeStatus(const T Maximum) : PossibleChangeStatus(Maximum, Maximum) {}
	PossibleChangeStatus(const T Current, const T Maximum, const T Minimize = 0)
		: CurrentStatus(Current), MaximumStatus(Maximum), MinimizeStatus(Minimize) {}
	T CurrentStatus, MaximumStatus;
	T& operator = (const PossibleChangeStatus& p) { return p.CurrentStatus; }
	PossibleChangeStatus& operator += (const T Num) {
		this->CurrentStatus += std::min(Num, this->MaximumStatus);
		return *this;
	}
	PossibleChangeStatus& operator -= (const T Num) {
		this->CurrentStatus -= std::max(Num, this->MinimizeStatus);
		return *this;
	}
	PossibleChangeStatus& operator *= (const T Num) {
		this->CurrentStatus = std::max(this->CurrentStatus * Num, this->MaximumStatus);
		return *this;
	}
	PossibleChangeStatus& operator /= (const T Num) {
		this->CurrentStatus /= Num;
		return *this;
	}
	T operator * () const { return this->CurrentStatus; }
	inline void ApplyLevelInfo(const T UpPoint) {
		this->MaximumStatus += UpPoint;
		this->CurrentStatus += UpPoint;
	}
	inline bool Less(const T CompareNum) const { return this->CurrentStatus < CompareNum; }
	inline bool Minimize() const { return this->CurrentStatus == this->MinimizeStatus; }
};

template<typename T> struct UseDamageCalculationStatus {
	UseDamageCalculationStatus() = default;
	UseDamageCalculationStatus(const T Attack, const T Defence) : Attack(Attack, Attack + 200, 1), Defence(Defence, Defence + 200, 1) {}
	PossibleChangeStatus<T> Attack, Defence;
	void ApplyLevelInfo(const T AddPointToAttack, const T AddpointToDefence) {
		this->Attack.ApplyLevelInfo(AddPointToAttack);
		this->Defence.ApplyLevelInfo(AddpointToDefence);
	}
};

#include <array>
#include <utility>

template<typename T> struct Parameter {
	Parameter() = default;
	Parameter(const T HP, const T MP, const T Attack, const T Defence, const T MagicAttack, const T MagicDefence, const T MagicCure,
		const T Speed)
		: Level(1, 100, 1), HP(HP), MP(MP),
		Physical(Attack, Defence), Magic(MagicAttack, MagicDefence), MagicCure(MagicCure, MagicCure + 200, 1), Speed(Speed, 999, 1),
		Cleverness(0, 999, 0) {}
	Parameter(const T HP, const T MP, const T Attack, const T Defence, const T MagicAttack, const T MagicDefence, const T MagicCure,
		const T Speed, const T Cleverness)
		: Level(1, 100, 1), HP(HP), MP(MP),
		Physical(Attack, Defence), Magic(MagicAttack, MagicDefence), MagicCure(MagicCure, MagicCure + 200, 1), Speed(Speed, 999, 1),
		Cleverness(Cleverness, 999, 0) {}
	PossibleChangeStatus<T> Level, HP, MP, MagicCure, Speed, Cleverness;
	UseDamageCalculationStatus<T> Physical, Magic;
	void ApplyLevelInfo(const std::array<T, 9> AddPointList) {
		this->Level.CurrentStatus++;
		this->HP.ApplyLevelInfo(AddPointList[0]);
		this->MP.ApplyLevelInfo(AddPointList[1]);
		this->Physical.ApplyLevelInfo(AddPointList[2], AddPointList[3]);
		this->Magic.ApplyLevelInfo(AddPointList[4], AddPointList[5]);
		this->MagicCure.ApplyLevelInfo(AddPointList[6]);
		this->Speed.ApplyLevelInfo(AddPointList[7]);
		this->Cleverness.ApplyLevelInfo(AddPointList[8]);
	}
	void ChangeCommunicationBattleModeStatus() {
		this->HP.MaximumStatus = 9999;
		this->MP.MaximumStatus = 9999;
	}
};

template<typename T> struct Status {
	Status(const std::string Name, const std::string Tag, const Parameter<T> Param)
		: Name(Name), Tag(Tag), Param(Param) {}
	std::string Name;
	std::string Tag;
	Parameter<T> Param;
};
#endif
