モンスター討伐ゲーム　アンチ核家族化

一人ではできなくても誰かと一緒ならどんなことだってできる
そう…たとえ凶悪なモンスターを倒すことだって

操作方法
Ｗキー／↑キー：カーソル　上へ
Ｓキー／↓キー：カーソル　下へ
Ａキー／←キー：カーソル　左へ
Ｄキー／→キー：カーソル　右へ
Spaceキー／Enterキー　　決定
Ｂキー／Escキー　　　　　キャンセル
※コンフィグは変更可能

PCSP
対応：あり
自動セーブ機能：なし
プレイ時間カウント開始：プレイモード選択時
時間になった時の処理：ターン終了時にゲームの強制終了
